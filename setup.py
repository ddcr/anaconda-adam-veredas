import sys
from setuptools import setup, find_packages

try:
    import versioneer
    versioneer.get_version()
except ImportError:
    sys.stderr.write('it lloks versioneer.py is missing')
    sys.stderr.write('call versioneer install to generate versioneer.py')
    sys.exit(1)

with open('README.rst') as readme_file:
    readme = readme_file.read()

setup(
    name='anaconda-adam-veredas',
    version=versioneer.get_version(),
    packages=find_packages(exclude=['playground']),
    include_package_data=True,
    author='Domingos Rodrigues',
    author_email='ddcr@lcc.ufmg.br',
    url='http://www.cenapad.ufmg.br',
    description='Anaconda Adam for cluster veredas (BULL-UFMG)',
    long_description=readme + '\n',
    entry_points={
        'console_scripts': [
            'adam = adam.cli.main:start',
        ],
    },
    license='BSD License',
    cmdclass=versioneer.get_cmdclass()
)
