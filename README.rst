===================================
Anaconda Adam for Cluster BULL-UFMG
===================================

========
Overview
========

This package is borrowed heavily from Anaconda Adam with
some reverse engineering. This package if for private
consumption only.

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `grzanka/cookiecutter-pip-docker-versioneer`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`grzanka/cookiecutter-pip-docker-versioneer`: https://github.com/grzanka/cookiecutter-pip-docker-versioneer
