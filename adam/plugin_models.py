from __future__ import print_function, division, absolute_import

import logging

from schematics.models import Model
from schematics.types import IntType, StringType, BooleanType
from schematics.types.compound import ListType
from schematics.exceptions import ValidationError

logger = logging.getLogger(__name__)


def get_plugins_modules():
    """
    Return the plugins that are installed

    Returns
    -------
        list of tuples: ('plugins_name', plugin_module), ...]

        Example:
        [('repository', <module 'adam_repository' from '.../adam_repository/__init__.py), ...]
    """

    # Hack: Use the CLI entrypoints to load the plugins so the plugins are loaded when using only the core library
    from pkg_resources import iter_entry_points
    plugins = list(iter_entry_points("adam.cli_plugins"))
    plugins.extend(list(iter_entry_points("adam.cli_scale_plugins")))

    plugins_modules = []
    for entry_point in plugins or ():
        # entrypoint -> adam_plugin.cli
        module_name = entry_point.module_name.split(".")[0]

        try:
            module_import = __import__(module_name)
        except NameError:
            logger.error("Failed to import module: {0} from {2}".format(entry_point.module_name))

        plugins_modules.append((module_name, module_import))

    # End Hack

    return plugins_modules


class Conda(Model):
    default_miniconda_url = u"http://repo.continuum.io/miniconda/Miniconda2-4.2.12-Linux-x86_64.sh"
    default_miniconda_hash = u"md5=c8b836baaa4ff89192947e4b1a70b07e"
    default_anaconda_url = u"http://repo.continuum.io/archive/Anaconda2-4.2.0-Linux-x86_64.sh"
    default_anaconda_hash = u"md5=a0d1fbe47014b71c6764d76fb403f217"

    enabled = BooleanType(required=True, default=True)
    rootdir = StringType(required=True, default="/opt/continuum")
    miniconda_url = StringType(required=True, default=default_miniconda_url)
    miniconda_hash = StringType(required=True, default=default_miniconda_hash)
    anaconda_url = StringType(required=True, default=default_anaconda_url)
    anaconda_hash = StringType(required=True, default=default_anaconda_hash)
    conda_canary = BooleanType(default=False)

    def validate_miniconda_hash(self, data, value):
        """
        Ensures we have an anaconda_hash with anaconda_url
        """
        if len(data["miniconda_hash"].split("=")) != 2:
            raise ValidationError("miniconda_hash must define hash type.  Example: md5=...")
        return value

    def validate_anaconda_hash(self, data, value):
        """
        Ensures we have an anaconda_hash with anaconda_url
        """
        if len(data["anaconda_hash"].split("=")) != 2:
            raise ValidationError("anaconda_hash must define hash type.  Example: md5=...")
        return value

    channels = ListType(StringType, default=lambda: ["defaults", "anaconda-adam"])
    channel_alias = StringType(default="https://conda.anaconda.org/")

    # ssl_verify can be true/false/str
    # http://conda.pydata.org/docs/install/central.html#ssl-verification
    ssl_verify = StringType(default=True)


class SaltSettings(Model):
    service_scripts = BooleanType(default=False)
    job_pub_port = IntType(default=14505)
    minion_pub_port = IntType(default=14510)
    minion_pull_port = IntType(default=14511)
    minion_ret_port = IntType(default=14506)
    rest_port = IntType(default=18000)
    salt_master = ListType(StringType)  # [host]
    acl = ListType(StringType, default=["anaconda"])
    salt_username = StringType(default="anaconda")
    salt_password = StringType(default="anaconda")
    salt_groupname = StringType(default="anaconda")
