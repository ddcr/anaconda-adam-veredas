from __future__ import print_function, division, absolute_import

import os
import json
import yaml
import time
import socket
import logging
import posixpath

from paramiko.ssh_exception import BadHostKeyException, AuthenticationException, SSHException

from schematics.models import Model, FieldDescriptor
from schematics.types import IntType, StringType, BooleanType
from schematics.types.compound import ListType, ModelType
from schematics.exceptions import ValidationError

from . import libpepper
from .utils import retry
from .ssh import SSHClient
from .responses import Response, StateResponse
from .compatibility import URLError
from .exceptions import AdamException
from .plugin_models import Conda, SaltSettings

logger = logging.getLogger(__name__)


class Node(Model):
    """
    A Node represents a machine (physical, VM or container) that will be managed by Adam.

    All the nodes will have the salt-minion running.
    Authentication attributes are used for SSH.
    One node should not be duplicated in the cluster.

    Attributes
    ----------
        host: Required. Fully Qualified domain name of the node. It can also be an IP or a hostname.
        username: Required. Username used for SSH. This username needs to match the authentication with
            either the keypair or the password.
        keypair: Paramiko.RSAKey for the username.
            Required: this or password or agent_pubkey
        password: SSH password for the username.
            Required: this or keypair or agent_pubkey
        agent_pubkey: Matching public key to a key stored in an optional ssh-agent
            Required: this or keypair or password
        port (default=22): SSH port.
        id: Optional unique ID. Useful at the moment for cloud IDs.
    """
    host = StringType(required=False)
    fqdn = StringType(required=False)
    username = StringType(required=True)
    keypair = StringType()
    password = StringType()
    agent_pubkey = StringType()
    sudo_su_login = BooleanType(default=False, required=False)
    invoke_shell = BooleanType(default=False, required=False)
    sudo_cmd = StringType(default="sudo -S", required=False)
    sudo_cmd_flags = StringType(default="", required=False)
    port = IntType(default=22, required=False)
    id = StringType(required=False)
    roles = ListType(StringType, default=lambda: [])

    def validate_host(self, data, value):
        if data["host"] is None and data["fqdn"] is None:
            raise ValidationError("Should declare Node.host")
        if data["host"] is not None and data["fqdn"] is not None:
            raise ValidationError("Should declare only one of Node.host or Node.fqdn")
        if data["host"] is None and data["fqdn"] is not None:
            logger.warn("Node.fqdn (value='%s') is deprecated and will be removed use Node.host instead", value)
            self.host = value
        return value

    def validate_password(self, data, value):
        if data["keypair"] is None and data["password"] is None and data["agent_pubkey"] is None:
            raise ValidationError("Should declare only one of the following keys: "
                                  "keypair, password, or agent_pubkey")
        return value

    def validate_agent_pubkey(self, data, value):
        if data["agent_pubkey"] and (data["keypair"] or data["password"]):
            raise ValidationError("Can't declare keypair or password when using ssh-agent key")


    @retry(catch=(BadHostKeyException, AuthenticationException, SSHException, socket.error, TypeError, AdamException))
    def check_ssh(self):
        """Check SSH connectivity to the Node
        """
        logger.debug("Checking ssh connection for %s", self.host)
        client = self.ssh_client
        if client.get_transport() is not None and client.get_transport().is_active():
            transport = client.get_transport()
            transport.send_ignore()
            logger.debug("SSH connection to %s: OK", self.host)
            return True
        return False

    @property
    def ssh_client(self):
        """Get an adam.ssh.SSHClient with the correct auth and settings for this node
        """

        if self.keypair:
            if os.path.isfile(os.path.expanduser(self.keypair)):
                pkey = SSHClient.pkey_from_key_file(self.keypair)
            else:
                pkey = SSHClient.pkey_from_string(self.keypair)
            client = SSHClient(self.host, username=self.username, pkey=pkey, port=self.port,
                               invoke_shell=self.invoke_shell, sudo_su_login=self.sudo_su_login,
                               sudo_cmd=self.sudo_cmd, sudo_cmd_flags=self.sudo_cmd_flags)
        elif self.agent_pubkey:
            pkey = SSHClient.pkey_from_key_file_w_agent(self.agent_pubkey)
            client = SSHClient(self.host, username=self.username, pkey=pkey, port=self.port,
                               invoke_shell=self.invoke_shell, sudo_su_login=self.sudo_su_login,
                               sudo_cmd=self.sudo_cmd, sudo_cmd_flags=self.sudo_cmd_flags)
        elif self.password:
            client = SSHClient(self.host, username=self.username, password=self.password, port=self.port,
                               invoke_shell=self.invoke_shell, sudo_su_login=self.sudo_su_login,
                               sudo_cmd=self.sudo_cmd, sudo_cmd_flags=self.sudo_cmd_flags)
        else:
            client = SSHClient(self.host, username=self.username, port=self.port,
                               invoke_shell=self.invoke_shell, sudo_su_login=self.sudo_su_login,
                               sudo_cmd=self.sudo_cmd, sudo_cmd_flags=self.sudo_cmd_flags)
        return client

    def __repr__(self):
        return json.dumps(self.to_primitive())


class PluginModel(Model):
    """
    Base class that plugin models should extend
    """
    enabled = BooleanType(default=False)

    def validate(self, *args, **kwargs):
        """
        Custom validation doesn't fail if the plugin is disabled
        """
        # For schematics 1.1.1:
        if self.enabled is True:
            return super(PluginModel, self).validate(*args, **kwargs)
        # END
        # For schematics 2.0.0: This functions does not get invoked and its not needed.
        # Instead use commented code at Cluster.validate

########################################################################################################################
# Cluster
########################################################################################################################


class System(Model):
    """
    System options for the cluster
    """
    tmp_dir = StringType(default="/tmp", required=False)


class Network(Model):
    """
    Networking options for the cluster
    """
    http_proxy = StringType(required=False)
    https_proxy = StringType(required=False)


class Security(Model):
    """
    Security options for the cluster
    """
    flush_iptables = BooleanType(default=False)
    selinux_permissive = BooleanType(default=False)
    selinux_context = BooleanType(default=False)


class Plugins_(Model):
    """
    Temporal class that that gets modified in `load_plugin_fields`.
    This is required to make `models.Plugins` (that extends `Cluster_`) have all the fields on import
    """
    conda = ModelType(Conda, default=Conda())
    salt_settings = ModelType(SaltSettings, default=SaltSettings())


class Cluster_(Model):
    """
    Temporal class that that gets modified in `load_plugin_fields`.
    This is required to make `models.Cluster` (that extends `Cluster_`) have all the fields on import
    """
    name = StringType(required=True)
    nodes = ListType(ModelType(Node), default=lambda: [])
    conda = ModelType(Conda, default=Conda())
    salt_settings = ModelType(SaltSettings, default=SaltSettings())
    network = ModelType(Network, default=Network())
    security = ModelType(Security, default=Security())
    system = ModelType(System, default=System())


def load_plugin_fields():
    """
    Insert the custom plugin models settings into the `Cluster` object above.
    """
    from .plugin_models import get_plugins_modules
    plugins_modules = get_plugins_modules()

    for p in plugins_modules:
        try:
            models = getattr(p[1], "MODELS")
            for model_name, model_cls in models.items():
                modeltype = ModelType(model_cls, default=model_cls())
                setattr(Cluster_, model_name, FieldDescriptor(model_name))
                Cluster_._fields[model_name] = modeltype
                Plugins_._fields[model_name] = modeltype
        except AttributeError:
            logger.debug("No models schema defined for {}".format(p[0]))


load_plugin_fields()


class Plugins(Plugins_):
    """Main model that has all the plugins settings

    Note: See adam.models.Plugins_
    """
    pass


class Cluster(Cluster_):
    """Main model that has all the cluster information
    """

    def __init__(self, *args, **kwargs):
        super(Cluster_, self).__init__(*args, **kwargs)
        # Note: default self.conda/salt are pre-populated in defaults of Cluster_
        self._pepper = None

    # For schematics 2.0.0. For schematics 1.1.1 see PluginModel.validate
    # def validate(self, *args, **kwargs):
    #     """
    #     Custom validation that uses partial=True (ignore required fields) for the PluginModels that are disabled.
    #     """
    #     from schematics.transforms import BasicConverter
    #     from schematics.validate import get_validation_context
    #     from schematics.transforms import validation_converter as original_validation_converter
    #
    #     @BasicConverter
    #     def custom_validation_converter(field, value, context):
    #         if isinstance(field, ModelType) and issubclass(field.model_class, PluginModel):
    #             if not value.enabled:
    #                 extra_context_options_2 = {"field_converter": original_validation_converter, "partial": True, "oo": False}
    #                 context = get_validation_context(**extra_context_options_2)
    #         return original_validation_converter(field, value, context)
    #
    #     extra_context_options = {"field_converter": custom_validation_converter, "oo": False}
    #
    #     context = get_validation_context(**extra_context_options)
    #     return super(Cluster, self).validate(context=context, *args, **kwargs)
    # END

    @classmethod
    def from_filepath(cls, filepath):
        with open(filepath, "r") as f:
            data = yaml.load(f.read())
            return cls(data)


    @classmethod
    def from_profile(cls, name, profile_spec):
        new_cluster = cls()
        new_cluster.name = name

        bare = profile_spec.bare
        for node in profile_spec.bare.nodes:
            host = node.host
            username = node.username or bare.username
            keypair = node.keypair or bare.keypair
            password = node.password or bare.password
            agent_pubkey = node.agent_pubkey or bare.agent_pubkey
            port = node.port or bare.port
            sudo_su_login = node.sudo_su_login or bare.sudo_su_login
            invoke_shell = node.invoke_shell or bare.invoke_shell
            sudo_cmd = node.sudo_cmd or bare.sudo_cmd
            sudo_cmd_flags = node.sudo_cmd_flags or bare.sudo_cmd_flags
            roles = node.roles or []
            node = Node(dict(host=host, username=username, keypair=keypair,
                             password=password, sudo_su_login=sudo_su_login,
                             sudo_cmd=sudo_cmd, sudo_cmd_flags=sudo_cmd_flags,
                             invoke_shell=invoke_shell, port=port,
                             roles=roles, agent_pubkey=agent_pubkey))
            new_cluster.nodes.append(node)

        new_cluster.profile = profile_spec
        new_cluster.network = profile_spec.network
        new_cluster.security = profile_spec.security
        new_cluster.system = profile_spec.system
        plugins = profile_spec.plugins

        if plugins:
            for plugin_name, plugin_values in plugins.items():
                setattr(new_cluster, plugin_name, plugin_values)
        return new_cluster

    def get_node(self, host=None):
        """
        Filter nodes by host
        """
        for node in self.nodes:
            if host is not None and node.host == host:
                return node

    @property
    def head(self):
        return self.nodes[0]

    @property
    def pepper(self):
        """Get a (singletion) reference to the a pepper REST client that is connected to the
        salt master of the cluster
        """
        if not self._pepper:
            url = "https://{}:18000".format(self.head.host)
            try:
                username = self.salt_settings.salt_username
                password = self.salt_settings.salt_password
                self._pepper = libpepper.Pepper(url, ignore_ssl_errors=True)
                self._pepper.login(username, password, "pam")
            except URLError as e:
                raise AdamException("Could not connect to salt REST server: {}".format(e))
        return self._pepper

    @property
    def rootdir(self):
        """
        Returns
        -------
        str
            root directory as defined in plugin_models.py::Conda
            default: /opt/continuum
        """
        return self.conda.rootdir

    @property
    def salt_prefix(self):
        """
        Returns
        -------
        str
            salt prefix location
            default: /opt/continuum/salt
        """
        return posixpath.join(self.rootdir, "salt")

    @property
    def conda_bin(self):
        """
        Returns
        -------
        str
            conda binary for salt agents
            default: /opt/continuum/salt/bin/conda
        """
        return posixpath.join(self.salt_prefix, "bin", "conda")

    @property
    def conda_rc(self):
        """
        Returns
        -------
        str
            .condarc file path for salt agents
            default: /opt/continuum/salt/.condarc
        """
        return posixpath.join(self.salt_prefix, ".condarc")

    @property
    def anaconda_dir(self):
        """
        Returns
        -------
        str
            anaconda installation location
            default: /opt/continuum/anaconda
        """
        return posixpath.join(self.rootdir, "anaconda")

    def salt(self, target, module, *args, **kwargs):
        """
        Method to remotely call salt function using libpepper interface

        Parameters
        ----------
            target: str
                salt target: (minion_id, *, ...)
            module: str
                salt module to run: state.sls, pkg.install, test.ping
            args: list
                positional arguments passed to the module a b
            kwargs: dictionary
                keyword arguments passed to the module: a=1 b=2

        Returns
        -------
            adam.Response

        Examples
        --------
            salt("*", "state.sls", "anaconda")  # cli/provision.py
            salt("*", "conda.install", kwarg=kwargs)  # cli/conda.py
        """
        args = [] or args
        logger.debug("Executing salt '{0}' {1} {2} {3}".format(target, module, args, kwargs))
        active = True
        try:
            async_output = self.pepper.local_async(target, module, *args, **kwargs)
            return_output = async_output.get("return")

            # check for missing output
            # {'return': [{}]}
            if not return_output[0].get('jid', False):
                logger.debug("Job successfully completed: {}".format(async_output))
                job_output = async_output
            else:
                jid = return_output[0].get('jid')
                logger.debug("Job successfully started wth id: {}".format(jid))

                # periodically check active job list
                i = 1
                while active:

                    # periodically check the active jobs list
                    # and reset connection
                    if i % 5 == 0:

                        # results in a re-authentication
                        # useful for dropped connections
                        logger.debug("Resetting salt-api connection")
                        self._pepper = None

                        if not self.job_active(jid):
                            active = False
                            time.sleep(2)
                            job_output = self.pepper.lookup_jid(jid)
                            break

                    logger.debug("Waiting for job {} to finish".format(jid))

                    if self.check_finished(jid):
                        active = False
                        logger.debug("Job {} finished".format(jid))
                        time.sleep(2)
                        job_output = self.pepper.lookup_jid(jid)
                        break

                    time.sleep(5)
                    i += 1

            logger.debug("Job output: {}".format(job_output))
            response = Response.from_libpepper(job_output)
            if module == "state.sls":
                try:
                    # validate here to check for missing sls, etc.
                    response.validate()
                    response = response.as_cls(StateResponse)
                except AttributeError:
                    # This error seems to occur when there is a hiccup with the network
                    # deleting self._pepper and reconnecting: cluster.salt('*', 'test.ping')
                    # can be helpful
                    msg = "Response object is unable convert to StateResponse"
                    logger.debug(msg)
                    logger.debug(str(response))
                    raise AttributeError("'list' object has no attribute 'items'")

            return response
        except URLError as e:
            raise AdamException("Could not connect to salt REST server: {}".format(e))

    def job_active(self, jid):
        """
        Check if Salt job is still active

        Parameters
        ----------
        jid: job id

        Returns
        -------
        bool
        """

        active_jobs = self.pepper.runner("jobs.active")
        return_output = active_jobs.get("return", False)
        if not return_output[0]:

            # sometimes the job queue can be clogged
            # wait a bit and try again just to be sure
            time.sleep(3)
            active_jobs = self.pepper.runner("jobs.active")
            return_output_2 = active_jobs.get("return", False)

            if not return_output_2[0]:
                logger.error("Job {} finished -- no active jobs running".format(jid))
                return False

        return True

    def check_finished(self, jid):
        """
        Check if Salt job is finished

        Parameters
        ----------
        jid: job id

        Returns
        -------
        bool
        """

        lookup_jid = self.pepper.runner("jobs.lookup_jid", jid=jid, returned=False, missing=True)
        return_output = lookup_jid.get('return', False)
        if return_output:
            return not bool(return_output[0])

        return False

    ####################################################################################################################
    # Cluster wide functions
    ####################################################################################################################

    def set_nodes_username(self, username):
        for node in self.nodes:
            node.username = username
        self.validate()

    def set_nodes_keypair(self, keypair_path):
        for node in self.nodes:
            node.keypair = keypair_path
        self.validate()

    def check_ssh(self):
        """
        Check SSH connection to all the nodes in the cluster

        Returns
        -------
            Dictionary like: {node_host: ssh_status}
        """
        ret = {}
        for node in self.nodes:
            ret[node.host] = node.check_ssh()
        return ret

    def __repr__(self):
        return json.dumps(self.to_primitive())

########################################################################################################################
# Profiles
########################################################################################################################


class NodeBare(Model):
    """Represents a Node in a bare metal context.
    Basically just SSH connection settings

    Note: These fields should not a have default values. TRUST ME!
    The default values will be the ones on BareProfile
    """
    host = StringType(required=True)
    username = StringType()
    keypair = StringType()
    password = StringType()
    agent_pubkey = StringType()
    port = IntType()
    sudo_su_login = BooleanType()
    invoke_shell = BooleanType()
    sudo_cmd = StringType()
    sudo_cmd_flags = StringType()
    roles = ListType(StringType, default=lambda: [])

    def validate_password(self, data, value):
        if data["keypair"] and data["password"]:
            raise ValidationError("Can't declare both keypair and password")
        return value

    def validate_agent_pubkey(self, data, value):
        if data["agent_pubkey"] and (data["keypair"] or data["password"]):
            raise ValidationError("Can't declare keypair or password when using ssh-agent key")


class BareProfile(Model):
    """Represents a Profile based on "Bare" nodes.
    Basically a list of nodes with just SSH connection settings

    Main `username`, `keypair`, `password`, `port` attributes are the defaults for each node
    but they can be overwriten for each particular node.
    """
    username = StringType(required=True)
    keypair = StringType()
    password = StringType()
    agent_pubkey = StringType()
    port = IntType()
    sudo_su_login = BooleanType(default=False)
    invoke_shell = BooleanType(default=False)
    sudo_cmd = StringType(default="sudo -S", required=False)
    sudo_cmd_flags = StringType(default="", required=False)
    nodes = ListType(ModelType(NodeBare), default=lambda: [], required=True)

    def validate_password(self, data, value):
        if data["keypair"] is None and data["password"] is None and data["agent_pubkey"] is None:
            raise ValidationError("Should declare only one of the following keys: "
                                  "keypair, password, or agent_pubkey")
        return value

    def validate_agent_pubkey(self, data, value):
        if data["agent_pubkey"] and (data["keypair"] or data["password"]):
            raise ValidationError("Can't declare keypair or password when using ssh-agent key")


class Profile(Model):
    name = StringType(required=True)
    provider = StringType(required=True)

    bare = ModelType(BareProfile)
    plugins = ModelType(Plugins)
    network = ModelType(Network, default=Network())
    security = ModelType(Security, default=Security())
    system = ModelType(System, default=System())
