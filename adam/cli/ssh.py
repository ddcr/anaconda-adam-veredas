from __future__ import print_function, division, absolute_import

import click

import adam
from .main import cli

from . import utils


@cli.command(short_help="SSH to one of the nodes (0-based index)")
@click.pass_context
@click.argument("node", required=False, default=0)
@click.option("--cluster", "-n", "cluster_name", envvar="CLUSTERNAME", required=True, help="Cluster name")
@click.option("--command", "-c", required=False, help="Command to execute in the node")
def ssh(ctx, node, cluster_name, command):
    """Start an SSH connection to node of the cluster

    \b
    Usage:
    adam ssh -n name
    adam ssh -n name 10             # Node 10 of the cluster
    adam ssh -n name -c "date"      # Excute a command
    """
    import os
    import sys
    import subprocess
    ctx.obj = ctx.obj or {}
    ctx.obj["cluster_name"] = cluster_name
    cluster = utils.get_cluster_object(ctx, salt_password_check=False)

    if node >= len(cluster.nodes):
        click.echo("Error: Cluster only has {} nodes".format(len(cluster.nodes)), err=True)
        sys.exit(1)

    instance = cluster.nodes[node]
    ip = instance.host
    username = instance.username
    keypair = os.path.expanduser(instance.keypair) if instance.keypair else None

    cmd = ["ssh", username + "@" + ip]
    if keypair:
        cmd.extend(["-i", keypair])
    cmd.extend(["-oStrictHostKeyChecking=no"])
    cmd.extend(["-p {}".format(instance.port)])
    if command is not None:
        cmd.extend(["{}".format(command)])
    click.echo(" ".join(cmd))

    if command is not None:
        p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()
        stdout, stderr = p.communicate()
        stdout, stderr = stdout.decode("utf-8").strip(), stderr.decode("utf-8").strip()
        if stdout:
            click.echo(stdout)
        if stderr:
            click.echo(stderr, err=True)
        sys.exit(p.returncode)
    else:
        subprocess.call(cmd)
