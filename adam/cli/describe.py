from __future__ import print_function, division, absolute_import

import sys
from copy import deepcopy
from collections import OrderedDict

import yaml
import click

import adam
from .main import cli


def order_output(key):
    """
    Small function to reorder the cluster profile dictionary.
    Desired order: name, nodes, everything else
    """
    if key == "name":
        return 0
    elif key == "nodes":
        return 1
    else:
        return 2


# safe_dump can't handle OrderedDict
# http://stackoverflow.com/questions/16782112/can-pyyaml-dump-dict-items-in-non-alphabetical-order
class UnsortableList(list):

    def sort(self, *args, **kwargs):
        pass


class UnsortableOrderedDict(OrderedDict):

    def items(self, *args, **kwargs):
        return UnsortableList(OrderedDict.items(self, *args, **kwargs))


yaml.add_representer(UnsortableOrderedDict, yaml.representer.SafeRepresenter.represent_dict)


@cli.group("describe", short_help="Describe an Adam cluster or profile", invoke_without_command=True)
@click.pass_context
@click.option("--cluster", "-n", "cluster_name", required=False, help="Cluster name")
@click.option("--profile", "-p", "profile_name", required=False, help="Profile name")
@click.option("--verbose", "-v", default=False, is_flag=True, help="Verbose Output")
def describe(ctx, cluster_name, profile_name, verbose):
    """Describe and adam object (Cluster or Profile)

    \b
    Usage:
    adam describe -n cluster-name
    adam describe -p profile_name
    """
    if not cluster_name and not profile_name:
        click.echo(
            "Error: either -n/--cluster or -p/--profile option required\nTry 'adam describe -h' for more details",
            err=True)
        sys.exit(1)

    if cluster_name:
        cluster = adam.config.get_cluster(cluster_name)

    else:
        cluster = adam.config.get_profile(profile_name)

    _data = cluster.to_primitive()
    _cleaned_data = deepcopy(_data)

    # clean up disabled plugins
    if not verbose:
        for k in _data.keys():
            if isinstance(_data[k], dict):
                if not _data[k].get('enabled'):
                    _cleaned_data.pop(k)

    _dict = UnsortableOrderedDict(sorted(_cleaned_data.items(), key=lambda k: order_output(k[0])))
    string = yaml.dump(_dict, default_flow_style=False).strip()
    click.echo(string)
