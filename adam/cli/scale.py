from __future__ import print_function, division, absolute_import

import yaml
import sys
import click
from click_plugins import with_plugins
from pkg_resources import iter_entry_points
from terminaltables import AsciiTable

from .. import config
from .main import cli
from . import utils

from ..responses import CondaRemoteResponse
from ..conda_remote import conda_install as rconda_install
from ..conda_remote import conda_info as rconda_info
from ..conda_remote import conda_list as rconda_list
from ..conda_remote import conda_remove as rconda_remove
from ..conda_remote import conda_update as rconda_update
from ..conda_remote import conda_create as rconda_create


@with_plugins(iter_entry_points("adam.cli_scale_plugins"))
@cli.group("scale", short_help="Anaconda Scale options")
@click.pass_context
@click.option("--cluster", "-n", "cluster_name", envvar="CLUSTERNAME", required=True, help="Cluster name")
def scale(ctx, cluster_name):
    """
    Manage Anaconda environments and packages on a cluster.
    Also includes functionality for distributed computations
    and single-user Jupyter Notebook.
    """
    ctx.obj = ctx.obj or {}
    ctx.obj["cluster_name"] = cluster_name
    pass


@scale.group("cluster", short_help="Anaconda Scale options for cluster installation")
@click.pass_context
def cluster(ctx):
    pass


@cluster.command("install", short_help="Install Anaconda across a cluster")
@click.option("--no-accelerate", "no_accelerate", is_flag=True, help="Don't install accelerate libraries")
@click.pass_context
def install(ctx, no_accelerate):
    """Install Anaconda Scale on all nodes in the cluster
    """
    from ..responses import StateResponse
    cluster = utils.get_cluster_object(ctx)
    click.echo("Installing Anaconda")
    response = cluster.salt("*", "state.sls", "anaconda")
    response = response.as_cls(StateResponse)
    response.validate()
    utils.print_states_table(cluster, response)

    if not no_accelerate:
        click.echo("Installing Accelerate")
        response = cluster.salt("*", "state.sls", "anaconda.accelerate")
        response = response.as_cls(StateResponse)
        response.validate()
        utils.print_states_table(cluster, response)


@scale.group("conda", short_help="Manage Anaconda across a cluster")
@click.pass_context
def conda(ctx):
    pass


@conda.command("create", short_help="Create conda environment")
@click.pass_context
@click.argument("packages", required=True, nargs=-1)
@click.option("--name", "-n", required=False, help="Name of environment in rootdir. Default: /opt/continuum/anaconda")
@click.option("--prefix", "-p", required=False, help="Full path to environment prefix")
@click.option("--channel", "-c", multiple=True, required=False, help="Channel name")
@click.option("--user", "-u", required=False, help="User")
def conda_create(ctx, packages, name, prefix, channel, user):
    """Create a conda environment in the cluster nodes

    \b
    Usage:
    adam conda -n name create -n env_name python=2 numpy
    adam conda -n name create -p /tmp/env_path python=3 jupyter -c conda-forge
    """
    is_prefix = False
    cluster = utils.get_cluster_object(ctx)
    if not name and not prefix:
        click.echo(
            "Error: either -n/--name or -p/--prefix option required\nTry 'adam conda create -h' for more details",
            err=True)
        sys.exit(1)
    if name or prefix:
        if name and prefix:
            click.echo("Error: argument -p/--prefix: not allowed with argument -n/--name", err=True)
            sys.exit(1)
        if prefix:
            is_prefix = True
        env = name or prefix

    pkgs = ",".join(packages)
    response = rconda_create(cluster, env=env, pkgs=pkgs, prefix=is_prefix, user=user, channels=channel)
    pprint_groups(response.groups(ignore_fields=["pid"]), general_print)


@conda.command("install", short_help="Install package(s)")
@click.pass_context
@click.argument("packages", required=True, nargs=-1)
@click.option("--name",
              "-n",
              required=False,
              help="Name of environment in rootdir. Default: /opt/continuum/anaconda",
              default="root")
@click.option("--prefix", "-p", required=False, help="Full path to environment prefix", default=False)
@click.option("--channel", "-c", multiple=True, required=False, help="Channel name")
@click.option("--user", "-u", required=False, help="User")
def conda_install(ctx, packages, name, prefix, channel, user):
    """Install conda packages

    \b
    Usage:
    adam conda -n name install scikit-learn pandas
    adam conda -n name install -n my_env jupyter pandas -c conda-forge
    """
    is_prefix = False
    cluster = utils.get_cluster_object(ctx)
    if name or prefix:
        if name and prefix:
            click.echo("Error: argument -p/--prefix: not allowed with argument -n/--name", err=True)
            sys.exit(1)
        if prefix:
            is_prefix = True
        env = name or prefix

    pkgs = ",".join(packages)
    response = rconda_install(cluster, env=env, pkgs=pkgs, prefix=is_prefix, user=user, channels=channel)
    pprint_groups(response.groups(ignore_fields=["pid"]), general_print)


@conda.command("list", short_help="List conda packages")
@click.pass_context
@click.option("--name",
              "-n",
              required=False,
              default="root",
              help="Name of environment in rootdir. Default: /opt/continuum/anaconda")
@click.option("--prefix", "-p", required=False, help="Full path to environment prefix")
def conda_list(ctx, name, prefix):
    """Install conda package

    \b
    Usage:
    adam conda -n name list
    adam conda -n name list -n my_env
    """
    is_prefix = False
    cluster = utils.get_cluster_object(ctx)

    if name or prefix:
        if name and prefix:
            click.echo("Error: argument -p/--prefix: not allowed with argument -n/--name", err=True)
            sys.exit(1)
        if prefix:
            is_prefix = True
        env = name or prefix

    response = rconda_list(cluster, env=env, prefix=is_prefix)
    pprint_groups(response.groups(ignore_fields=["pid"]), print_pkgs)


@conda.command("remove", short_help="Remove package(s)")
@click.pass_context
@click.argument("packages", required=False, nargs=-1)
@click.option("--name", "-n", default="root", required=False, help="Name of environment in rootdir. Default: /opt/continuum/anaconda")
@click.option("--prefix", "-p", required=False, help="Full path to environment prefix")
@click.option("--user", "-u", required=False, help="User")
@click.option("--all", "all_flag", required=False, is_flag=True, help="remove environment")
def conda_remove(ctx, packages, name, prefix, user, all_flag):
    """Remove a conda package

    \b
    Usage:
    adam conda -n name remove pandas
    """

    is_prefix = False
    cluster = utils.get_cluster_object(ctx)
    if len(packages) == 0 and not all_flag:
        click.echo('Usage: adam scale conda remove [OPTIONS] PACKAGES...\n'
                   'Error: Missing argument "packages".')
        # error code is 2 to mimic the error code when "packages" is required
        sys.exit(2)

    elif len(packages) > 0 and all_flag:
        click.echo("Error: Unable to remove conda environment when packages are defined. "
                   "Please use --all with no packages listed\n\t$ adam scale -n <CLUSTER> conda remove --all")
        sys.exit(1)
    elif len(packages)> 0:
        # everything is fine for removing packages
        packages = packages
    else:
        packages = ('--all',)

    if name or prefix:
        if name and prefix:
            click.echo("Error: argument -p/--prefix: not allowed with argument -n/--name", err=True)
            sys.exit(1)
        if prefix:
            is_prefix = True
        env = name or prefix

    # fail if name/prefix is root when removing env:
    if name == 'root' and all_flag:
        click.echo("Error: No environment was listed.  Please define which environment to remove.")
        sys.exit(1)

    pkgs = ",".join(packages)

    response = rconda_remove(cluster, env=env, pkgs=pkgs, prefix=is_prefix, user=user)
    pprint_groups(response.groups(ignore_fields=["pid"]), general_print)


@conda.command("update", short_help="Update package(s)")
@click.pass_context
@click.argument("packages", required=True, nargs=-1)
@click.option("--name", "-n", required=False, default="root", help="Name of environment in rootdir. Default: /opt/continuum/anaconda")
@click.option("--prefix", "-p", required=False, help="Full path to environment prefix")
@click.option("--channel", "-c", multiple=True, required=False, help="Channel name")
@click.option("--user", "-u", required=False, help="User")
def conda_update(ctx, packages, name, prefix, channel, user):
    """Update a conda package

    \b
    Usage:
    adam conda -n name update pandas
    """

    is_prefix = False
    cluster = utils.get_cluster_object(ctx)
    if name or prefix:
        if name and prefix:
            click.echo("Error: argument -p/--prefix: not allowed with argument -n/--name", err=True)
            sys.exit(1)
        if prefix:
            is_prefix = True
        env = name or prefix

    pkgs = ",".join(packages)
    response = rconda_update(cluster, env=env, pkgs=pkgs, prefix=is_prefix, user=user, channels=channel)
    pprint_groups(response.groups(ignore_fields=["pid"]), general_print)


@conda.command("info", short_help="Get information about conda installation")
@click.pass_context
@click.option("--all",
              "-a",
              "all_",
              required=False,
              is_flag=True,
              help="Show all information, (environments, license, and system information")
@click.option("--envs", "-e", required=False, is_flag=True, help="List all known conda environments")
@click.option("--license", "-l", required=False, is_flag=True, help="Display information about conda licenses")
@click.option("--system", "-s", required=False, is_flag=True, help="List environment variables")
def conda_info(ctx, all_, envs, license, system):
    """Get conda information

    \b
    Usage:
    adam conda -n name info --all
    """

    cluster = utils.get_cluster_object(ctx)

    options = []
    if all_:
        options.append("a")
    if envs:
        options.append("e")
    if license:
        options.append("l")
    if system:
        options.append("s")
    options = ",".join(options)

    response = rconda_info(cluster, options=options)
    pprint_groups(response.groups(ignore_fields=["pid"]), general_print)


@scale.group("env", short_help="Manage Anaconda across a cluster with conda-env yaml files")
@click.pass_context
def conda_env(ctx):
    pass


@conda_env.command("create", short_help="Create conda environment")
@click.pass_context
@click.option('-f', '--file', nargs=1, required=True, type=click.Path(exists=True))
@click.option("--user", "-u", required=False, help="User")
def conda_env_create(ctx, file, user):
    """Create a conda environment in the cluster nodes with yaml file

    \b
    Usage:
    adam scale -n name env create -f /full/path/f.yaml
    """

    cluster = utils.get_cluster_object(ctx)
    env_name, env_channels, env_deps = _conda_env_parse(file)

    pkgs = ",".join(env_deps)

    print(env_channels)
    print(user)

    print(env_name)

    print(env_deps)
    response = rconda_create(cluster, env=env_name, pkgs=pkgs, prefix=False, user=user, channels=env_channels)
    pprint_groups(response.groups(ignore_fields=["pid"]), general_print)


@conda_env.command("update", short_help="Update conda environment")
@click.pass_context
@click.option('-f', '--file', nargs=1, required=True, type=click.Path(exists=True))
@click.option("--user", "-u", required=False, help="User")
def conda_env_update(ctx, file, user):
    """Update a conda environment in the cluster nodes with yaml file

    \b
    Usage:
    adam scale -n name env update -f /full/path/f.yaml
    """

    cluster = utils.get_cluster_object(ctx)
    env_name, env_channels, env_deps = _conda_env_parse(file)

    pkgs = ",".join(env_deps)

    response = rconda_update(cluster, env=env_name, pkgs=pkgs, prefix=False, user=user, channels=env_channels)
    pprint_groups(response.groups(ignore_fields=["pid"]), general_print)


@conda_env.command("remove", short_help="Remove conda environment")
@click.pass_context
@click.option("--name",
              "-n",
              required=False,
              default="root",
              help="Name of environment in rootdir. Default: /opt/continuum/anaconda")
@click.option("--prefix", "-p", required=False, help="Full path to environment prefix")
@click.option("--user", "-u", required=False, help="User")
def conda_env_remove(ctx, name, prefix, user):

    """Remove a conda environment

    \b
    Usage:
    adam scale -n name remove -n <name of environment>
    """

    ctx.invoke(conda_remove, name=name, prefix=prefix, user=user, all_flag=True)


@conda_env.command("list", short_help="List conda environments")
@click.pass_context
@click.option("--user", "-u", required=False, help="User")
def conda_env_list(ctx, user):
    """List all conda environments

    \b
    Usage:
    adam scale -n name env list
    """

    ctx.invoke(conda_info, envs='--envs')


def general_print(data):
    resp = CondaRemoteResponse(data)
    if resp.success:
        click.echo(resp.stdout)
    else:
        click.echo(resp.stdout, err=True)
        click.echo(resp.stderr, err=True)
        click.echo(resp.error, err=True)

    check_anaconda_scale_installed(resp)


def _conda_env_parse(_file):
    """
    parse a conda env file and return triplet of name, deps, channels
    Parameters
    ----------
    _file: filepath

    Returns
    -------
    triplet: (name, deps, channels)

    """

    try:
        data = config.load_yml(_file)
    except yaml.scanner.ScannerError:
        print("ERROR: {} file is not valid YAML. Please fix and review documentation: "
              "http://conda.pydata.org/docs/using/envs.html#create-environment-file-by-hand".format(_file))
        sys.exit(1)
    env_name = data.get('name', '')

    if not env_name:
        click.echo("ERROR: conda env file missing name")
        sys.exit(2)

    env_channels = data.get('channels', [])

    env_deps = data.get('dependencies', [])

    if not env_deps:
        click.echo("ERROR: conda env file missing list of dependencies")
        sys.exit(2)

    for f in list(env_deps):
        # detect and remove pip dependencies
        if isinstance(f, dict):
            click.echo("WARNING: pip dependencies are not supported, skipping")
            env_deps.remove(f)

    return (env_name, env_channels, env_deps)


def print_pkgs(data):
    resp = CondaRemoteResponse(data)
    if resp.success:
        table_data = [["Package", "Version", "Build"]]
        for pkg_name, pkg_info in resp.packages.items():
            table_data.append([pkg_name, pkg_info["version"], pkg_info["build"]])
        table = AsciiTable(table_data)
        click.echo(table.table)
    else:
        click.echo(resp.stderr, err=True)
        click.echo(resp.error, err=True)

    check_anaconda_scale_installed(resp)


def pprint_groups(groups, pprint_group):
    if len(groups) == 1:
        group = groups[0]
        click.echo("All nodes (x{}) response:".format(len(group[0])))
        pprint_group(group[1])
    else:
        for group in groups:
            group_size = len(group[0])
            group_members = " ".join(group[0])
            click.echo("{0} nodes response ({1}):".format(group_size, group_members))
            pprint_group(group[1])


def check_anaconda_scale_installed(resp):
    if any("bin/conda: No such file" in msg for msg in resp.values() if isinstance(msg, str)):
        click.echo("Error: Anaconda is not installed/managed on one or more nodes in the cluster.", err=True)
        click.echo("Anaconda Scale can install a centrally managed version of Anaconda on the", err=True)
        click.echo("cluster using the following command: adam scale cluster install", err=True)
