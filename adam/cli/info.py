from __future__ import print_function, division, absolute_import

import click

import adam
from adam import license
from adam import plugin_models
from .main import cli


@cli.group("info", short_help="Display Adam version, system, and license information", invoke_without_command=True)
@click.pass_context
def info(ctx):
    """Output information about the system installation
    """
    import sys
    import platform

    clusters = adam.get_clusters()
    available_nodes = license.get_available_nodes()
    already_created_nodes = sum(len(cluster.nodes) for _, cluster in clusters.items())

    click.echo("Adam version: {}".format(adam.__version__))
    click.echo("Plugins:")
    plugin_modules = plugin_models.get_plugins_modules()
    for plugin_name, plugin_module in plugin_modules:
        click.echo("  {}: {}".format(plugin_name, plugin_module.__version__))
    click.echo("")
    (sysname, nodename, release, version, machine, processor) = platform.uname()
    click.echo("Platform: {}-{}-{}".format(sysname, release, machine))
    click.echo("{}".format(version))
    if sysname == "Linux":
        click.echo("Linux dist: {}".format(platform.linux_distribution()[:-1]))
    if not processor:
        processor = "not recognized"
    click.echo("Python version: {}".format(sys.version))
    click.echo("Processor: {}".format(processor))
    click.echo("Byte-ordering: {}".format(sys.byteorder))
    click.echo("")
    click.echo("License information:")
    click.echo("Number of nodes currently in use: {} ".format(already_created_nodes))
    click.echo("Number of licensed nodes: {}".format(available_nodes))
    click.echo("Number of managed clusters: {}".format(len(clusters)))
    click.echo("")
    click.echo("Valid platform component licenses:")
    licenses = license.get_all_licenses()
    if licenses:
        for i, license_ in enumerate(licenses):
            click.echo("{}: {}. End date: {}".format(i + 1, license_["product"], license_["end_date"]))
            license.check_expired_license(license_)
    else:
        click.echo("None")
