from __future__ import print_function, division, absolute_import

import sys

import click

from .. import config
from .main import cli
from .provision import provision
from adam import license


@cli.command("up", short_help="Create a cluster from a profile")
@click.pass_context
@click.argument("profile")
@click.option("--cluster", "-n", "cluster_name", required=True, help="Cluster name")
@click.option("--no-provision",
              is_flag=True,
              default=False,
              show_default=True,
              required=False,
              help="Provision Salt on the nodes")
@click.option("--plugins", is_flag=True, default=False, show_default=True, required=False, help="Install enabled plugins on provision")
@click.option("--yes", "-y", is_flag=True, default=False, help="Answers yes to questions")
def up(ctx, profile, cluster_name, no_provision, plugins, yes):
    """Start a cluster from a profile definition file

    \b
    Usage:
    adam up profile-name -n cluster-name
    """
    from adam import Cluster

    if not yes and config.cluster_exists(cluster_name) and \
       not click.confirm("A cluster named {} already exists, proceeding will overwrite this file. Continue?".format(cluster_name)):
            click.echo("No actions occurred")
            sys.exit(0)

    if config.cluster_exists(cluster_name):
        config.remove_cluster(cluster_name)

    profile = config.get_profile(profile)
    cluster = Cluster.from_profile(cluster_name, profile)

    # license.check_license_nodes(len(cluster.nodes))

    cluster.validate()
    config.save_cluster(cluster)

    if not no_provision:
        ctx.invoke(provision, cluster_name=cluster_name, plugins=plugins)
