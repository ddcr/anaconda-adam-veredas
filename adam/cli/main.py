from __future__ import print_function, division, absolute_import

import sys
import logging
import traceback
from pkg_resources import iter_entry_points

import click
from click_plugins import with_plugins

import adam
from .. import config
from ..exceptions import AdamException
from schematics.exceptions import ModelConversionError, ModelValidationError


def start():
    try:
        config.makedirs()
        cli(obj={})
    except ModelConversionError as e:
        click.echo("Error: One or more fields is not supported or recognized by "
                   "this version of Anaconda Adam. Correct or remove the "
                   "unsupported field in the cluster YAML profile or cluster "
                   "definition YAML file, and try the operation again. See the "
                   "above errors for more details: %s" % e, err=True)
        sys.exit(1)
    except ModelValidationError as e:
        click.echo("Error: One or more fields are not valid "
                   "this version of Anaconda Adam. Correct or remove the "
                   "invalid fields in the cluster YAML profile or cluster "
                   "definition YAML file, and try the operation again. See the "
                   "following errors for more details:\n %s" % e, err=True)
        sys.exit(1)
    except AdamException as e:
        click.echo("ERROR: %s" % e, err=True)
        sys.exit(1)
    except adam.libpepper.PepperException as e:
        click.echo("Salt Client Error: Unauthorized: %s" % e, err=True)
        sys.exit(1)
    except KeyboardInterrupt:
        click.echo("Interrupted by Ctrl-C. One or more actions could be still running in the cluster", err=True)
        sys.exit(1)
    except Exception as e:
        click.echo(traceback.format_exc(), err=True)
        error_msg = """An unexpected error has occurred, please contact Anaconda support at support@continuum.io

Include the following information in your support email:

- The above error output, including the command that failed to run
- The cluster definition using the command: adam describe -n cluster
"""
        click.echo(error_msg, err=True)
        sys.exit(1)


CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@with_plugins(iter_entry_points("adam.cli_plugins"))
@click.group(context_settings=CONTEXT_SETTINGS)
@click.version_option(prog_name="adam", version=adam.__version__)
@click.pass_context
@click.option("--log-level",
              "-l",
              "log_level",
              show_default=True,
              type=click.Choice(["info", "debug", "error"]),
              default="debug",
              required=False,
              help="Logging level")
def cli(ctx, log_level):
    ctx.obj = ctx.obj or {}
    if log_level == "info":
        log_level = logging.INFO
    elif log_level == "debug":
        log_level = logging.DEBUG
    elif log_level == "error":
        log_level = logging.ERROR
    adam.set_logging(log_level)


from .up import *
from .remove import *
from .provision import *
from .ssh import *
from .salt import *
from .list import *
from .cmd import *
from .info import *
from .describe import *
from .scale import *
