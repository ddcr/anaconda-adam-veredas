from __future__ import print_function, division, absolute_import

import yaml
from ..compatibility import StringIO

import click

import adam
from .main import cli
from . import utils


@cli.command(short_help="Execute a command on the nodes")
@click.pass_context
@click.argument("command", required=True)
@click.option("--cluster", "-n", "cluster_name", envvar="CLUSTERNAME", required=True, help="Cluster name")
@click.option("--target", "-t", "target", required=False, default="*", show_default=True, help="Target nodes")
def cmd(ctx, command, cluster_name, target):
    """Execute a command in the cluster nodes

    \b
    Usage:
    adam cmd -n name date
    """
    ctx.obj["cluster_name"] = cluster_name
    cluster = utils.get_cluster_object(ctx)

    arg = [command]
    response = cluster.salt(target, "cmd.run", arg=arg)

    stream = StringIO()
    yaml.safe_dump(dict(response), stream, default_flow_style=False)
    output = stream.getvalue()
    click.echo(output)
