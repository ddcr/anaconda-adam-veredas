from __future__ import print_function, division, absolute_import

import sys
from collections import OrderedDict

import click

import adam
from .main import cli


@cli.group("list", short_help="List Adam clusters and profiles", invoke_without_command=True)
@click.pass_context
def list_(ctx):
    """List adam objects. By default it lists clusters

    \b
    Usage:
    adam list
    adam list clusters     # Same as command above
    adam list profiles
    """
    if ctx.invoked_subcommand is None:
        return ctx.invoke(list_clusters)


@list_.command("clusters", short_help="List clusters")
def list_clusters():
    """List clusters
    """
    clusters = adam.get_clusters()

    if len(clusters) == 0:
        click.echo("No clusters running")
        sys.exit(0)

    for cluster in clusters:
        click.echo(cluster)


@list_.command("profiles", short_help="List profiles")
def list_profiles():
    """List profiles
    """
    profiles = adam.get_profiles()

    if len(profiles) == 0:
        click.echo("No profiles found")
        sys.exit(0)

    for profile in profiles:
        click.echo(profile)
