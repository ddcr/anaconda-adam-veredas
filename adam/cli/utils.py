from __future__ import print_function, division, absolute_import

import collections

import click
from terminaltables import AsciiTable

from .. import config

def print_states_table(cluster, response):
    """Output a table based on a response from salt
    """
    aggregated = response.group_successful()

    mixed = collections.OrderedDict()
    for node in cluster.nodes:
        mixed[node.host] = aggregated[node.host] if node.host in aggregated.keys() else "-"

    n_ok, n_fail = 0, 0
    table_data = [["Node ID", "# Successful actions", "# Failed actions"]]
    for node_id, data in mixed.items():
        if isinstance(data, dict):
            n_ok = n_ok + len(data["successful"])
            n_fail = n_fail + len(data["failed"])
            table_data.append([node_id, str(len(data["successful"])), str(len(data["failed"]))])
    table_data.append(["All nodes", str(n_ok), str(n_fail)])
    table = AsciiTable(table_data)
    table.inner_footing_row_border = True
    click.echo(table.table)

    for node_id, data in mixed.items():
        if isinstance(data, dict):
            failed = data["failed"]
            if failed:
                click.echo("Failed states for '{}'".format(node_id))
                for fail in failed:
                    name = fail["name"].replace("_|-", " > ")
                    click.echo("  {name}: {comment}".format(name=name, comment=fail["comment"]))
                    error_msg = fail.get('changes', {}).get('stderr', '')
                    if error_msg:
                        click.echo("    ERROR: {error_msg}".format(error_msg=error_msg))


def get_cluster_object(ctx, salt_password_check=True):
    """
    Utility fucntion to get cluster object and prompt for optional
    salt user password

    Parameters
    ----------
    ctx
    salt_password_check: bool, prompt check for salt password (default: True)

    Returns
    -------
    cluster object

    """

    test_cluster_exists = ctx.obj.get('cluster')
    if test_cluster_exists:
        return test_cluster_exists

    ctx.obj['cluster'] = config.get_cluster(ctx.obj['cluster_name'])

    if not ctx.obj['cluster'].salt_settings.salt_password and salt_password_check:
        password = click.prompt("Please enter salt user's password", type=str, hide_input=True)
        ctx.obj['cluster'].salt_settings.salt_password = password

    return ctx.obj['cluster']
