from __future__ import print_function, division, absolute_import

import click

import adam
from .main import cli
from . import utils


@cli.command("salt", short_help="Execute a Salt module")
@click.pass_context
@click.argument("target")
@click.argument("module")
@click.argument("args", required=False, nargs=-1)
@click.option("--cluster", "-n", "cluster_name", envvar="CLUSTERNAME", required=True, help="Cluster name")
def salt(ctx, target, module, args, cluster_name):
    """\b
    Advanced feature: Execute a salt module.
    Salt modules are available at: http://docs.saltstack.com/en/latest/ref/modules/all/
    \b
    Examples:
      adam salt "*" test.ping
      adam salt "*" network.ip_addrs eth0
    """
    import yaml
    from ..compatibility import StringIO
    from ..responses import StateResponse

    ctx.obj = ctx.obj or {}
    ctx.obj["cluster_name"] = cluster_name

    cluster = utils.get_cluster_object(ctx)

    response = cluster.salt(target, module, args)
    if module == "state.sls":
        response = response.as_cls(StateResponse)
        response.validate()
        utils.print_states_table(cluster, response)
    else:
        alive_dict = {}
        for node in cluster.nodes:
            host = node.host
            alive_dict[host] = response.get(host, False)
        stream = StringIO()
        yaml.safe_dump(alive_dict, stream, default_flow_style=False)
        output = stream.getvalue()
        click.echo(output)
