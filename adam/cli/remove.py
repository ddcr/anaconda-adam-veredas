from __future__ import print_function, division, absolute_import

import sys

import click

from .. import config
from .main import cli
from .provision import provision
from adam import license


@cli.command("remove", short_help="Remove a cluster")
@click.pass_context
@click.option("--cluster", "-n", "cluster_name", required=True, help="Cluster name")
@click.option("--yes", "-y", is_flag=True, default=False, help="Answers yes to questions")
def remove(ctx, cluster_name, yes):
    """Remove a cluster definition file
    """
    from adam import Cluster

    if not config.cluster_exists(cluster_name):
        click.echo("Cluster {} doesn't exists", err=True)
        sys.exit(1)

    if yes or click.confirm("Are you sure you want to remove the cluster {}?".format(cluster_name)):
        config.remove_cluster(cluster_name)
