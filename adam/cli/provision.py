from __future__ import print_function, division, absolute_import

import collections

import click
from terminaltables import AsciiTable

from .. import config
from .main import cli
from . import utils
from .. import salt
from adam import ssh
from adam.exceptions import RetriesExceededException


@cli.group("provision", invoke_without_command=True, short_help="Provision options")
@click.pass_context
@click.option("--cluster", "-n", "cluster_name", envvar="CLUSTERNAME", required=True, help="Cluster name")
@click.option("--flush-iptables", "flush_iptables", is_flag=True, default=False, show_default=True, required=False, help="Flush iptables")
@click.option("--selinux-permissive", "selinux_permissive", is_flag=True, default=False, show_default=True, required=False, help="Set SELinux to permissive")
@click.option("--selinux-context", "selinux_context", is_flag=True, default=False, show_default=True, required=False, help="Set SELinux contexts")
@click.option("--plugins", is_flag=True, default=False, show_default=True, required=False, help="Install enabled plugins on provision")
def provision(ctx, cluster_name, flush_iptables, selinux_permissive, selinux_context, plugins):
    """Execute all the provisioning steps for a cluster

    See subcommands help for more info.
    \b
    Usage:
    adam provision -n
    """
    ctx.obj = ctx.obj or {}
    ctx.obj["cluster_name"] = cluster_name
    utils.get_cluster_object(ctx)

    if ctx.invoked_subcommand is None:
        ctx.invoke(check_ssh)
        ctx.invoke(anaconda_user)
        ctx.invoke(provision_salt, flush_iptables=flush_iptables, selinux_permissive=selinux_permissive, selinux_context=selinux_context)
        if plugins:
            ctx.invoke(provision_plugins)


@provision.command("check-ssh", short_help="Check SSH connection to the nodes")
@click.pass_context
def check_ssh(ctx):
    """Check SSH connectivity to all the cluster nodes
    """
    cluster = utils.get_cluster_object(ctx, salt_password_check=False)
    click.echo("Checking SSH connection to nodes")
    results = cluster.check_ssh()
    table = nodes_table(cluster, results, header="SSH Available")
    click.echo(table.table)

    click.echo("Checking basic sudo command")
    clients = [node.ssh_client for node in cluster.nodes]
    results = ssh.parallel_exec_command(clients, "echo 'anaconda-adam'", sudo=True)
    check_ok = lambda x: x["exit_code"] == 0 and x["stdout"] == "anaconda-adam"
    table = nodes_table(cluster, results, header="Sudo check OK", check_ok=check_ok)
    click.echo(table.table)

    click.echo("Checking id command")
    clients = [node.ssh_client for node in cluster.nodes]
    results = ssh.parallel_exec_command(clients, "id", sudo=True)
    check_ok = lambda x: x["exit_code"] == 0 and x["stdout"] != ""
    table = nodes_table(cluster, results, header="Check `id` command", check_ok=check_ok)
    click.echo(table.table)

    click.echo("Checking getent command")
    clients = [node.ssh_client for node in cluster.nodes]
    results = ssh.parallel_exec_command(clients, "getent --help", sudo=True)
    check_ok = lambda x: x["exit_code"] == 0 and x["stdout"] != ""
    table = nodes_table(cluster, results, header="Check `getent` command", check_ok=check_ok)
    click.echo(table.table)

    click.echo("Checking groupadd command")
    clients = [node.ssh_client for node in cluster.nodes]
    # We expect `groupadd --help` to return exit code 2
    results = ssh.parallel_exec_command(clients, "groupadd --help || [ $? == 2 ]", sudo=True)
    check_ok = lambda x: "Usage: groupadd" in x["stdout"]
    table = nodes_table(cluster, results, header="Check `groupadd` command", check_ok=check_ok)
    click.echo(table.table)

    click.echo("Checking useradd command")
    clients = [node.ssh_client for node in cluster.nodes]
    # We expect `useradd --help` to return exit code 2
    results = ssh.parallel_exec_command(clients, "useradd --help || [ $? == 2 ]", sudo=True)
    check_ok = lambda x: "Usage: useradd" in x["stdout"]
    table = nodes_table(cluster, results, header="Check `useradd` command", check_ok=check_ok)
    click.echo(table.table)

    click.echo("Checking openssl command")
    clients = [node.ssh_client for node in cluster.nodes]
    results = ssh.parallel_exec_command(clients, "openssl --help", sudo=True)
    check_ok = lambda x: x["exit_code"] == 0 and x["stdout"] != ""
    table = nodes_table(cluster, results, header="Check `openssl` command", check_ok=check_ok)
    click.echo(table.table)


@provision.command("anaconda-user", short_help="Create the default anaconda user in all the nodes")
@click.pass_context
def anaconda_user(ctx):
    """Create the default `anaconda` user in the nodes
    """
    cluster = utils.get_cluster_object(ctx, salt_password_check=False)

    click.echo("Creating anaconda group")
    salt.create_anaconda_group(cluster)

    click.echo("Checking anaconda group")
    results = salt.check_anaconda_group(cluster)
    table = nodes_table(cluster, results, header="Anaconda group")
    click.echo(table.table)

    click.echo("Creating anaconda user")
    salt.create_anaconda_user(cluster)

    click.echo("Checking anaconda user")
    results = salt.check_anaconda_user(cluster)
    table = nodes_table(cluster, results, header="Anaconda user")
    click.echo(table.table)


@provision.command("security-settings", short_help="Execute the security settings")
@click.pass_context
def provision_security_settings(ctx):
    """Execute the security settings that are enabled in the cluster yaml settings
    """
    cluster = utils.get_cluster_object(ctx, salt_password_check=False)

    if cluster.security.flush_iptables:
        click.echo("iptables flush enabled")
        ctx.invoke(flush_iptables)
    else:
        click.echo("iptables flush not enabled")
    if cluster.security.selinux_permissive:
        click.echo("SELinux permissive mode enabled")
        ctx.invoke(selinux_permissive)
    else:
        click.echo("SELinux permissive mode not enabled")
    if cluster.security.selinux_context:
        click.echo("SELinux context enabled")
        ctx.invoke(selinux_context)
    else:
        click.echo("SELinux context not enabled")


@provision.command("flush-iptables", short_help="Flush iptables")
@click.pass_context
def flush_iptables(ctx):
    """Flush iptables on all nodes.

    Note: This will run independent of the cluster yaml settings
    """
    cluster = utils.get_cluster_object(ctx, salt_password_check=False)
    click.echo("Flushing iptables")
    salt.flush_iptables(cluster)
    click.echo("Flushed iptables on all nodes")


@provision.command("selinux-permissive", short_help="Set SELinux to permissive")
@click.pass_context
def selinux_permissive(ctx):
    """Set SELinux to permissive

    Note: This will run independent of the cluster yaml settings
    """
    cluster = utils.get_cluster_object(ctx, salt_password_check=False)
    click.echo("Setting SELinux to permissive")
    salt.set_selinux_permissive(cluster)
    click.echo("SELinux was set to permissive on all nodes")


@provision.command("selinux-context", short_help="Set SELinux contexts")
@click.pass_context
def selinux_context(ctx):
    """Set the SELinux context for the salt executables

    Note: This will run independent of the cluster yaml settings
    """
    cluster = utils.get_cluster_object(ctx, salt_password_check=False)
    click.echo("Setting SELinux context")
    salt.set_selinux_context(cluster)
    click.echo("SELinux context was set on all nodes")


@provision.command("salt", short_help="Provision Salt")
@click.option("--flush-iptables", "flush_iptables", is_flag=True, default=False, show_default=True, required=False, help="Flush iptables")
@click.option("--selinux-permissive", "selinux_permissive", is_flag=True, default=False, show_default=True, required=False, help="Set SELinux to permissive")
@click.option("--selinux-context", "selinux_context", is_flag=True, default=False, show_default=True, required=False, help="Set SELinux contexts")
@click.pass_context
def provision_salt(ctx, flush_iptables, selinux_permissive, selinux_context):
    """Execute the provisioning steps to install salt

    Requires:
        - anaconda-user

    Also executes:
        - sync
        - roles
    """
    cluster = utils.get_cluster_object(ctx)

    click.echo("Installing miniconda for salt")
    salt.install_miniconda(cluster)

    click.echo("Configuring agent's conda settings")
    salt.configure_condarc(cluster)

    click.echo("Installing and starting salt-master and salt-minions")
    salt.install_salt(cluster, force_flush_iptables=flush_iptables, force_selinux_permissive=selinux_permissive, force_selinux_context=selinux_context)

    click.echo("Checking salt installation")
    results = salt.check_salt_running(cluster)
    table = nodes_table(cluster, results, header="Salt-minion running")
    click.echo(table.table)

    click.echo("Checking salt-minion connection")
    results = salt.check_salt_api_connection(cluster)
    table = nodes_table(cluster, results, header="Salt minion-connected")
    click.echo(table.table)

    ctx.invoke(provision_sync)
    ctx.invoke(provision_roles)

    if cluster.salt_settings.service_scripts:
        results = salt.install_service_scripts(cluster)
        n_results = {node: not bool(result['failed']) for node, result in results.group_successful().items()}
        table = nodes_table(cluster, n_results, header="Service Script Install")
        click.echo(table.table)


@provision.command("sync", short_help="Sync formulas and settings")
@click.pass_context
@click.option("--licenses-only",
              "licenses_only_",
              default=False,
              show_default=True,
              required=False,
              is_flag=True,
              help="Only sync license files")
def provision_sync(ctx, licenses_only_):
    """Sync (upload) the salt formulas and cluster settings

    \b
    Requires:
        - salt
    """
    cluster = utils.get_cluster_object(ctx)

    click.echo("Uploading license files")
    salt.upload_licenses(cluster)
    if licenses_only_:
        return

    click.echo("Uploading salt formulas")
    salt.upload_formulas(cluster)

    click.echo("Uploading settings")
    salt.upload_settings(cluster)

    click.echo("Checking conda formulas")
    results = cluster.salt("*", "conda.prefix")
    table = nodes_table(cluster, results, header="Conda module", check_ok=lambda x: x == cluster.anaconda_dir)
    click.echo(table.table)


@provision.command("roles", short_help="Set roles on the cluster nodes")
@click.pass_context
def provision_roles(ctx):
    """Set roles for different nodes based on plugins

    \b
    Requires:
        - salt
    """
    cluster = utils.get_cluster_object(ctx)
    click.echo("Setting roles on the cluster nodes")
    salt.set_roles(cluster)


@provision.command("plugins", short_help="Provision enabled plugins")
@click.pass_context
def provision_plugins(ctx):
    """Install all enabled plugins
    """
    from adam.plugin_models import get_plugins_modules
    plugins_modules = get_plugins_modules()
    cluster = utils.get_cluster_object(ctx)

    for p in plugins_modules:
        try:
            models = getattr(p[1], "MODELS")
            for model_name, _ in models.items():
                if getattr(cluster, model_name).enabled:
                    func = getattr(p[1], "CLI_ENABLE_INSTALL")
                    ctx.invoke(func)
        except AttributeError:
            pass  # Plugins didnt defined any default installation


def nodes_table(cluster, results, header="Status", agg=True, check_ok=lambda x: x is True):
    """
    Generate a table based on the cluster nodes and the status on a service

    Arguments
    ---------
        cluster: adam.Cluster
        results: Dictionary like: `{node_id: status}`

    Returns
    -------
        AsciiTable
    """
    mixed = collections.OrderedDict()
    for node in cluster.nodes:
        mixed[node.host] = results[node.host] if node.host in results.keys() else "-"

    all_ok = True
    table_data = [["Node IP", header]]
    for node_host, status in mixed.items():
        if isinstance(status, RetriesExceededException):
            table_data.append([node_host, "Error: {}".format(status.last_exception)])
            all_ok = False
        else:
            table_data.append([node_host, str(check_ok(status))])

    table = AsciiTable(table_data)
    if agg:
        table_data.append(["All nodes", str(all_ok)])
        table.inner_footing_row_border = True
    return table
