from __future__ import print_function, division, absolute_import

import os
import json
import time
import shutil
import logging
import tempfile
from hashlib import md5
from contextlib import contextmanager

from .exceptions import RetriesExceededException

logger = logging.getLogger(__name__)


def set_logging(level):
    format = ("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    logging.basicConfig(format=format, level=logging.WARNING)

    logging.getLogger("paramiko").setLevel(logging.WARNING)
    logger = logging.getLogger("adam")

    logger.setLevel(level)


def retry(retries=10, wait=5, catch=None):
    """
    Decorator to retry on exceptions raised
    """
    catch = catch or (Exception,)

    def real_retry(function):

        def wrapper(*args, **kwargs):
            last_exception = None

            for attempt in range(1, retries + 1):
                try:
                    ret = function(*args, **kwargs)
                    return ret
                except catch as e:
                    last_exception = e
                    logger.debug("Attempt %i/%i of function '%s' failed", attempt, retries, function.__name__)
                    logger.debug("Reason: %s", str(e))
                    time.sleep(wait)
                except Exception as e:
                    raise e
            else:
                raise RetriesExceededException(function=function, last_exception=last_exception)

        return wrapper

    return real_retry


def hash_dict(dd):
    return md5(json.dumps(dd, sort_keys=True).encode("utf-8")).hexdigest()


@contextmanager
def tmpfile(extension="", dir=None):
    extension = "." + extension.lstrip(".")
    handle, filename = tempfile.mkstemp(extension, dir=dir)
    os.close(handle)
    os.remove(filename)

    yield filename

    if os.path.exists(filename):
        if os.path.isdir(filename):
            shutil.rmtree(filename)
        else:
            try:
                os.remove(filename)
            except OSError:  # sometimes we can't remove a generated temp file
                pass
