from adam.tests.utils import remotetest, cluster, invoke


@remotetest
def test_salt_ping(cluster):
    result = invoke(["salt", "*", "test.ping"])
    assert result.exit_code == 0

    for node in cluster.nodes:
        assert "{}: true".format(node.host) in result.output
