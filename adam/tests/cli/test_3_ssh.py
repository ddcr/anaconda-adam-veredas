from adam.tests.utils import invoke, remotetest, dockertest


@remotetest
def test_ssh_nonexistent_node():
    result = invoke(["ssh", "9999"])
    assert "Error: Cluster only has" in result.output
    assert result.exit_code == 1


@remotetest
@dockertest
def test_ssh_inline():
    result = invoke(["ssh", "-c", "whoami"])
    assert result.exit_code == 0
    assert "root" in result.output.strip()
