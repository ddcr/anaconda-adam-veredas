from adam.tests.utils import invoke, remotetest


@remotetest
def test_main_cli():
    result = invoke(["-h"], clustername_flag=False)
    assert result.exit_code == 0

    result = invoke(["--help"], clustername_flag=False)
    assert result.exit_code == 0

    result = invoke(["info"], log_level="info", clustername_flag=False)
    assert result.exit_code == 0

    result = invoke(["info"], log_level="debug", clustername_flag=False)
    assert result.exit_code == 0

    result = invoke(["info"], log_level="error", clustername_flag=False)
    assert result.exit_code == 0

    result = invoke("info", log_level="another", clustername_flag=False)
    assert 'Invalid value for "--log-level" / "-l"' in result.output
    assert result.exit_code == 2
