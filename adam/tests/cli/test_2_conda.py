import os
import pytest
import posixpath

import adam
from adam.tests.utils import invoke, remotetest, cluster

cur_dir = os.path.dirname(__file__)


@remotetest
def test_install_anaconda_scale_cluster():
    result = invoke(["scale", "cluster", "install"])
    assert result.exit_code == 0, result.output


@remotetest
def test_conda_main():
    result = invoke(["scale", "conda"])
    assert result.exit_code == 0


@remotetest
def test_conda_info():
    result = invoke(["scale", "conda", "info"])
    assert "version" in result.output
    assert not result.exception
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "info", "-e"])
    assert "environments" in result.output
    assert not result.exception
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "info", "-s"])
    assert "PYTHONPATH" in result.output
    assert not result.exception
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "info", "-a"])
    assert "version" in result.output
    assert "environments" in result.output
    assert "PYTHONPATH" in result.output
    assert not result.exception
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "info", "--license"])
    assert result.exit_code == 0


@remotetest
def test_conda_list():
    result = invoke(["scale", "conda", "list"])
    assert not result.exception
    assert result.exit_code == 0


@remotetest
def test_conda_create_wrong_arguments(cluster):
    result = invoke(["scale", "conda", "create"])
    assert 'Error: Missing argument "packages".' in result.output
    assert result.exit_code == 2

    result = invoke(["scale", "conda", "create", "numpy"])
    assert "Error: either -n/--name or -p/--prefix option required" in result.output
    assert result.exit_code == 1

    result = invoke(["scale", "conda", "create", "numpy", "-n", "name", "-p", "prefix"])
    assert "Error: argument -p/--prefix: not allowed with argument -n/--name" in result.output
    assert result.exit_code == 1


@remotetest
def test_conda_list_wrong_arguments(cluster):
    result = invoke(["scale", "conda", "list", "-n", "name", "-p", "prefix"])
    assert "Error: argument -p/--prefix: not allowed with argument -n/--name" in result.output
    assert result.exit_code == 1


@remotetest
@pytest.mark.parametrize("subcommand", ["install", "update", "remove"])
def test_conda_wrong_arguments(cluster, subcommand):
    result = invoke(["scale", "conda", subcommand])
    assert 'Error: Missing argument "packages".' in result.output
    assert result.exit_code == 2

    result = invoke(["scale", "conda", subcommand, "numpy", "-n", "name", "-p", "prefix"])
    assert "Error: argument -p/--prefix: not allowed with argument -n/--name" in result.output
    assert result.exit_code == 1


@remotetest
def test_conda_remove(request, cluster):
    testname = request.node.name

    # no arguments
    result = invoke(["scale", "conda", "remove"])
    assert result.exit_code == 2

    # just --all
    result = invoke(["scale", "conda", "remove", "--all"])
    assert result.exit_code == 1

    # packages and --all
    result = invoke(["scale", "conda", "remove", "foo", "--all"])
    assert result.exit_code == 1

    result = invoke(["scale", "conda", "create", "-n", "flowers", "python=2 ldap3"])
    assert result.exit_code == 0
    assert 'flowers' in result.output

    # remove package with cli (note: will also test in test_conda_remote.py)
    result = invoke(["scale", "conda", "remove", "-n", "flowers", "ldap3"])
    assert result.exit_code == 0
    assert 'flowers' in result.output

    result = invoke(["scale", "conda", "create", "-n", "flowers", "python=2"])
    assert result.exit_code == 0
    assert 'flowers' in result.output

    # env and --all -- should work cleanly
    result = invoke(["scale", "conda", "remove", "-n", "flowers", "--all"])
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "info", "-e"])
    assert 'flowers' not in result.output

    # uncomment when using conda 4.2.11 or greater
    # remove a non-existing env
    # result = invoke(["scale", "conda", "remove", "-n", "flowers", "--all"])
    # assert "Could not find environment: flowers" in result.output
    # assert result.exit_code == 0


@remotetest
def test_conda_everything(request, cluster):
    testname = request.node.name

    result = invoke(["scale", "conda", "create", "-n", testname, "numpy==1.7.1"])
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "list", "-n", testname])
    assert "numpy" in result.output
    assert get_version(result.output, "numpy") == "1.7.1"
    assert "scipy" not in result.output
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "install", "-n", testname, "scipy==0.15.0"])
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "update", "-n", testname, "numpy"])
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "list", "-n", testname])
    assert result.exit_code == 0
    assert version_gt(get_version(result.output, "numpy"), "1.7.1")

    result = invoke(["scale", "conda", "remove", "-n", testname, "numpy", "scipy"])
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "list", "-n", testname])
    assert result.exit_code == 0
    assert "numpy" not in result.output
    assert "scipy" not in result.output

    def fin():
        for node in cluster.nodes:
            client = node.ssh_client
            env = posixpath.join(cluster.rootdir, "anaconda", "envs", testname)
            response = client.exec_command("rm -rf {}".format(env), sudo=True)
            assert response["exit_code"] == 0
            client.close()

    request.addfinalizer(fin)



@remotetest
def test_conda_env(request, cluster):
    result = invoke(["scale", "env"])
    assert result.exit_code == 0

    result = invoke(["scale", "env"])
    conda_envs = os.path.join(cur_dir, "..", "files", "conda_env_ymls")

    missing_names = os.path.join(conda_envs, "malformed.yml")
    result = invoke(["scale", "env", "create", "-f", missing_names])
    assert result.exit_code == 1

    missing_names = os.path.join(conda_envs, "missing_name.yml")
    result = invoke(["scale", "env", "create", "-f", missing_names])
    assert result.exit_code == 2

    pip_deps = os.path.join(conda_envs, "pip_deps.yml")
    result = invoke(["scale", "env", "create", "-f", pip_deps])
    assert "WARNING: pip dependencies are not supported, skipping" in result.output
    assert result.exit_code == 0

    javascript = os.path.join(conda_envs, "javascript.yml")
    result = invoke(["scale", "env", "create", "-f", javascript])
    assert result.exit_code == 0

    result = invoke(["scale", "conda", "list", "-n", "cenv_test_javscript"])
    assert "nodejs" in result.output
    assert result.exit_code == 0

    javascript_update = os.path.join(conda_envs, "javascript_update.yml")
    result = invoke(["scale", "env", "update", "-f", javascript_update])
    assert result.exit_code == 0

    result = invoke(["scale", "env", "remove", "-n", "cenv_test_javscript"])
    assert result.exit_code == 0

    result = invoke(["scale", "env", "list"])
    assert "cenv_test_javscript" not in result.output
    assert result.exit_code == 0


    def fin():
        for node in cluster.nodes:
            client = node.ssh_client
            env = posixpath.join(cluster.rootdir, "anaconda", "envs", "cenv_test_*")
            response = client.exec_command("rm -rf {}".format(env), sudo=True)
            assert response["exit_code"] == 0
            client.close()

    request.addfinalizer(fin)



def get_version(list_output, pkg_name):
    for line in list_output.split("\n"):
        if pkg_name in line:
            parts = line.split("|")
            return parts[2].strip()


def version_gt(version1, version2):
    """Compare if one string versions are greater than the other
    """
    from distutils.version import StrictVersion
    return StrictVersion(version1) > StrictVersion(version2)
