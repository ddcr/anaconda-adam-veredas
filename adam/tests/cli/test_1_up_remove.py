from adam.tests.utils import invoke, remotetest


@remotetest
def test_up_remove():
    import os
    import adam
    profile_name = os.environ["TEST_PROFILENAME"]
    cluster_name = "test_up_remove"
    result = invoke(["up", profile_name, "-n", cluster_name, "--no-provision"], clustername_flag=False)
    assert result.exit_code == 0, result.output
    assert adam.cluster_exists(cluster_name) is True
    result = invoke(["remove", "-n", cluster_name, "--yes"], clustername_flag=False)
    assert adam.cluster_exists(cluster_name) is False


@remotetest
def test_up_remove_nonexistent():
    result = invoke(["remove", "-n", "wakawaka", "--yes"], clustername_flag=False)
    assert result.exit_code == 1, result.output
