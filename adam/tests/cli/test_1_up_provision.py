from adam.tests.utils import invoke, remotetest


@remotetest
def test_up_provision():
    """
    Note: Cluster file already exists because of `adam/tests/test_0_setup.py`
    """
    import os
    profile_name = os.environ["TEST_PROFILENAME"]
    cluster_name = os.environ["TEST_CLUSTERNAME"]

    # Invoke the first time to see the duplicated message - Don't provision this time
    result = invoke(["up", profile_name, "-n", cluster_name, "--no-provision"], clustername_flag=False)
    assert result.exit_code == 0, result.output
    assert "A cluster named {} already exists".format(cluster_name) in result.output

    # Invoke with --yes to force - DO provision this time
    result = invoke(["up", profile_name, "-n", cluster_name, "--yes"], clustername_flag=False)
    assert result.exit_code == 0, result.output
