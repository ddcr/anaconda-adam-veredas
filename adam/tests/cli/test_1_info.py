from adam.tests.utils import invoke, remotetest, dockertest


@remotetest
def test_info():
    import adam
    result = invoke(["info"], clustername_flag=False)
    assert adam.__version__ in result.output
    assert result.exit_code == 0
