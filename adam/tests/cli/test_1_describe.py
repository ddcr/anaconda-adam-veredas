from adam.tests.utils import invoke, remotetest


def test_describe_main():
    result = invoke(["describe"], clustername_flag=False)
    assert result.exit_code == 1, result.output


@remotetest
def test_describe_clusters():
    import os
    result = invoke(["describe", "-n", os.environ["TEST_CLUSTERNAME"]], clustername_flag=False)
    assert result.exit_code == 0, result.output


@remotetest
def test_describe_profiles():
    import os
    result = invoke(["describe", "-n", os.environ["TEST_PROFILENAME"]], clustername_flag=False)
    assert result.exit_code == 0, result.output
