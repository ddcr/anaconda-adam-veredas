from adam.tests.utils import invoke, remotetest


@remotetest
def test_list_main():
    result = invoke(["list"], clustername_flag=False)
    assert result.exit_code == 0, result.output


@remotetest
def test_list_clusters():
    result = invoke(["list", "clusters"], clustername_flag=False)
    assert result.exit_code == 0, result.output


@remotetest
def test_list_profiles():
    result = invoke(["list", "profiles"], clustername_flag=False)
    assert result.exit_code == 0, result.output
