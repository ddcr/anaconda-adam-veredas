from adam.tests.utils import invoke, remotetest, dockertest


@remotetest
def test_cmd():
    result = invoke(["cmd", "date"])
    assert result.exit_code == 0
