import os

import pytest
from schematics.exceptions import ModelValidationError

import adam
from adam import Cluster
from adam.models import Conda, SaltSettings
from .utils import test_config

cur_dir = os.path.dirname(__file__)


def test_bare_simple(test_config):
    simple_bare_fp = os.path.join(cur_dir, "files", "profile.d", "bare_simple.yml")
    profile = adam.get_profile("bare_simple")

    profile.to_primitive() == adam.load_yml(simple_bare_fp)

    cluster = Cluster.from_profile("bare_simple", profile)
    cluster.validate()

    assert cluster.conda == Conda()
    assert cluster.salt_settings == SaltSettings()


def test_bare_simple_overwrites(test_config):
    overwrite_fp = os.path.join(cur_dir, "files", "profile.d", "bare_overwrites.yml")
    profile = adam.get_profile("bare_overwrites")

    profile.to_primitive() == adam.load_yml(overwrite_fp)
    cluster = Cluster.from_profile("bare_overwrites", profile)
    cluster.validate()

    assert cluster.nodes[0].port == 2222
    assert cluster.nodes[1].port == 2223
    assert cluster.conda == Conda()


def test_bare_and_conda(test_config):
    overwrite_fp = os.path.join(cur_dir, "files", "profile.d", "bare_and_conda.yml")
    profile = adam.get_profile("bare_and_conda")

    profile.to_primitive() == adam.load_yml(overwrite_fp)
    cluster = Cluster.from_profile("bare_and_conda", profile)
    cluster.validate()

    assert profile.plugins.conda.to_primitive() == adam.load_yml(overwrite_fp)["plugins"]["conda"]
    assert cluster.salt_settings == SaltSettings()


def test_bare_and_salt(test_config):
    overwrite_fp = os.path.join(cur_dir, "files", "profile.d", "bare_and_salt.yml")
    profile = adam.get_profile("bare_and_salt")

    profile.to_primitive() == adam.load_yml(overwrite_fp)
    cluster = Cluster.from_profile("bare_and_salt", profile)
    cluster.validate()

    assert profile.plugins.salt_settings.to_primitive() == adam.load_yml(overwrite_fp)["plugins"]["salt_settings"]
