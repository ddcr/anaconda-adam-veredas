"""
Tests for the licensing module only run on Travis CI since they modify ~/.continuum
"""
import pytest

from .utils import travis_ci, use_license


@travis_ci
def test_valid():
    from adam import license
    use_license("license_bundle_20160524191753.txt")
    license.check_valid_product_license("Anaconda Cluster")
    license.check_valid_product_license("Anaconda Repository Enterprise")
    license.check_valid_product_license("Wakari Enterprise")


@travis_ci
def test_valid_2():
    from adam import license
    use_license("license_bundle_20160707161854.txt")
    license.check_valid_product_license("Anaconda Cluster")
    license.check_valid_product_license("Anaconda Enterprise Repository")
    license.check_valid_product_license("Anaconda Enterprise Notebooks")


@travis_ci
def test_expired():
    from adam import license
    use_license("license_bundle_20160524191743.txt")
    license.check_valid_product_license("Anaconda Cluster")


@travis_ci
def test_decorator():
    from adam import license

    use_license("license_bundle_20160524191753.txt")

    @license.check_license_decorator(("Anaconda Cluster", "Anaconda Repository Enterprise",))
    def noop():
        return True

    assert noop() is True
