from __future__ import absolute_import, print_function, division

import pytest

from adam import Node
from .utils import remotetest, cluster

from schematics.exceptions import ModelValidationError

valid = {"host": "127.0.0.1", "username": "root", "keypair": "~/.ssh/keypair"}
valid2 = {"host": "1.1.1.1", "username": "root", "keypair": "~/.ssh/keypair", "port": 2222, "id": "i-123"}
valid_complete = {"host": "1.1.1.1",
                  "username": "root",
                  "keypair": "~/.ssh/keypair",
                  "password": "root",
                  "port": 2222,
                  "id": "i-123"}

invalid_ssh = {"host": "127.0.0.1", "username": "root"}


def test_node_valid():
    import json

    node = Node()
    assert node.port == 22
    with pytest.raises(ModelValidationError):
        node.validate()

    node = Node(valid)
    node.validate()

    node = Node(valid_complete)
    assert json.loads(repr(node)) == node.to_primitive()


def test_node_invalid():
    with pytest.raises(ModelValidationError):
        node = Node(invalid_ssh)
        node.validate()


def test_dict_serde():
    node = Node(valid2)
    node.validate()
    node2 = Node(node.to_primitive())
    assert node == node2
    assert node.host == "1.1.1.1"
    assert node.id == "i-123"
    assert node.port == 2222
    assert node.username == "root"
    assert node.keypair == "~/.ssh/keypair"


@remotetest
def test_check_ssh(cluster):
    head = cluster.head
    assert head.check_ssh() == True
