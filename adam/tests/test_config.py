import adam
from adam.tests.utils import remotetest


@remotetest
def test_load_configs():
    import os

    profiles = adam.get_profiles()
    assert len(profiles) >= 1
    assert adam.profile_exists(os.environ["TEST_PROFILENAME"]) is True
    _ = adam.get_profile(os.environ["TEST_PROFILENAME"])

    profiles = adam.get_clusters()
    assert len(profiles) >= 1
    assert adam.cluster_exists(os.environ["TEST_CLUSTERNAME"]) is True
    _ = adam.get_cluster(os.environ["TEST_CLUSTERNAME"])


@remotetest
def test_cluster_save_delete():
    import os
    assert adam.cluster_exists(os.environ["TEST_CLUSTERNAME"]) is True
    cluster = adam.get_cluster(os.environ["TEST_CLUSTERNAME"])

    new_cluster_name = "test_cluster_save_delete"
    cluster.name = new_cluster_name

    adam.save_cluster(cluster)
    assert adam.cluster_exists(new_cluster_name) is True

    adam.remove_cluster(new_cluster_name)
    assert adam.cluster_exists(new_cluster_name) is False
