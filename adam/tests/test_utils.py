import adam


def test_logger():
    import logging
    logger = logging.getLogger("adam")
    assert logger.level == logging.INFO
    adam.set_logging(logging.DEBUG)
    assert logger.level == logging.DEBUG
    adam.set_logging(logging.INFO)
