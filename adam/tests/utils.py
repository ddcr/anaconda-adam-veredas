"""
Utilities for testing
"""
import os
import shutil
import tempfile
from contextlib import contextmanager

from adam.exceptions import AdamException
import pytest

remotetest = pytest.mark.skipif("TEST_CLUSTERNAME" not in os.environ,
                                reason="Environment variable 'TEST_CLUSTERNAME' is required")


def is_docker_test_env():
    import adam
    if "TEST_CLUSTERNAME" in os.environ:
        clustername = os.environ["TEST_CLUSTERNAME"]

        try:
            cluster = adam.get_cluster(clustername)
        except AdamException as e:
            # handle setting clustername before it"s been populated by test
            # script -- We probably are testing with docker
            if "TEST_PROFILENAME" in os.environ:
                return True
            else:
                raise AdamException(e.message)

        if len(cluster.nodes) != 2:
            return False
        if cluster.nodes[0].host != "ci_head_1":
            return False
        if cluster.nodes[1].host != "ci_compute_1":
            return False
        return True
    else:
        return False


nodockertest = pytest.mark.skipif(is_docker_test_env(), reason="This test is hardcoded to the docker test setup")

dockertest = pytest.mark.skipif(not is_docker_test_env(), reason="This test is hardcoded to the docker test setup")

travis_ci = pytest.mark.skipif("CI" not in os.environ or "TRAVIS" not in os.environ,
                               reason="This tests should only run on travis ci")


@pytest.yield_fixture(scope="module")
def cluster():
    import adam
    if "TEST_CLUSTERNAME" in os.environ:
        clustername = os.environ["TEST_CLUSTERNAME"]
        yield adam.get_cluster(clustername)




runner = None


def invoke(args=None, log_level="info", clustername_flag=True):
    """Invoke a command in the CLI. Return the results
    """
    from click.testing import CliRunner
    from adam.cli.main import cli
    global runner
    if runner is None:
        runner = CliRunner()

    args = args or []

    params = ["-l", log_level]
    if clustername_flag:
        params.append(args[0])
        params.extend(["-n", os.environ["TEST_CLUSTERNAME"]])
        params.extend(args[1:])
    else:
        params.extend(args)
    results = runner.invoke(cli, params)
    return results


@pytest.yield_fixture(scope="module")
def test_config():
    """
    User the test files in `adam/tests/files`
    """
    from adam import config
    BASE_DIR = os.path.dirname(__file__)
    FILES_DIR = os.path.join(BASE_DIR, "files")

    original_ROOT_DIR = config.ROOT_DIR
    original_PROFILE_DIR = config.PROFILE_DIR
    original_CLUSTER_DIR = config.CLUSTER_DIR

    config.ROOT_DIR = os.path.expanduser(FILES_DIR)
    config.PROFILE_DIR = os.path.join(config.ROOT_DIR, "profile.d")
    config.CLUSTER_DIR = os.path.join(config.ROOT_DIR, "cluster.d")

    yield config.ROOT_DIR

    config.ROOT_DIR = original_ROOT_DIR
    config.PROFILE_DIR = original_PROFILE_DIR
    config.CLUSTER_DIR = original_CLUSTER_DIR


@pytest.yield_fixture(scope="module")
def tmp_config():
    """
    Create a temporal configuration directory.
    Its deleted after the tests run
    """
    from adam import config
    TMP_DIR = tempfile.mkdtemp("tmp")

    original_ROOT_DIR = config.ROOT_DIR
    original_PROFILE_DIR = config.PROFILE_DIR
    original_CLUSTER_DIR = config.CLUSTER_DIR

    config.ROOT_DIR = os.path.expanduser(TMP_DIR)
    config.PROFILE_DIR = os.path.join(config.ROOT_DIR, "profile.d")
    config.CLUSTER_DIR = os.path.join(config.ROOT_DIR, "cluster.d")
    config.makedirs()

    yield config.ROOT_DIR

    config.ROOT_DIR = original_ROOT_DIR
    config.PROFILE_DIR = original_PROFILE_DIR
    config.CLUSTER_DIR = original_CLUSTER_DIR

    if os.path.exists(TMP_DIR):
        shutil.rmtree(TMP_DIR)


def ssh_dir():
    """
    use valid rsa key
    """
    BASE_DIR = os.path.dirname(__file__)
    SSH_DIR = os.path.join(BASE_DIR, "files", "ssh_keys")
    return SSH_DIR

@contextmanager
def tmpfile(extension="", dir=None):
    extension = "." + extension.lstrip(".")
    handle, filename = tempfile.mkstemp(extension, dir=dir)
    os.close(handle)
    os.remove(filename)

    yield filename

    if os.path.exists(filename):
        if os.path.isdir(filename):
            shutil.rmtree(filename)
        else:
            try:
                os.remove(filename)
            except OSError:  # sometimes we can"t remove a generated temp file
                pass


def use_license(name):
    """
    Use a license from the test data directory.
    If name is None all licenses are removed and no license will be found

    WARNING: This will remove all licenses from ~/.continuum
    """
    import glob
    from shutil import copyfile

    # Remove all licenses
    license_dir = os.path.expanduser("~/.continuum")
    all_licenses = glob.glob(os.path.join(license_dir, "license_bundle*.txt"))
    for license_path in all_licenses:
        os.remove(license_path)

    if name is not None:
        # Add a new license
        cur_dir = os.path.dirname(__file__)
        src = os.path.join(cur_dir, "data", "licenses", name)
        dst = os.path.join(license_dir, name)
        copyfile(src, dst)


def assert_response_ok(response):
    """
    For a salt state.sls check all states result is True
    """
    for minion_id, states in response.items():
        for state, values in states.items():
            assert values["result"] is True, values


def assert_response_fail(response):
    """
    For a salt state.sls check all responses where not successful
    """
    for minion_id, states in response.items():
        for state, values in states.items():
            assert (values["result"] is None) or (values["result"] is False), values


def assert_response_no_changes(response):
    """
    For a salt state.sls check all states and checks that there were no changes
    """
    for minion_id, states in response.items():
        for state, values in states.items():
            assert len(values["changes"]) == 0, values
