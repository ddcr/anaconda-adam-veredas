from adam.tests.utils import invoke, remotetest


@remotetest
def test_create_cluster_file():
    """
    Create the cluster file based on the profile definition
    """
    import os
    profile_name = os.environ["TEST_PROFILENAME"]
    cluster_name = os.environ["TEST_CLUSTERNAME"]
    result = invoke(["up", profile_name, "-n", cluster_name, "--no-provision"], clustername_flag=False)
    assert result.exit_code == 0, result.output
