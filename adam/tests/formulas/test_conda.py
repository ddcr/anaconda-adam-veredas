import pytest

from adam.tests.utils import remotetest, cluster


@remotetest
def test_conda_prefix(cluster):
    """
    Checks that the prefix is the default prefix
    """
    response = cluster.salt("*", "conda.prefix")
    for minionid, value in response.items():
        assert value == cluster.anaconda_dir


@remotetest
def test_conda_list(cluster):
    """
    List packages in the root environment
    """
    response = cluster.salt("ci_head_1", "conda.list")
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        assert "conda" in packages.keys()
        assert "python" in packages.keys()
        assert "pip" in packages.keys()


@remotetest
@pytest.mark.parametrize(("envname", "is_prefix", "expected_prefix"), [
    ("test_conda_create_name", False, "/opt/a/b/c/muunitnoc/anaconda/envs/test_conda_create_name"),
    ("/tmp/test_conda_create_prefix", True, "/tmp/test_conda_create_prefix"),
])
def test_conda_create(cluster, request, envname, is_prefix, expected_prefix):
    """
    1. Create an environment
    2. Check pkgs versions
    3. Creating the same environment again will result in error
    X. Delete environment prefix so test can be ran again correctly
    """
    pkgs = {'python': '3.4.0', 'pandas': ''}
    pkgs_str = gen_pkgs_str(pkgs)

    # 1. Create environment
    kwargs = {}
    kwargs['pkgs'] = pkgs_str
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.create", envname, kwarg=kwargs)
    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 2. Do conda.list and check package versions
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        for pkg, version in pkgs.items():
            assert pkg in packages
            if version != '':
                assert packages[pkg]['version'] == version, "{} version doesn't match".format(pkg)

    # 3. Attempt to create environment again
    kwargs = {}
    kwargs['pkgs'] = pkgs_str
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.create", envname, kwarg=kwargs)
    for minionid, value in response.items():
        assert "prefix already exists: {}".format(expected_prefix) in value["error"]

    # X. Delete environment
    def fin():
        client = cluster.head.ssh_client
        client.exec_command("rm -rf {}".format(expected_prefix))
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_conda_install_root(cluster):
    """
    1. Install packages (specific old versions) in root environment
    2. Check pkgs versions
    """
    pkgs = {'numpy': '1.7.1', 'scipy': ''}
    pkgs_str = gen_pkgs_str(pkgs)

    # 1. Install packages in the root environment
    kwargs = {}
    kwargs['pkgs'] = pkgs_str
    response = cluster.salt("ci_head_1", "conda.install", kwarg=kwargs)

    # 2. List
    response = cluster.salt("ci_head_1", "conda.list")
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        for pkg, version in pkgs.items():
            assert pkg in packages
            if version != '':
                assert packages[pkg]['version'] == version, "{} version doesn't match".format(pkg)


@remotetest
@pytest.mark.parametrize(("envname", "is_prefix", "expected_prefix"), [
    ("test_conda_update", False, "/opt/a/b/c/muunitnoc/anaconda/envs/test_conda_update"),
    ("/tmp/test_conda_update_prefix", True, "/tmp/test_conda_update_prefix"),
])
def test_conda_update(cluster, request, envname, is_prefix, expected_prefix):
    """
    1. Create an environment including one outdated package
    2. Check pkgs versions
    3. Update one outdate package
    4. Check pkgs versions
    X. Delete environment prefix so test can be ran again correctly
    """
    pkgs = {'python': '2.7.10', 'numpy': '1.7.1'}
    pkgs_str = gen_pkgs_str(pkgs)

    # 1. Create environment
    kwargs = {}
    kwargs['pkgs'] = pkgs_str
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.create", envname, kwarg=kwargs)
    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 2. List
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix

    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)

    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        for pkg, version in pkgs.items():
            assert pkg in packages
            if version != '':
                assert packages[pkg]['version'] == version, "{} version doesn't match".format(pkg)

    # 3. Update
    kwargs = {}
    kwargs['env'] = envname
    kwargs['pkgs'] = "numpy"
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.update", kwarg=kwargs)
    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 4. List
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        for pkg, version in pkgs.items():
            assert pkg in packages
            if pkg == "numpy":
                new_version = packages[pkg]['version']
                assert version_gt(new_version, version), 'Updated versions should be greater'

    # X. Delete environment
    def fin():
        client = cluster.head.ssh_client
        client.exec_command("rm -rf {}".format(expected_prefix))
        client.close()

    request.addfinalizer(fin)


@remotetest
@pytest.mark.parametrize(("envname", "is_prefix", "expected_prefix"), [
    ("test_conda_remove", False, "/opt/a/b/c/muunitnoc/anaconda/envs/test_conda_remove"),
    ("/tmp/test_conda_remove_prefix", True, "/tmp/test_conda_remove_prefix"),
])
def test_conda_remove(cluster, request, envname, is_prefix, expected_prefix):
    """
    1. Create an environment with packages
    2. Check pkgs exist
    3. Remove one package
    2. Check pkgs was removed
    X. Delete environment prefix so test can be ran again correctly
    """
    pkgs = {'python': '2.7.10', 'numpy': ''}
    pkgs_str = gen_pkgs_str(pkgs)

    # 1. Create environment
    kwargs = {}
    kwargs['pkgs'] = pkgs_str
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.create", envname, kwarg=kwargs)
    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 2. List
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        for pkg, version in pkgs.items():
            assert pkg in packages

    # 3. Remove
    kwargs = {}
    kwargs['env'] = envname
    kwargs['pkgs'] = "numpy"
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.remove", kwarg=kwargs)
    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 4. List
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)
    for minionid, conda_resp in response.items():
        assert "numpy" not in conda_resp['packages']

    # X. Delete environment
    def fin():
        client = cluster.head.ssh_client
        client.exec_command("rm -rf {}".format(expected_prefix))
        client.close()

    request.addfinalizer(fin)


@remotetest
@pytest.mark.parametrize(("envname", "is_prefix", "expected_prefix"), [
    ("test_conda_everything", False, "/opt/a/b/c/muunitnoc/anaconda/envs/test_conda_everything"),
    ("/tmp/test_conda_everything_prefix", True, "/tmp/test_conda_everything_prefix"),
])
def test_conda_everything(cluster, request, envname, is_prefix, expected_prefix):
    """
    1. Create empty environment
    2. Check some packages are not installed
    3. Install packages
    4. Check pkgs versions
    5. Update one outdate package
    6. Check pkgs versions, should be updated
    7. Remove one package
    8. Check pkgs versions
    X. Delete environment prefix so test can be ran again correctly
    """
    # pkgs = {'numpy': '1.7.1', 'scipy': '', 'boto': '2.38.0'}
    pkgs = {'numpy': '1.7.1', 'scipy': ''}
    pkgs_str = gen_pkgs_str(pkgs)

    # 1. Create environment
    kwargs = {}
    kwargs['pkgs'] = 'python=2'
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.create", envname, kwarg=kwargs)

    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 2. List
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        for pkg, version in pkgs.items():
            assert pkg not in packages

    # 3. Install
    kwargs = {}
    kwargs['env'] = envname
    kwargs['pkgs'] = pkgs_str
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.install", kwarg=kwargs)
    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 4. List
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        for pkg, version in pkgs.items():
            assert pkg in packages
            if version != '':
                assert packages[pkg]['version'] == version, "{} version doesn't match".format(pkg)

    # 5. Update
    kwargs = {}
    kwargs['env'] = envname
    kwargs['pkgs'] = "numpy"
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.update", kwarg=kwargs)
    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 6. List
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        for pkg, version in pkgs.items():
            assert pkg in packages
            if pkg == "numpy":
                new_version = packages[pkg]['version']
                assert version_gt(new_version, version), 'Updated versions should be greater'

    # 7. Remove
    kwargs = {}
    kwargs['env'] = envname
    kwargs['pkgs'] = "numpy"
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.remove", kwarg=kwargs)
    for minionid, value in response.items():
        assert value["success"] is True
        assert value["prefix"] == expected_prefix

    # 8. List
    kwargs = {}
    kwargs['env'] = envname
    kwargs['prefix'] = is_prefix
    response = cluster.salt("ci_head_1", "conda.list", kwarg=kwargs)
    for minionid, conda_resp in response.items():
        packages = conda_resp['packages']
        assert "numpy" not in packages

    # X. Delete environment
    def fin():
        client = cluster.head.ssh_client
        client.exec_command("rm -rf {}".format(expected_prefix))
        client.close()

    request.addfinalizer(fin)


def version_gt(version1, version2):
    """Compate if one string versions are greater than the other
    """
    from distutils.version import StrictVersion
    return StrictVersion(version1) > StrictVersion(version2)


def gen_pkgs_str(pkgs):
    """Generate a string of packages based on a dictionary
    """
    list_ = []
    for pkg, version in pkgs.items():
        if version != '':
            list_.append('{}={}'.format(pkg, version))
        else:
            list_.append(pkg)
    return ','.join(list_)
