import posixpath

from adam.tests.utils import remotetest, cluster, assert_response_ok, assert_response_no_changes
from adam.salt.core import upload_settings

from adam.conda_remote import conda_create

from adam.ssh import parallel_exec_command

@remotetest
def test_custom_user_group(cluster):
    """
    Test salt_username/salt_groupname settings
    1. create new user
    2. sync new cluster profile
    3. repermission anaconda
    4. verify salt_username/salt_group settings
    """

    t_user = cluster.salt_settings.salt_username
    t_group = cluster.salt_settings.salt_groupname

    cluster.salt_settings.salt_username = "hexspeak"
    cluster.salt_settings.salt_groupname = "oxdeadbeef"

    # create user/group
    # we manually do the following because create_anaconda_* funcs
    # set permissions

    clients = [node.ssh_client for node in cluster.nodes]

    command = "getent group {group} &> /dev/null || groupadd {group}".format(group=cluster.salt_settings.salt_groupname)
    results = parallel_exec_command(clients, command, sudo=True)

    command = "getent passwd {username} &> /dev/null || useradd -M -g " \
              "{group} -d {homedir} -p $(openssl passwd -1 {password}) " \
              "{username}".format(username=cluster.salt_settings.salt_username,
                                  group=cluster.salt_settings.salt_groupname,
                                  homedir=cluster.rootdir,
                                  password=cluster.salt_settings.salt_password)
    parallel_exec_command(clients, command, sudo=True)


    # upload new pillar
    upload_settings(cluster)

    # reset user/group to run salt commands
    cluster.salt_settings.salt_groupname = t_group
    cluster.salt_settings.salt_username = t_user
    cluster.salt("*", "saltutil.sync_all")

    response = cluster.salt(cluster.head.host, "state.sls", "anaconda.repermission")
    assert response.has_failed == False

    client = cluster.head.ssh_client
    uid = int(client.exec_command("id -u hexspeak")["stdout"])
    gid = int(client.exec_command("id -g hexspeak")["stdout"])

    anaconda_conda_bin = posixpath.join(cluster.rootdir, "anaconda", "bin", "conda")
    assert client.stat(cluster.anaconda_dir).st_uid == uid
    assert client.stat(cluster.anaconda_dir).st_gid == gid
    assert client.stat(anaconda_conda_bin).st_uid == uid
    assert client.stat(anaconda_conda_bin).st_gid == gid

    # only true if t_user does not equal the usual default of "anaconda"
    assert client.stat(cluster.rootdir).st_uid != uid
    assert client.stat(cluster.rootdir).st_gid != gid

    response = conda_create(cluster, cluster.head.host, env="new_user_env", pkgs="numpy,pandas")
    assert response.has_failed == False

    [client.close() for client in clients]
