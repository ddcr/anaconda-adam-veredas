import posixpath

from adam.tests.utils import remotetest, cluster, assert_response_ok, assert_response_no_changes


@remotetest
def test_supervisor_formulas(cluster):
    response = cluster.salt(cluster.head.host, "state.sls", "supervisor")
    assert_response_ok(response)

    client = cluster.head.ssh_client
    u_command = "id -u {}".format(cluster.salt_settings.salt_username)
    g_command = "id -g {}".format(cluster.salt_settings.salt_username)

    uid = int(client.exec_command(u_command)["stdout"])
    gid = int(client.exec_command(g_command)["stdout"])
    supervisor_dir = posixpath.join(cluster.rootdir, 'supervisor')
    supervisor_pid = posixpath.join(supervisor_dir, 'var', 'run', 'supervisord.pid')
    supervisor_bin = posixpath.join(supervisor_dir, 'bin', 'supervisord')
    assert client.stat(supervisor_dir).st_uid == uid
    assert client.stat(supervisor_dir).st_gid == gid
    assert client.stat(supervisor_bin).st_uid == uid
    assert client.stat(supervisor_bin).st_gid == gid
    assert client.exec_command("kill -0 $(cat {})".format(supervisor_pid), sudo=True)["exit_code"] == 0
    client.close()

    # Install it again. No changes. CUrrently it has changes becuase of the conda state
    # response = cluster.salt(cluster.head.host, "state.sls", "supervisor")
    # assert_response_no_changes(response)
