import posixpath

from adam.tests.utils import remotetest, cluster, assert_response_ok, assert_response_no_changes


@remotetest
def test_anaconda_formulas(cluster):
    response = cluster.salt(cluster.head.host, "state.sls", "anaconda")
    assert_response_ok(response)

    client = cluster.head.ssh_client
    u_command = "id -u {}".format(cluster.salt_settings.salt_username)
    g_command = "id -g {}".format(cluster.salt_settings.salt_username)

    uid = int(client.exec_command(u_command)["stdout"])
    gid = int(client.exec_command(g_command)["stdout"])

    anaconda_conda_bin = posixpath.join(cluster.rootdir, "anaconda", "bin", "conda")
    assert client.stat(cluster.anaconda_dir).st_uid == uid
    assert client.stat(cluster.anaconda_dir).st_gid == gid
    assert client.stat(anaconda_conda_bin).st_uid == uid
    assert client.stat(anaconda_conda_bin).st_gid == gid
    client.close()

    # Install it again. No changes
    response = cluster.salt(cluster.head.host, "state.sls", "anaconda")
    assert_response_no_changes(response)
