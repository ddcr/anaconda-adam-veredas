from adam.tests.utils import remotetest, cluster, assert_response_ok, assert_response_no_changes


@remotetest
def test_conda_managed(cluster):
    response = cluster.salt(cluster.head.host, "state.sls", "anaconda.tests.managed")
    assert_response_ok(response)
    assert response.has_failed == False

    response = cluster.salt(cluster.head.host, "state.sls", "anaconda.tests.managed", kwarg={"test": True})
    assert_response_no_changes(response)
