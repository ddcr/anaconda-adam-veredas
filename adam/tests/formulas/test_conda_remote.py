from __future__ import absolute_import, print_function, division

import json
import pytest
import posixpath

from adam.responses import CondaResponse
from adam.exceptions import AdamException
from adam.tests.utils import remotetest, cluster
from adam.conda_remote import conda_info, conda_install, \
                           conda_list, conda_create, \
                           conda_remove, conda_update


@remotetest
def test_failed_create(cluster):
    with pytest.raises(AdamException):
        conda_create(cluster, cluster.head.host, env='')

    with pytest.raises(AdamException):
        conda_create(cluster, cluster.head.host, pkgs=None)

    with pytest.raises(AdamException):
        conda_create(cluster, cluster.head.host, env="bob_env")

    with pytest.raises(AdamException):
        conda_create(cluster, cluster.head.host, pkgs="numpy=1.3")


@remotetest
def test_basic_response(cluster):
    response = conda_info(cluster, target="*")
    assert isinstance(response, CondaResponse)
    assert len(response) == 2


@remotetest
def test_conda_info_envs(cluster):
    response = conda_info(cluster, target="*", options='e')
    assert cluster.anaconda_dir in response[cluster.head.host].stdout


@remotetest
def test_failed_response(cluster):
    response = conda_install(cluster, cluster.head.host)
    failures = response.failed_nodes
    minion, failure = failures[0]
    head = response[cluster.head.host]

    error = failure.get('error') or head.message
    assert minion == cluster.head.host
    assert 'too few arguments, must supply command line package specs or --file' in error


@remotetest
def test_incorrect_package(cluster):
    response = conda_install(cluster, cluster.head.host, pkgs='not_a_package')
    head = response[cluster.head.host]
    failures = response.failed_nodes
    minion, failure = failures[0]

    assert response.has_failed == True
    assert minion == cluster.head.host
    assert head.success is False

    # Error message differences between conda versions
    # Conda 4.2.X populates the message field
    # Conda 4.0.5 Error: No packages found in current linux-64 channels matching: not_a_package
    # Conda 3.19.1 'Package missing in current linux-64 channels:'
    error = failure.get('error') or head.message
    assert 'not_a_package' in error


@remotetest
def test_conda_list(cluster):
    response = conda_list(cluster)
    head = response[cluster.head.host]

    assert 'conda' in head.packages.keys()


@remotetest
def test_clean_create(cluster, request):
    """
    1. Create an environment
    2. Check pkgs versions
    3. Creating the same environment again will result in error
    4. Delete environment prefix so test can be ran again correctly
    """
    expected_prefix = posixpath.join(cluster.anaconda_dir, 'envs', 'bob_env')
    # 1 create env
    response = conda_create(cluster, cluster.head.host, env="bob_env", pkgs="numpy=1.10")
    head = response[cluster.head.host]

    assert head.success is True
    assert head.prefix == expected_prefix
    assert 'numpy' in head.stdout

    # Failing recreate
    response = conda_create(cluster, cluster.head.host, env="bob_env", pkgs="numpy=1.10")
    head = response[cluster.head.host]
    assert head.success is False
    error = head.error or head.message
    assert 'prefix already exists' in error
    assert 'prefix already exists' in head.stdout

    error_type = head.error_type or head.exception_type
    assert 'ValueError' in error_type

    # 4. Delete environment
    def fin():
        client = cluster.head.ssh_client
        client.exec_command("rm -rf {}".format(expected_prefix))
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_conda_install(cluster):
    response = conda_install(cluster, cluster.head.host, pkgs='ldap3')
    head = response[cluster.head.host]
    assert head.success is True


@remotetest
def test_conda_remove(cluster):
    response = conda_install(cluster, cluster.head.host, pkgs='ldap3')
    head = response[cluster.head.host]

    assert head.success is True

    response = conda_remove(cluster, cluster.head.host, pkgs='ldap3')
    head = response[cluster.head.host]
    assert head.success is True
    assert 'UNLINK' in head.stdout

    response = conda_remove(cluster, cluster.head.host, pkgs='ldap3')
    head = response[cluster.head.host]
    error = head.error or head.message
    assert 'no packages found to remove' in error
    assert head.success is False


@remotetest
def test_conda_update(cluster):
    """
    1. install old version of ldap3
    2. update version
    3. update again and check message
    4. update nonexistent package
    5. remove ldap3
    """

    # 1.
    response = conda_install(cluster, cluster.head.host, pkgs='ldap3=0.9.8.4')
    head = response[cluster.head.host]
    assert head.success is True

    # 2.
    response = conda_update(cluster, cluster.head.host, pkgs='ldap3')
    head = response[cluster.head.host]
    assert head.success is True


    msg = '"UNLINK": [\n      "ldap3-0.9.8.4-py27_0"'
    assert msg in head.stdout

    # 3.
    response = conda_update(cluster, cluster.head.host, pkgs='ldap3')
    head = response[cluster.head.host]
    assert 'All requested packages already installed' in head.message
    assert head.success is True

    # 4.
    response = conda_update(cluster, cluster.head.host, pkgs='ldap_bob')
    head = response[cluster.head.host]

    error = head.error or head.message
    assert head.success is False
    assert "Package 'ldap_bob' is not installed" in error

    # 5.
    response = conda_remove(cluster, cluster.head.host, pkgs='ldap3')
    head = response[cluster.head.host]
    assert head.success is True
    assert 'UNLINK' in head.stdout
