from __future__ import absolute_import, print_function, division

import pytest

from adam import Cluster
from .utils import remotetest, cluster

from schematics.exceptions import ModelValidationError, ModelConversionError

valid_node = {"host": "127.0.0.1", "username": "root", "keypair": "~/.ssh/keypair"}
valid = {"name": "test", "nodes": [valid_node, valid_node, valid_node, valid_node]}

valid_complete_node = {"host": "127.0.0.1",
                       "username": "root",
                       "keypair": "~/.ssh/keypair",
                       "port": "2222",
                       "password": "root"}
valid_complete = {"name": "test", "nodes": [valid_complete_node, valid_node]}

invalid_node = {"host": "127.0.0.1", "username": "root", "keypair": "~/.ssh/keypair", "port": "uno"}
invalid = {"name": "test", "nodes": [invalid_node, valid_node]}


def test_cluster_valid():
    import json
    cluster = Cluster({"name": "test"})
    cluster.validate()

    cluster = Cluster(valid)
    cluster.validate()

    cluster = Cluster(valid_complete)
    assert json.loads(repr(cluster)) == cluster.to_primitive()


def test_cluster_invalid():
    with pytest.raises(ModelValidationError):
        cluster = Cluster()
        cluster.validate()

    with pytest.raises(ModelConversionError):
        cluster = Cluster(invalid)
        cluster.validate()


def test_set_nodes_username():
    cluster = Cluster(valid)
    user = "ubuntu"
    cluster.set_nodes_username(user)
    for node in cluster.nodes:
        assert node.username == user


def test_set_nodes_keypair():
    cluster = Cluster(valid)
    pkey = "~/.ssh/anotherkey"
    cluster.set_nodes_keypair(pkey)
    for node in cluster.nodes:
        assert node.keypair == pkey


@remotetest
def test_check_ssh(cluster):
    response = cluster.check_ssh()
    assert len(response) == len(cluster.nodes)
    for address, status in response.items():
        assert status is True

########################################################################################################################
# Model fields
########################################################################################################################

from schematics.types import StringType
from schematics.types.compound import ModelType
from adam.models import PluginModel


class ExamplePlugin(PluginModel):
    f1 = StringType()
    f2 = StringType(required=True)
    f3 = StringType()


class ClusterWithPlugins(Cluster):
    example = ModelType(ExamplePlugin, default=ExamplePlugin())


def test_plugin_enabled():
    # with pytest.raises(ModelConversionError):  # Schematics 2.0.0
    with pytest.raises(ModelValidationError):  # Schematics 1.1.1
        model = ClusterWithPlugins({"name": "test", "example": {"enabled": True}})
        model.validate()

    model = ClusterWithPlugins({"name": "test", "example": {"enabled": True, "f2": "string"}})
    model.validate()


def test_plugin_disabled():
    model = ClusterWithPlugins({"name": "test", "example": {"enabled": False}})
    model.validate()
