from __future__ import absolute_import, print_function, division

import os
import pytest

import posixpath

from adam.ssh import SSHClient, exec_command, parallel_exec_command, parallel_upload, check_results
from adam.exceptions import AdamException, SSHException, RetriesExceededException, MKDIRException
from .utils import remotetest, dockertest, cluster, ssh_dir

# note we test with RSA generate keys else where


def test_dsa_key():
    key_file = os.path.join(ssh_dir(), "dsa_key.key")
    pkey = SSHClient.pkey_from_key_file(key_file)
    pkey = SSHClient.pkey_from_string(open(key_file).read())


def test_ecdsa_key():
    key_file = os.path.join(ssh_dir(), "ECDSA.pem")
    pkey = SSHClient.pkey_from_key_file(key_file)
    pkey = SSHClient.pkey_from_string(open(key_file).read())


def test_invalid_key():
    key_file = os.path.join(ssh_dir(), "not_valid_key.key")
    with pytest.raises(SSHException):
        pkey = SSHClient.pkey_from_key_file(key_file)
    with pytest.raises(SSHException):
        pkey = SSHClient.pkey_from_string(open(key_file).read())


def test_agent_key():
    key_file = os.path.join(ssh_dir(), "id_agent_rsa.pub")
    pkey = SSHClient.pkey_from_key_file_w_agent(key_file)


def test_no_agent_key():
    key_file = os.path.join(ssh_dir(), "id_no_agent_rsa.pub")
    with pytest.raises(SSHException):
        pkey = SSHClient.pkey_from_key_file_w_agent(key_file)


@remotetest
def test_open_file(cluster, request):
    import random

    testname = request.node.name
    fpath = "/tmp/{}".format(testname)
    client = cluster.head.ssh_client
    fcontent = "{}".format(random.random() * 1000000)

    with client.open(fpath, "w") as f:
        f.write(fcontent)
    assert client.exec_command("test -e {}".format(fpath))["exit_code"] == 0

    with client.open(fpath, "r") as f:
        read = f.read().decode("utf-8")
        assert read == fcontent

    client.remove(fpath)
    assert client.exec_command("test -e {}".format(fpath))["exit_code"] == 1


@remotetest
@dockertest
def test_chown_stat(cluster, request):
    testname = request.node.name
    fpath = "/tmp/{}".format(testname)
    client = cluster.head.ssh_client
    uid = int(client.exec_command("id -u testuser")["stdout"])
    gid = int(client.exec_command("id -g testuser")["stdout"])

    with client.open(fpath, "w") as f:
        f.write("fake-content")
    stats = client.stat(fpath)
    assert stats.st_uid == 0
    assert stats.st_gid == 0

    client.chown(fpath, uid, gid)
    stats = client.stat(fpath)
    assert stats.st_uid == uid
    assert stats.st_gid == gid

    client.remove(fpath)


@remotetest
@dockertest
def test_chmod_stat(cluster, request):
    testname = request.node.name
    fpath = "/tmp/{}".format(testname)
    client = cluster.head.ssh_client

    with client.open(fpath, "w") as f:
        f.write("fake-content")
    stats = client.stat(fpath)
    assert stats.st_mode == 33188

    client.chmod(fpath, "777")
    stats = client.stat(fpath)
    assert stats.st_mode == 33279

    client.remove(fpath)


@remotetest
def test_exec_command_fail(cluster):
    client = cluster.head.ssh_client
    with pytest.raises(AdamException) as excinfo:
        exec_command(client, "false")


@remotetest
def test_parallel_exec_command(cluster):
    clients = [node.ssh_client for node in cluster.nodes]
    assert len(clients) == len(cluster.nodes)

    results = parallel_exec_command(clients, "hostname")
    assert len(results) == len(cluster.nodes)
    check_results(results, error_msg="")
    for node in cluster.nodes:
        assert node.host in results.keys()

    results = parallel_exec_command(clients, "echo {node_number}")
    assert len(results) == len(cluster.nodes)
    check_results(results, error_msg="")
    for i, node in enumerate(cluster.nodes):
        assert results[node.host]["stdout"] == "{0}".format(i)


@remotetest
def test_parallel_exec_command_fail(cluster):
    clients = [node.ssh_client for node in cluster.nodes]
    results = parallel_exec_command(clients, "false")
    for node_host, result in results.items():
        assert isinstance(result, RetriesExceededException)
    with pytest.raises(AdamException) as excinfo:
        check_results(results, "")


@remotetest
def test_parallel_upload(cluster, tmpdir):
    import random

    name = "{}.txt".format(random.randint(1000000, 9999999))
    content = str(random.random() * 1000000)
    tempf = tmpdir.mkdir("sub").join(name)
    tempf.write(content)
    local = tempf.strpath
    remote = posixpath.join("/tmp", name)

    clients = [node.ssh_client for node in cluster.nodes]
    results = parallel_upload(clients, local, remote)
    assert len(results) == len(cluster.nodes)
    check_results(results, error_msg="")
    for i, node in enumerate(cluster.nodes):
        assert results[node.host] is True
        client = node.ssh_client
        with client.open(remote, "r") as f:
            read = f.read().decode("utf-8")
            assert read == content


@remotetest
@dockertest
def test_ssh_multiple_users(cluster):
    import os
    instance = cluster.head
    pkey = SSHClient.pkey_from_key_file(os.path.expanduser(instance.keypair))
    root_client = SSHClient(host=instance.host, port=instance.port, username=instance.username, pkey=pkey)
    testuser_client = SSHClient(host=instance.host, port=instance.port, username="testuser", password="password")

    response = root_client.exec_command("whoami")
    assert response["exit_code"] == 0
    assert response["stdout"] == "root"

    response = root_client.exec_command("whoami", asuser="testuser")
    assert response["exit_code"] == 0
    assert response["stdout"] == "testuser"

    response = root_client.exec_command("whoami", asuser="testuser", sudo=True)
    assert response["exit_code"] == 0
    assert response["stdout"] == "testuser"

    response = testuser_client.exec_command("whoami")
    assert response["exit_code"] == 0
    assert response["stdout"] == "testuser"

    response = testuser_client.exec_command("whoami", asuser="root", sudo=True)
    assert response["exit_code"] == 0
    assert response["stdout"] == "root"

    root_client.close()
    testuser_client.close()


@remotetest
def test_ssh_ok_pkey_obj(cluster):
    import os
    instance = cluster.head
    pkey = SSHClient.pkey_from_key_file(os.path.expanduser(instance.keypair))
    client = SSHClient(host=instance.host, username=instance.username, port=instance.port, password=None, pkey=pkey)
    client.close()


@remotetest
def test_ssh_ok_pkey_string(cluster):
    import os
    instance = cluster.head
    with open(os.path.expanduser(instance.keypair)) as f:
        key_data = f.read()
    pkey = SSHClient.pkey_from_string(key_data)
    client = SSHClient(host=instance.host, username=instance.username, port=instance.port, password=None, pkey=pkey)
    client.close()


@remotetest
def test_wrong_pkey_type(cluster):
    instance = cluster.head
    pkey = {"wrong": "obj"}
    with pytest.raises(AttributeError) as excinfo:
        client = SSHClient(host=instance.host, username=instance.username, port=instance.port, password=None, pkey=pkey)


@remotetest
@dockertest
def test_ssh_ok_password(cluster):
    instance = cluster.head
    password = "root"
    client = SSHClient(host=instance.host, username=instance.username, port=instance.port, password=password, pkey=None)
    reponse = client.exec_command("ls")
    client.close()


@remotetest
def test_ssh_fail_password(cluster):
    instance = cluster.head
    password = "root_not"  # NOTE: this is a little bit hardcoded to docker setup
    with pytest.raises(SSHException) as excinfo:
        client = SSHClient(host=instance.host,
                           username=instance.username,
                           port=instance.port,
                           password=password,
                           pkey=None)
    assert "Authentication Error" in str(excinfo.value)


@remotetest
def test_ssh_fail_user(cluster):
    client = cluster.head.ssh_client
    client.username = "FAKEUSER"
    with pytest.raises(SSHException) as excinfo:
        client.connect()
    assert "Authentication Error" in str(excinfo.value)
    client.close()


@remotetest
def test_ssh_fail_host(cluster):
    client = cluster.head.ssh_client
    client.host = "1.1.1.1"
    client.timeout = 3  # so test runs faster
    with pytest.raises(SSHException) as excinfo:
        client.connect()
    assert "Error connecting to host" in str(excinfo.value)
    assert "timed out" in str(excinfo.value)
    client.close()


@remotetest
def test_wrong_host(cluster):
    client = cluster.head.ssh_client
    client.host = "WRONGHOST"
    with pytest.raises(SSHException) as excinfo:
        client.connect()
    assert "Unknown host 'WRONGHOST'" in str(excinfo.value)
    client.close()


@remotetest
def test_exec_command(cluster):
    client = cluster.head.ssh_client
    response = client.exec_command("whoami")
    assert response["exit_code"] == 0
    assert response["stdout"] == cluster.head.username
    client.close()


@remotetest
def test_exec_command_sudo(cluster, request):
    testname = request.node.name

    client = cluster.head.ssh_client
    response = client.exec_command("mkdir /{}".format(testname), sudo=True)
    assert response["exit_code"] == 0
    response = client.exec_command("test -d /{}".format(testname), sudo=True)
    assert response["exit_code"] == 0

    def fin():
        response = client.exec_command("rm -rf /{}".format(testname), sudo=True)
        assert response["exit_code"] == 0
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_file_exists(cluster, request):
    testname = request.node.name
    client = cluster.head.ssh_client
    fpath = posixpath.join("/tmp", testname + ".txt")
    assert client.file_exists(fpath) is False

    with client.open(fpath, "w") as f:
        f.write("content")

    assert client.file_exists(fpath) is True

    def fin():
        response = client.exec_command("rm {}".format(fpath))
        assert response["exit_code"] == 0
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_mkdir(cluster, request):
    testname = request.node.name
    client = cluster.head.ssh_client

    dir1 = posixpath.join("/tmp", testname, "dir1")
    dir2 = posixpath.join(dir1, "dir2")
    dir3 = posixpath.join(dir2, "dir3")
    client.mkdir(dir3)
    assert client.dir_exists(dir1) is True
    assert client.dir_exists(dir2) is True
    assert client.dir_exists(dir3) is True

    assert client.dir_exists("test -d /FAKEFAKE") is False
    assert client.exec_command("test -d /FAKEFAKE")["exit_code"] == 1
    assert client.exec_command("test -d {}".format(dir1))["exit_code"] == 0
    assert client.exec_command("test -d {}".format(dir2))["exit_code"] == 0
    assert client.exec_command("test -d {}".format(dir3))["exit_code"] == 0

    with pytest.raises(MKDIRException):
        client.mkdir('/failed_directory', asuser='bob')


    def fin():
        response = client.exec_command("rm -rf /tmp/{}".format(testname), sudo=True)
        assert response["exit_code"] == 0
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_mkdir_already_exists(cluster, request):
    from random import randint
    testname = request.node.name
    client = cluster.head.ssh_client

    test_dir = posixpath.join("/tmp", testname, "test_dir-{}".format(randint(1000, 9999)))
    client.mkdir(test_dir)
    assert client.dir_exists(test_dir) is True
    with pytest.raises(SSHException) as excinfo:
        client.mkdir(test_dir)

    def fin():
        response = client.exec_command("rm -rf /tmp/{}".format(testname), sudo=True)
        assert response["exit_code"] == 0
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_put_no_exist(cluster, tmpdir, request):
    testname = request.node.name
    client = cluster.head.ssh_client

    content = "content"
    f = tmpdir.join("{}.txt".format(testname))
    f.write(content)

    local = f.strpath + ".fake"
    remote = "/tmp/{}.txt".format(testname)

    with pytest.raises(SSHException):
        client.put(local, remote)

    def fin():
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_put_remote_dir_no_exists(cluster, tmpdir, request):
    testname = request.node.name
    client = cluster.head.ssh_client

    content = "content"
    f = tmpdir.join("{}.txt".format(testname))
    f.write(content)

    local = f.strpath
    remote = "/tmp/this/dir/should/not/exists/{}.txt".format(testname)

    with pytest.raises(SSHException):
        client.put(local, remote)

    def fin():
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_put_remote_dir_no_exists_sudo(cluster, tmpdir, request):
    testname = request.node.name
    client = cluster.head.ssh_client

    content = "content"
    f = tmpdir.join("{}.txt".format(testname))
    f.write(content)

    local = f.strpath
    remote = "/tmp/this/dir/should/not/exists/{}.txt".format(testname)

    with pytest.raises(SSHException):
        client.put(local, remote, sudo=True)

    def fin():
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_put_file(cluster, tmpdir, request):
    testname = request.node.name
    client = cluster.head.ssh_client

    content = "content"
    f = tmpdir.join("{}.txt".format(testname))
    f.write(content)

    local = f.strpath
    remote = "/tmp/{}.txt".format(testname)

    client.put(local, remote, sudo=True)
    assert client.exec_command("test -e {}".format(remote))["exit_code"] == 0
    with client.open(remote, "r") as f:
        read = f.read().decode("utf-8")
        assert read == content

    def fin():
        client.close()

    request.addfinalizer(fin)


@remotetest
def test_put_file_failed(cluster, tmpdir, request):
    """
    test failure for making /tmp/adam
    """
    testname = request.node.name
    client = cluster.head.ssh_client

    content = "content"
    f = tmpdir.join("{}.txt".format(testname))
    f.write(content)

    local = f.strpath
    remote = "/opt/continuum/{}.txt".format(testname)
    client.username = 'bob'


    def fin():
        client.close()

    request.addfinalizer(fin)

@remotetest
def test_put_dir(cluster, tmpdir, request):
    testname = request.node.name
    client = cluster.head.ssh_client

    d1 = tmpdir.mkdir("rootdir")
    f1 = d1.join("upload1.txt")
    f1.write("content1")

    d2 = d1.mkdir("subdir")
    f2 = d2.join("upload2.txt")
    f2.write("content2")

    d3 = d2.mkdir("subsubdir")
    f3 = d3.join("upload3.txt")
    f3.write("content3")

    local = d1.strpath
    remote = "/tmp/{}".format(testname)
    client.put(local, remote, sudo=True)
    assert client.dir_exists(posixpath.join(remote)) is True
    assert client.dir_exists(posixpath.join(remote, "subdir")) is True
    assert client.dir_exists(posixpath.join(remote, "subdir", "subsubdir")) is True
    assert client.exec_command("test -e {}".format(posixpath.join(remote)))["exit_code"] == 0
    with client.open(posixpath.join(remote, "upload1.txt"), "r") as f:
        read = f.read().decode("utf-8")
        assert read == "content1"
    with client.open(posixpath.join(remote, "subdir", "upload2.txt"), "r") as f:
        read = f.read().decode("utf-8")
        assert read == "content2"
    with client.open(posixpath.join(remote, "subdir", "subsubdir", "upload3.txt"), "r") as f:
        read = f.read().decode("utf-8")
        assert read == "content3"

    def fin():
        response = client.exec_command("rm -rf /tmp/{}".format(testname), sudo=True)
        assert response["exit_code"] == 0
        client.close()

    request.addfinalizer(fin)


def test_format_command():
    from adam.ssh import format_command
    cmd = "echo 1"
    assert format_command(cmd) == cmd
    cmd = "echo {node_number}"
    assert format_command(cmd) == cmd
    cmd = "echo {node_number}"
    assert format_command(cmd, 2) == "echo 2"
