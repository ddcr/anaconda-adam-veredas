import pytest

from adam.exceptions import SaltException
from adam.responses import Response, StateResponse


def test_response_groups():
    response = Response()
    response["i1"] = 1
    response["i2"] = 2
    response["i3"] = 1
    response["i4"] = 2

    groups = response.groups()
    assert groups[0][1] == 1
    assert "i1" in groups[0][0]
    assert "i3" in groups[0][0]
    assert groups[1][1] == 2
    assert "i2" in groups[1][0]
    assert "i4" in groups[1][0]


def test_response_groups_dict():
    response = Response()
    response["i1"] = {"result": False}
    response["i2"] = {"result": True}
    response["i3"] = {"result": True}
    response["i4"] = {"result": False}

    groups = response.groups()
    assert groups[0][1] == {"result": False}
    assert "i1" in groups[0][0]
    assert "i4" in groups[0][0]
    assert groups[1][1] == {"result": True}
    assert "i2" in groups[1][0]
    assert "i3" in groups[1][0]


def test_state_response_groups():
    response = StateResponse()
    response["i1"] = {"state1": {"result": True}, "state2": {"result": True}}
    response["i2"] = {"state1": {"result": False}, "state2": {"result": True}}
    response["i3"] = {"state1": {"result": True}, "state2": {"result": False}}
    response["i4"] = {"state1": {"result": False}, "state2": {"result": False}}

    gresponse = response.group_successful()
    assert len(gresponse["i1"]["successful"]) == 2
    assert len(gresponse["i1"]["failed"]) == 0
    assert {"name": "state1", "result": True} in gresponse["i1"]["successful"]
    assert {"name": "state2", "result": True} in gresponse["i1"]["successful"]

    assert len(gresponse["i2"]["successful"]) == 1
    assert len(gresponse["i2"]["failed"]) == 1
    assert gresponse["i2"]["successful"] == [{"name": "state2", "result": True}]
    assert gresponse["i2"]["failed"] == [{"name": "state1", "result": False}]

    assert len(gresponse["i3"]["successful"]) == 1
    assert len(gresponse["i3"]["failed"]) == 1
    assert gresponse["i3"]["successful"] == [{"name": "state1", "result": True}]
    assert gresponse["i3"]["failed"] == [{"name": "state2", "result": False}]

    assert len(gresponse["i4"]["successful"]) == 0
    assert len(gresponse["i4"]["failed"]) == 2
    assert {"name": "state1", "result": False} in gresponse["i4"]["failed"]
    assert {"name": "state2", "result": False} in gresponse["i4"]["failed"]

    assert len(response.failed_nodes) == 3
    assert response.has_failed == True

def test_state_response_valid():
    response = StateResponse()
    response["i1"] = {"state1": {"result": True}, "state2": {"result": True}}
    response["i2"] = {"state1": {"result": False}, "state2": {"result": True}}
    assert response.validate() is None


def test_state_response_invalid_template():
    response = StateResponse()
    response["i1"] = ["Rendering SLS 'base:anaconda' failed: Jinja variable 'prefix' is undefined"]
    response["i2"] = {"state1": {"result": False}, "state2": {"result": True}}
    assert response.has_no_matching_sls() is False
    assert response.has_template_error() is True
    with pytest.raises(SaltException):
        response.validate()


def test_state_response_invalid_no_sls():
    response = StateResponse()
    response["i1"] = ["No matching sls found for 'anaconda.sls' in env 'base'"]
    response["i2"] = {"state1": {"result": False}, "state2": {"result": True}}
    assert response.has_no_matching_sls() is True
    assert response.has_template_error() is False
    with pytest.raises(SaltException):
        response.validate()
