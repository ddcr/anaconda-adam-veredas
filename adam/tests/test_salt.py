from __future__ import absolute_import, print_function, division

import posixpath

from .utils import remotetest, cluster


@remotetest
def test_anaconda_user(cluster):
    from adam.salt import create_anaconda_group, create_anaconda_user
    create_anaconda_group(cluster)
    create_anaconda_user(cluster)
    rootdir = cluster.rootdir
    for node in cluster.nodes:
        client = node.ssh_client
        u_command = "id -u {}".format(cluster.salt_settings.salt_username)
        g_command = "id -g {}".format(cluster.salt_settings.salt_username)
        getent_command = "getent group {}".format(cluster.salt_settings.salt_groupname)

        uid = int(client.exec_command(u_command)["stdout"])
        gid = int(client.exec_command(g_command)["stdout"])
        assert client.exec_command(getent_command)["exit_code"] == 0
        assert client.exec_command(u_command)["exit_code"] == 0
        assert client.dir_exists(rootdir) is True
        stat = client.stat(rootdir)
        assert stat.st_mode == 16877  # equivalent of chmod 755 oct(16877)
        assert stat.st_uid == uid
        assert stat.st_gid == gid

    [node.ssh_client.close() for node in cluster.nodes]


@remotetest
def test_install_miniconda(cluster):
    from adam.salt import install_miniconda
    install_miniconda(cluster)

    rootdir = cluster.rootdir
    salt_prefix = cluster.salt_prefix
    conda_bin = cluster.conda_bin

    for node in cluster.nodes:
        client = node.ssh_client
        root_uid = int(client.exec_command("id -u root")["stdout"])
        root_gid = int(client.exec_command("id -g root")["stdout"])

        u_command = "id -u {}".format(cluster.salt_settings.salt_username)
        g_command = "id -g {}".format(cluster.salt_settings.salt_username)
        anaconda_uid = int(client.exec_command(u_command)["stdout"])
        anaconda_gid = int(client.exec_command(g_command)["stdout"])
        assert client.stat(salt_prefix).st_uid == root_uid
        assert client.stat(salt_prefix).st_gid == root_gid
        assert client.stat(conda_bin).st_uid == root_uid
        assert client.stat(conda_bin).st_gid == root_gid
        # Permissions for /opt/continuum did not change
        assert client.stat(rootdir).st_uid == anaconda_uid
        assert client.stat(rootdir).st_gid == anaconda_gid
        client.close()


@remotetest
def test_install_salt(cluster):
    from adam.salt import configure_condarc, install_salt
    configure_condarc(cluster)
    install_salt(cluster)

    salt_prefix = cluster.salt_prefix
    conda_bin = cluster.conda_bin
    salt_minion_pid = posixpath.join(salt_prefix, "var", "run", "salt-minion.pid")
    salt_master_pid = posixpath.join(salt_prefix, "var", "run", "salt-master.pid")
    salt_api_pid = posixpath.join(salt_prefix, "var", "run", "salt-api.pid")

    for node in cluster.nodes:
        client = node.ssh_client
        uid = int(client.exec_command("id -u root")["stdout"])
        gid = int(client.exec_command("id -g root")["stdout"])
        assert client.stat(conda_bin).st_uid == uid
        assert client.stat(conda_bin).st_gid == gid

        assert client.exec_command("kill -0 $(cat {})".format(salt_minion_pid),
                                   sudo=True)["exit_code"] == 0, "Node {}".format(node.host)
        if node.host == cluster.head.host:
            assert client.exec_command("kill -0 $(cat {})".format(salt_master_pid),
                                       sudo=True)["exit_code"] == 0, "Node {}".format(node.host)
            assert client.exec_command("kill -0 $(cat {})".format(salt_api_pid),
                                       sudo=True)["exit_code"] == 0, "Node {}".format(node.host)
        client.close()


@remotetest
def test_upload_formulas(cluster):
    from adam.salt import upload_formulas
    upload_formulas(cluster)

    salt_prefix = cluster.salt_prefix
    states = posixpath.join(salt_prefix, "srv", "salt", "base", "top.sls")
    pillars = posixpath.join(salt_prefix, "srv", "pillar", "base", "top.sls")

    client = cluster.head.ssh_client
    uid = int(client.exec_command("id -u root")["stdout"])
    gid = int(client.exec_command("id -g root")["stdout"])
    assert client.file_exists(states)
    assert client.stat("{}".format(states)).st_uid == uid
    assert client.stat("{}".format(states)).st_gid == gid
    assert client.file_exists(pillars)
    assert client.stat("{}".format(pillars)).st_uid == uid
    assert client.stat("{}".format(pillars)).st_gid == gid
    client.close()


@remotetest
def test_upload_settings(cluster):
    from adam.salt import upload_settings
    upload_settings(cluster)

    salt_prefix = cluster.salt_prefix
    settings_sls = posixpath.join(salt_prefix, "srv", "pillar", "base", "settings.sls")

    client = cluster.head.ssh_client
    uid = int(client.exec_command("id -u root")["stdout"])
    gid = int(client.exec_command("id -g root")["stdout"])
    assert client.file_exists(settings_sls)
    assert client.stat("{}".format(settings_sls)).st_uid == uid
    assert client.stat("{}".format(settings_sls)).st_gid == gid
    client.close()


@remotetest
def test_salt_ping(cluster):
    response = cluster.salt("*", "test.ping")
    assert len(response) == len(cluster.nodes)
    for node in cluster.nodes:
        assert node.host in response
        assert response[node.host] is True


@remotetest
def test_upload_config(cluster, request):
    from adam.salt import upload_config
    testname = request.node.name
    clients = [node.ssh_client for node in cluster.nodes]
    remote = "/tmp/{}.yml".format(testname)
    data = {"first": {"second": "value"}}
    upload_config(clients, remote, data)
    for node in cluster.nodes:
        client = node.ssh_client
        with client.open(remote, "r") as f:
            read = f.read().decode("utf-8")
            assert read == "first:\n  second: value\n"
        client.close()


@remotetest
def test_set_roles_defaults(cluster):
    from adam.salt import set_roles
    set_roles(cluster)

    response = cluster.salt("*", "grains.item", arg=["roles"])
    print(response)
    for node in cluster.nodes:
        for role in node.roles:
            assert role in response[node.host]["roles"]


@remotetest
def test_set_roles_extra(cluster):
    from adam.salt import set_roles
    roles = {node.host: "extra-role-{0}-{1}".format(i, node.host) for i, node in enumerate(cluster.nodes)}
    set_roles(cluster, roles)

    response = cluster.salt("*", "grains.item", arg=["roles"])

    for node_host, role in roles.items():
        assert role in response[node_host]["roles"]
