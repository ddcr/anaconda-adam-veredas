{% from 'service_scripts/map.jinja' import adam_salt_master, adam_salt_minion, adam_salt_api with context %}

adam-salt-master-initd:
  file.managed:
    - name: /etc/init.d/adam-salt-master
    - source: {{ adam_salt_master.template }}
    - mode: 0755
    - user: root
    - group: root
    - template: jinja

adam-salt-minion-initd:
  file.managed:
    - name: /etc/init.d/adam-salt-minion
    - source: {{ adam_salt_minion.template }}
    - mode: 0755
    - user: root
    - group: root
    - template: jinja

adam-salt-api-initd:
  file.managed:
    - name: /etc/init.d/adam-salt-api
    - source: {{ adam_salt_api.template }}
    - mode: 0755
    - user: root
    - group: root
    - template: jinja


adam-salt-api:
  service.running:
    - enable: True

adam-salt-minion:
  service.running:
    - enable: True

adam-salt-master:
  service.running:
    - enable: True
