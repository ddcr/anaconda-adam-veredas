{% from 'service_scripts/map.jinja' import adam_salt_minion with context %}

adam-salt-minion-initd:
  file.managed:
    - name: /etc/init.d/adam-salt-minion
    - source: {{ adam_salt_minion.template }}
    - mode: 0755
    - user: root
    - group: root
    - template: jinja

adam-salt-minion:
  service.running:
    - enable: True
