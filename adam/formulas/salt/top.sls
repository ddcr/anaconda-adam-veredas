{%- set states = salt['cp.list_states'](env) -%}

base:
  '*':
    {% if 'roles' in grains %}
    {% for role in grains['roles'] %}
    {% if role in states %}
    - {{ role }}
    {% endif %}
    {% endfor %}
    {% endif %}
