{%- from "anaconda/settings.sls" import anaconda_dir, miniconda_curl_cmd, rootdir,
                                salt_username, salt_groupname
                                with context -%}
{%- from "supervisor/settings.sls" import supervisor_dir, supervisord, supervisord_conf with context -%}

include:
  - system.proxy

supervisor-miniconda-download:
  cmd.run:
    - name: {{ miniconda_curl_cmd }}
    - unless: test -e {{ anaconda_dir }}/bin/conda
    - require:
      - sls: system.proxy

supervisor-miniconda-perms:
  file.managed:
    - name: {{ rootdir }}/miniconda.sh
    - user: {{ salt_username }}
    - group: {{ salt_groupname }}

supervisor-miniconda-install:
  cmd.run:
    - name: bash {{ rootdir }}/miniconda.sh -b -p {{ supervisor_dir }}
    - user: {{ salt_username }}
    - unless: test -e {{ supervisor_dir }}/bin/conda
    - require:
      - cmd: supervisor-miniconda-download

supervisor-licenses:
  file.recurse:
    - name: {{ supervisor_dir }}/licenses/
    - source: salt://licenses/
    - include_empty: True
    - require:
      - cmd: supervisor-miniconda-install

supervisor_condarc:
  file.managed:
    - name: {{ supervisor_dir }}/.condarc
    - source: salt://anaconda/templates/condarc.conf
    - template: jinja
    - user: {{ salt_username }}
    - require:
      - cmd: supervisor-miniconda-install

install-supervisor:
  conda.managed:
    - name: root
    - packages:
      - supervisor
    - rootdir: {{ supervisor_dir }}
    - user: {{ salt_username }}
    - unless: test -e {{ supervisor_dir }}/bin/supervisord
    - require:
      - cmd: supervisor-miniconda-install

supervisord-log:
  file.directory:
    - name: {{ supervisor_dir }}/var/log/supervisor/
    - makedirs: true
    - require:
      - conda: install-supervisor

supervisord-run:
  file.directory:
    - name: {{ supervisor_dir }}/var/run/
    - makedirs: true
    - require:
      - conda: install-supervisor

supervisord.conf:
  file.managed:
    - name: {{ supervisord_conf }}
    - source: salt://supervisor/templates/supervisord.conf
    - template: jinja
    - makedirs: true
    - require:
      - conda: install-supervisor

supervisord-running:
  cmd.run:
    - name: {{ supervisord }} -c {{ supervisord_conf }}
    - unless: |
              test -e {{ supervisor_dir }}/var/run/supervisor.sock &&
              test -e {{ supervisor_dir }}/var/run/supervisord.pid &&
              ps cax | grep supervisord > /dev/null
    - require:
      - conda: install-supervisor
      - file: supervisord-log
      - file: supervisord-run
      - file: supervisord.conf
