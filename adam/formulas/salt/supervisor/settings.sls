{% from "anaconda/settings.sls" import rootdir with context %}

{% set default_prefix = rootdir ~ "/supervisor"  %}
{% set supervisor_dir = salt["grains.get"]("supervisor:prefix", salt["pillar.get"]("supervisor:prefix", default_prefix))  %}

{%- set supervisord = supervisor_dir ~ '/bin/supervisord' -%}
{%- set supervisorctl = supervisor_dir ~ '/bin/supervisorctl' -%}
{%- set supervisord_dir = supervisor_dir ~ '/etc/supervisor/' -%}
{%- set supervisord_log_dir = supervisor_dir ~ '/var/log/' -%}
{%- set supervisor_conf_d = supervisor_dir ~ "/etc/supervisor/conf.d/" -%}
{%- set supervisord_conf = supervisor_dir ~ "/etc/supervisor/supervisord.conf" -%}
