from __future__ import absolute_import, division, print_function

from schematics.models import Model
from schematics.types import IntType
from schematics.types import StringType
from schematics.types import BooleanType
from schematics.types.compound import DictType
from schematics.transforms import blacklist


class CondaRemoteResponse(Model):
    stdout = StringType(required=True)  #standard output from conda
    stderr = StringType(required=True)  # standard error from conda
    success = BooleanType(required=True)  # true/false
    pid = IntType(required=True)  # PID of job
    prefix = StringType(required=True)  #(conda.prefix or env name)
    retcode = IntType(required=True)  # return code from command being executed

    # should we require this ?
    host = StringType(default='Unknown')

    message = StringType()
    packages = DictType(DictType(StringType))

    schema_error = StringType()  # error with Conversion or Requirements

    # conda 4.1.X handling
    error = StringType()
    error_type = StringType()

    # conda 4.2.X handling
    exception_name = StringType()
    exception_type = StringType()

    error_keys = ['error', 'schema_error', 'error_type', 'exception_name',
                  'exception_type']

    class Options:
        roles = {'ignore_host': blacklist('host')}

    def __repr__(self):
        return "CondaRemoteResponse <{host}>".format(host=getattr(self, 'host', 'Unknown'))
