from __future__ import absolute_import, division, print_function

import os
import json
import logging
import posixpath

from schematics.exceptions import ModelConversionError, ModelValidationError

from conda_models import CondaRemoteResponse

logger = logging.getLogger(__name__)


def __virtual__():
    return True


__func_alias__ = {"info_": "info", "list_": "list", "conda_prefix": "prefix"}


def validate_response(func, *args, **kwargs):

    def validator(*args, **kwargs):
        # remove __pub_fun/__pub_jid/__pub_pid/__pub_tgt/__pub_tgt_type...
        salt_kwargs = [k for k in kwargs.keys() if "__pub" in k]

        for skwargs in salt_kwargs:
            try:
                kwargs.pop(skwargs)
            except KeyError:
                pass
        logger.debug("Calling Conda Remote %s with Arguments : %s, %s" % (func.__name__, args, kwargs))
        conda_resp = func(*args, **kwargs)
        conda_resp.host = __grains__["id"]
        try:
            conda_resp.validate()
        except ModelValidationError as e:
            conda_resp.schema_error = str(e.messages)
        except ModelConversionError as e:
            conda_resp.schema_error = str(e.messages)
        return conda_resp.to_primitive()

    validator._original = func
    return validator


def _get_rootdir(rootdir=None):
    if rootdir is None:
        rootdir = __salt__["pillar.get"]("conda:rootdir", "/opt/continuum/")
        return rootdir
    return rootdir


def conda_prefix(user=None, rootdir=None):
    if rootdir is None:
        rootdir = _get_rootdir(rootdir=rootdir)
        anaconda_dir = posixpath.join(rootdir, "anaconda")
        return anaconda_dir
    return rootdir


def _get_salt_username(user=None):
    """
    Get salt user from pillar data

    salt_settings:
      salt_username: anaconda

    Parameters
    ----------
    user: str
        default: anaconda

    Returns
    -------
    str: salt user (default: anaconda)
    """

    if user is None:
        user = __salt__["pillar.get"]("salt_settings:salt_username",
                                         "anaconda")
        return user
    return user


@validate_response
def create(env, pkgs, prefix=False, channels="", user=None, rootdir=None):
    """
    Create a conda environment and install some packages

    Parameters
    ----------
    env : str
        Environment name or path
        If path use kwarg: {"prefix": true}
    pkgs : str
        Comma delimited list of packages to be installed.
        Example: ``python=3,numpy=1.7,pandas,boto"``
    prefix : bool (default=False)
        Indicates if the env arg should be used as prefix install (path)
        for example: ``conda create -p /foo/bar python=3``
    channels : str (default=None)
        Comma delimited list of additional channels to search for packages
    user : str (default: anaconda)
        User to run the commands as
    rootdir : str (default=/opt/continuum/anaconda)
        If multiple anaconda installations which one to use

    Examples
    --------
    .. code-block:: bash

        salt "*" conda.create py3k pkgs=python=3,numpy
        salt "*" conda.create py3k pkgs=python=3,numpy channels=conda-forge,anaconda-adam
        salt "*" conda.create /tmp/env2 pkgs=python=3,numpy prefix=true
        salt "*" conda.create /tmp/env2 pkgs=python=3,numpy prefix=true user=root
        salt "*" conda.create custom_env  # Fails
    """

    user = _get_salt_username(user)

    pkgs = pkgs.split(",")
    logger.debug("Attempting to creating env {0} with packages {1}".format(env, pkgs))

    channel_list = channels.split(",")
    cmd = _create_conda_cmd("create", args=pkgs, env=env, prefix=prefix, channels=channel_list, user=user, rootdir=rootdir)
    ret = _execcmd(cmd, user=user)

    cresp = CondaRemoteResponse(ret)

    try:
        conda_output = json.loads(cresp.stdout)
    except TypeError:
        conda_output = {}
    except ValueError:
        # probably the command didn't run
        # likely caused by anaconda installation not existing
        conda_output = {}

    if cresp.retcode == 0:
        if "actions" in conda_output:
            cresp.prefix = conda_output["actions"].get("PREFIX", conda_prefix(user=user, rootdir=rootdir))
        cresp.message = conda_output.get("message", "")
        cresp.success = conda_output.get("success", False)
    else:
        cresp.error = conda_output.get("error", "")
        cresp.message = conda_output.get("message", "")
        cresp.exception_name = conda_output.get("exception_name", "")
        cresp.exception_type = conda_output.get("exception_type", "")
        cresp.error_type = conda_output.get("error_type", "")
        cresp.prefix = os.path.join(conda_prefix(user=user, rootdir=rootdir), env)
        cresp.success = False

    return cresp


@validate_response
def install(pkgs, env="root", prefix=False, channels="", user=None, rootdir=None):
    """
    Install conda packages in an environment

    Parameters
    ----------
    pkgs: str
        Comma delimited list of packages to be installed.
        Example: ``python=3,numpy=1.7,pandas,boto"``
    env: str (default=root)
        Name of environment to install packages into
    prefix : bool (default=False)
        Indicates if the env arg should be used as prefix install (path)
        for example: ``conda install -p /foo/bar numpy``
    channels : str (default="")
        Comma delimited list of additional channels to search for packages
    user : str (default: anaconda)
        User to run the commands as
    rootdir : str (default=/opt/continuum/anaconda)
        If multiple anaconda installations which one to use

    Examples
    --------
    .. code-block:: bash

        salt "*" conda.install env=name pkgs=python=3,scipy,pandas=0.14
        salt "*" conda.install env=/tmp/env2 pkgs=pandas prefix=true
        salt "*" conda.install env=/tmp/env2 pkgs=pandas channel=adam
    """
    user = _get_salt_username(user)

    pkgs = format_pkg_str_list(pkgs)
    channel_list = channels.split(",")
    conda_cmd = _create_conda_cmd(
        "install", args=[pkgs],
        channels=channel_list, env=env,
        prefix=prefix, user=user,
        rootdir=rootdir)
    conda_ret = _execcmd(conda_cmd, user=user)

    cresp = CondaRemoteResponse(conda_ret)
    cresp.prefix = os.path.join(conda_prefix(user=user, rootdir=rootdir), "root")

    try:
        conda_output = json.loads(cresp.stdout)
    except TypeError:
        conda_output = []
    except ValueError:
        # probably the command didn't run
        # likely caused by anaconda installation not existing
        conda_output = {}

    if cresp.retcode == 0:
        cresp.success = conda_output.get("success", False)
        if "actions" in conda_output:
            cresp.prefix = conda_output["actions"].get("PREFIX", conda_prefix(user=user, rootdir=rootdir))
        cresp.message = conda_output.get("message", "")
    else:
        cresp.error = conda_output.get("error", "")
        cresp.message = conda_output.get("message", "")
        cresp.exception_name = conda_output.get("exception_name", "")
        cresp.exception_type = conda_output.get("exception_type", "")
        cresp.success = False

    return cresp


@validate_response
def update(pkgs, env="root", prefix=False, channels="", user=None, rootdir=None):
    """
    Update conda packages in a conda env.

    Parameters
    ----------
    pkgs: str
        Comma delimited list of packages to be installed.
        Example: ``python=3,numpy=1.7,pandas,boto"``
    env: str (default=root)
        Name of environment to install packages into
    prefix : bool (default=False)
        Indicates if the env arg should be used as prefix install (path)
        for example: ``conda update -p /foo/bar numpy``
    channels : str (default="")
        Comma delimited list of additional channels to search for packages
    user : str (default: anaconda)
        User to run the commands as
    rootdir : str (default=/opt/continuum/anaconda)
        If multiple anaconda installations which one to use

    Examples
    --------
    .. code-block:: bash

        salt "*" conda.update env=name pkgs=scipy,pandas
        salt "*" conda.update env=name pkgs=scipy,pandas channels=conda-forge
        salt "*" conda.update env=/tmp/env2 pkgs=pandas prefix=true
        salt "*" conda.update env=/tmp/env2 pkgs=pandas channel=adam
    """
    user = _get_salt_username(user)

    pkgs = format_pkg_str_list(pkgs)
    channel_list = channels.split(",")
    conda_cmd = _create_conda_cmd(
        "update", args=[pkgs],
        channels=channel_list, env=env,
        prefix=prefix, user=user,
        rootdir=rootdir)
    conda_ret = _execcmd(conda_cmd, user=user)

    cresp = CondaRemoteResponse(conda_ret)
    cresp.prefix = os.path.join(conda_prefix(user=user, rootdir=rootdir), "root")

    try:
        conda_output = json.loads(cresp.stdout)
    except TypeError:
        conda_output = {}
    except ValueError:
        # probably the command didn't run
        # likely caused by anaconda installation not existing
        conda_output = {}

    if cresp.retcode == 0:
        if "actions" in conda_output:
            cresp.prefix = conda_output["actions"].get("PREFIX", conda_prefix(user=user, rootdir=rootdir))
        cresp.message = conda_output.get("message")
        cresp.success = conda_output.get("success", False)
    else:
        cresp.error = conda_output.get("error", "")
        cresp.message = conda_output.get("message", "")
        cresp.exception_name = conda_output.get("exception_name", "")
        cresp.exception_type = conda_output.get("exception_type", "")
        cresp.error_type = conda_output.get("error_type", "")
        cresp.success = False

    return cresp


@validate_response
def remove(pkgs=None, env="root", prefix=False, user=None, rootdir=None):
    """
    Remove conda packages in a conda env

    Parameters
    ----------
    pkgs: str
        Comma delimited list of packages to be installed.
        Example: ``python=3,numpy=1.7,pandas,boto"``
    env: str (default=root)
        Name of environment to install packages into
    prefix : bool (default=False)
        Indicates if the env arg should be used as prefix install (path)
        for example: ``conda remove -p /foo/bar numpy``
    user : str (default: anaconda)
        User to run the commands as
    rootdir : str (default=/opt/continuum/anaconda)
        If multiple anaconda installations which one to use

    Examples
    --------
    .. code-block:: bash

        salt "*" conda.remove env=name pkgs=pandas
        salt "*" conda.remove env=/tmp/env2 pkgs=pandas prefix=true
    """

    user = _get_salt_username(user)

    pkgs = format_pkg_str_list(pkgs)
    conda_cmd = _create_conda_cmd("remove", args=[pkgs], env=env, prefix=prefix, user=user, rootdir=rootdir)
    conda_ret = _execcmd(conda_cmd, user=user)

    cresp = CondaRemoteResponse(conda_ret)
    cresp.prefix = os.path.join(conda_prefix(user=user, rootdir=rootdir), "root")
    try:
        conda_output = json.loads(cresp.stdout)
    except TypeError:
        conda_output = []
    except ValueError:
        # probably the command didn't run
        # likely caused by anaconda installation not existing
        conda_output = {}

    if cresp.retcode == 0:
        if "actions" in conda_output:
            cresp.prefix = conda_output["actions"].get("PREFIX", conda_prefix(user=user, rootdir=rootdir))
        cresp.message = conda_output.get("message", "")
        cresp.success = conda_output.get("success", False)
    else:
        cresp.success = False
        cresp.error = conda_output.get("error", "")
        cresp.message = conda_output.get("message", "")
        cresp.exception_name = conda_output.get("exception_name", "")
        cresp.exception_type = conda_output.get("exception_type", "")
        cresp.error_type = conda_output.get("error_type", "")

    return cresp


@validate_response
def list_(env="root", prefix=False, user=None, rootdir=None):
    """
    List the installed packages on an environment

    Parameters
    ----------
    env: str (default=root)
        Name of environment to install packages into
    prefix : bool (default=False)
        Indicates if the env arg should be used as prefix install (path)
        for example: ``conda list -p /foo/bar python``
    user : str (default: anaconda)
        User to run the commands as
    rootdir : str (default=/opt/continuum/anaconda)
        If multiple anaconda installations which one to use
    """

    user = _get_salt_username(user)

    cmd = _create_conda_cmd(
        "list", env=env, user=user,
        prefix=prefix, rootdir=rootdir,
        yes=False, quiet=False,
        json=True)
    ret = _execcmd(cmd, user=user)
    cresp = CondaRemoteResponse(ret)
    cresp.prefix = os.path.join(conda_prefix(user, rootdir=rootdir), env if env else "root")
    try:
        conda_output = json.loads(cresp.stdout)
    except TypeError:
        conda_output = []
    except ValueError:
        # probably the command didn't run
        # likely caused by anaconda installation not existing
        conda_output = {}

    if cresp.retcode == 0:
        packages = {}

        if isinstance(conda_output[0], dict):
            extract_pkg_info = lambda x: x.get("dist_name")
        else:
            extract_pkg_info = lambda x: x

        for dist_data in conda_output:
            pkg_data = extract_pkg_info(dist_data)
            pkg_info = pkg_data.split("-")
            build = pkg_info[-1]
            version = pkg_info[-2]
            name = "-".join(pkg_info[0:-2])
            packages[name] = {"version": version, "build": build, "raw": pkg_data}
        cresp.packages = packages
        cresp.success = True
    else:
        cresp.success = False

    return cresp


@validate_response
def info_(options=None, user=None, rootdir=None):
    """
    Get information about conda installation.

    Parameters
    ----------
    options: str (default=None)
        Comma delimited list of options used

        - a (all): Show all information, (environments, license, and system information)
        - e (envs): List all known conda environments
        - l (license): Display information about conda licenses
        - s (system): List environment variables
    user : str (default: anaconda)
        User to run the commands as
    rootdir : str (default=/opt/continuum/anaconda)
        If multiple anaconda installations which one to use

    Examples
    --------
    .. code-block:: bash

        salt "*" conda.info
        salt "*" conda.info a
        salt "*" conda.info e,l,s
    """

    user = _get_salt_username(user)

    cmd = [_get_conda_path(user=user, rootdir=rootdir), "info"]

    if options:
        opts = options.split(",")
        opts = "".join(opts)
        options = "-{}".format(opts)
        cmd.append(options)

    logger.debug(cmd)
    ret = _execcmd(cmd, user=user)

    cresp = CondaRemoteResponse(ret)
    cresp.prefix = os.path.join(conda_prefix(user, rootdir=rootdir), "root")

    if cresp.retcode == 0:
        cresp.success = True
    else:
        cresp.success = False

    return cresp


def format_pkg_str_list(pkgs):
    """Format a comma separated string of to string valid for a conda command.
    """
    if isinstance(pkgs, str):
        return pkgs.replace(",", " ")
    else:
        return ""


def _create_conda_cmd(conda_cmd,
                      args=None,
                      channels=None,
                      env=None,
                      prefix=False,
                      user=None,
                      yes=True,
                      quiet=True,
                      json=True,
                      rootdir=None):
    cmd = [_get_conda_path(user=user, rootdir=rootdir)]
    cmd.append(conda_cmd)

    if env and not prefix:
        cmd.extend(["-n", env])
    elif env and prefix:
        cmd.extend(["-p", env])

    if channels:
        for channel in channels:
            if channel:
                cmd.extend(["-c", channel])

    if args is not None and type(args) is list and args != []:
        cmd.extend(args)

    if yes:
        cmd.append("-y")
    if quiet:
        cmd.append("-q")

    if json:
        cmd.append("--json")
    return cmd


def _get_conda_path(user=None, rootdir=None):
    """
    Get path to the conda binary with CONDARC env defined

    Parameters
    ----------
    user : str
        user
    rootdir : str
        rootdir of adam installation

    Returns
    -------
        full path to conda binary (CONDARC=/opt/continuum/.condarc /opt/continuum/anaconda/bin/conda)

    """

    CONDARC = os.path.join(_get_rootdir(rootdir), ".condarc")
    conda_bin = os.path.join(conda_prefix(user=user, rootdir=rootdir), "bin", "conda")
    if not os.path.exists(CONDARC):
        modified_conda_bin = "{conda_bin}".format(CONDARC=CONDARC, conda_bin=conda_bin)
    else:
        modified_conda_bin = "CONDARC={CONDARC} {conda_bin}".format(CONDARC=CONDARC, conda_bin=conda_bin)
    return modified_conda_bin


def get_env_path(env=None, prefix=False, user=None, rootdir=None):
    """
    Get the path to a named conda environment inside the anaconda prefix
    """
    if env == "root":
        return conda_prefix(user=user, rootdir=rootdir)
    elif env is not None:
        return os.path.join(conda_prefix(user=user), "envs", env)
    else:
        return conda_prefix(user=user, rootdir=rootdir)


def _execcmd(cmd, user=None):
    """
    Small wrapper to execute a command
    """
    cmd = " ".join(cmd)

    # take extra precaution and get user one more time
    # cmd.run_all will fail if user is None

    user = _get_salt_username(user)
    return __salt__["cmd.run_all"](cmd, runas=user, python_shell=True)
