{%- from "anaconda/settings.sls" import ssl_verify, http_proxy, https_proxy, channels, channel_alias, conda_canary with context -%}

# channel locations. These override conda defaults, i.e., conda will
# search *only* the channels listed here, in the order given. Use "defaults" to
# automatically include all default channels. Non-url channels will be
# interpreted as binstar usernames (this can be changed by modifying the
# channel_alias key; see below).
channels:
{%- for channel in channels %}
  - {{ channel }}
{%- endfor %}
{%- if conda_canary %}
  - conda-canary
{%- endif %}

# Alias to use for non-url channels used with the -c flag. Default is https://conda.anaconda.org/
channel_alias: {{ channel_alias }}

# when creating new environments add these packages by default
create_default_packages:
  - python
  - pip

{% if http_proxy is not none or https_proxy is not none -%}
proxy_servers:
  {% if http_proxy is not none -%}
  http: {{ http_proxy }}
  {%- endif %}
  {% if https_proxy is not none -%}
  https: {{ https_proxy }}
  {%- endif %}
{%- endif %}

auto_update_conda: false

{% if ssl_verify is not none-%}
ssl_verify: {{ ssl_verify }}
{%- endif %}
