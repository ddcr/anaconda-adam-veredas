{%- from "anaconda/settings.sls" import rootdir, anaconda_dir, download_url,
                                        salt_username, salt_groupname,
                                        download_hash, anaconda_curl_cmd with context %}

include:
  - system.proxy


anaconda-download:
  cmd.run:
    - name: {{ anaconda_curl_cmd }}
    - unless: test -e {{ anaconda_dir }}/bin/conda
    - require:
      - sls: system.proxy

anaconda-file-perms:
  file.managed:
    - name: {{ rootdir }}/anaconda.sh
    - user: {{ salt_username }}
    - group: {{ salt_groupname }}
    - require:
      - cmd: anaconda-download


anaconda-install:
  cmd.run:
    - name: bash {{ rootdir }}/anaconda.sh -b -p {{ anaconda_dir }}
    - unless: test -e {{ anaconda_dir }}/bin/conda
    - user: {{ salt_username }}
    - require:
      - cmd: anaconda-download

anaconda-license:
  file.recurse:
    - name: {{ anaconda_dir }}/licenses/
    - source: salt://licenses/
    - user: {{ salt_username }}
    - group: {{ salt_groupname }}
    - include_empty: True
    - require:
      - cmd: anaconda-install

condarc:
  file.managed:
    - name: {{ rootdir }}/.condarc
    - source: salt://anaconda/templates/condarc.conf
    - template: jinja
    - user: {{ salt_username }}
    - group: {{ salt_groupname }}
    - require:
      - cmd: anaconda-install

anaconda-install-pip:
  cmd.run:
    - name: {{ anaconda_dir }}/bin/conda install pip -y -q
    - unless: test -e {{ anaconda_dir }}/bin/pip
    - require:
      - file: condarc
