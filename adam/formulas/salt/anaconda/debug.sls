{%- from 'system/settings.sls' import tmp_dir with context %}
{%- from 'anaconda/settings.sls' import anaconda_dir with context %}

{{ tmp_dir }}/salt/anaconda.debug:
  file.managed:
    - makedirs: true
    - contents: |
        anaconda_dir: {{ anaconda_dir }}
