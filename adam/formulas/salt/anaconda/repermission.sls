{%- from "anaconda/settings.sls" import anaconda_dir, salt_username,
                                        rootdir, salt_groupname with context %}

re_permission:
  file.directory:
    - name: {{ anaconda_dir }}/
    - user: {{ salt_username }}
    - group: {{ salt_groupname }}
    - recurse:
      - user
      - group

condarc:
  file.managed:
    - name: {{ rootdir }}/.condarc
    - user: {{ salt_username }}
    - group: {{ salt_groupname }}
