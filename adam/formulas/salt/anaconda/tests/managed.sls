{%- from 'system/settings.sls' import tmp_dir with context %}

myenv:
  conda.managed:
    - name: {{ tmp_dir }}/adam-tests/conda-state-env
    - prefix: true
    - packages:
      - numpy
      - scipy
    - channels:
      - conda-forge
      - anaconda-adam


add_packages:
  conda.managed:
    - name: {{ tmp_dir }}/adam-tests/conda-state-env
    - prefix: true
    - packages:
      - boto
