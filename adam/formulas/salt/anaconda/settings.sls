{% set rootdir = salt["pillar.get"]("conda:rootdir", "/opt/continuum/")  %}
{% set default_prefix = rootdir ~ "/anaconda/"  %}
{% set anaconda_dir = salt["pillar.get"]("anaconda:prefix", default_prefix)  %}

{% set miniconda_default_url = "http://repo.continuum.io/miniconda/Miniconda2-4.2.12-Linux-x86_64.sh"  %}
{% set miniconda_download_url = salt["pillar.get"]("conda:miniconda_url")  %}
{% set miniconda_default_hash = "md5=c8b836baaa4ff89192947e4b1a70b07e"  %}
{% set miniconda_download_hash = salt["pillar.get"]("conda:miniconda_hash")  %}

{% set default_url = "http://repo.continuum.io/archive/Anaconda2-4.2.0-Linux-x86_64.sh"  %}
{% set download_url = salt["pillar.get"]("conda:anaconda_url", default_url)  %}
{% set default_hash = "md5=a0d1fbe47014b71c6764d76fb403f217"  %}
{% set download_hash =  salt["pillar.get"]("conda:anaconda_hash", default_hash)  %}

## .condarc settings
{% set default_channels = ["defaults"] %}
{% set channels = salt["pillar.get"]("conda:channels", default_channels)  %}
{% set conda_canary = salt["pillar.get"]("conda:conda_canary", False)  %}
{% set default_channel_alias = "https://conda.anaconda.org/"  %}
{% set channel_alias =  salt["pillar.get"]("conda:channel_alias", default_channel_alias)  %}
{%- set http_proxy = salt["pillar.get"]("network:http_proxy", none) -%}
{%- set https_proxy = salt["pillar.get"]("network:https_proxy", none) -%}
{%- set ssl_verify = salt["pillar.get"]("conda:ssl_verify", none) -%}

{%- set anaconda_curl_cmd = " curl -q -L -o " ~ rootdir ~ "/anaconda.sh " ~ download_url %}
{%- set miniconda_curl_cmd = " curl -q -L -o " ~ rootdir ~ "/miniconda.sh " ~ miniconda_download_url %}

{% if http_proxy is not none -%}
{%- set anaconda_curl_cmd = "http_proxy= "~ http_proxy ~ " " ~ anaconda_curl_cmd %}
{%- set miniconda_curl_cmd = "http_proxy= "~ http_proxy ~ " " ~ miniconda_curl_cmd %}
{% endif %}

{% if https_proxy is not none -%}
{%- set anaconda_curl_cmd = "https_proxy= "~ https_proxy ~ " " ~ anaconda_curl_cmd %}
{%- set miniconda_curl_cmd = "https_proxy= "~ https_proxy ~ " " ~ miniconda_curl_cmd %}
{% endif %}

# salt_settings
{% set salt_username = salt["pillar.get"]("salt_settings:salt_username", "anaconda")  %}
{% set salt_groupname = salt["pillar.get"]("salt_settings:salt_groupname", "anaconda")  %}
