{%- set http_proxy = salt["pillar.get"]("network:http_proxy", none) -%}
{%- set https_proxy = salt["pillar.get"]("network:https_proxy", none) -%}

{% if http_proxy is not none or https_proxy is not none -%}

{% if http_proxy is not none -%}
http_proxy:
  environ.setenv:
    - value: {{ http_proxy }}
{%- endif %}

{% if https_proxy is not none -%}
https_proxy:
  environ.setenv:
    - value: {{ https_proxy }}
{%- endif %}

{%- else %}

proxy-noop:
  cmd.run:
    - name: echo 1
    - unless: test -e /tmp

{%- endif %}
