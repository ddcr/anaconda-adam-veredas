import os

__virtualname__ = "conda"


def __virtual__():
    """
    Only load if the conda module is available in __salt__
    """
    if "conda.list" in __salt__:
        return __virtualname__
    return False


def _check_if_installed(pkg, env="root", prefix=False, user=None, rootdir=None):
    """
    Returns
    -------
        None means the command failed to run
        True means the package is installed
        False means the package is not installed

    """
    user = _get_salt_username(user)

    conda_list = __salt__["conda.list"](env=env, prefix=prefix, user=user)
    return pkg in conda_list["packages"]


def _check_if_environment_exists(env="root", prefix=False, user=None, rootdir=None):

    user = _get_salt_username(user)

    if not prefix:
        # Environent in $PREFIX/envs/{env}
        prefix_ = __salt__["conda.get_env_path"](env=env, prefix=prefix, user=user, rootdir=rootdir)
    else:
        prefix_ = env

    return os.path.exists(prefix_) and os.path.exists(os.path.join(prefix_, "bin", "conda"))


def _get_salt_username(user=None):
    """
    Get salt user from pillar data

    salt_settings:
      salt_username: anaconda

    Parameters
    ----------
    user: string

    Returns
    -------
    str: salt user (default: anaconda)

    """
    if user is None:
        user = __salt__["pillar.get"]("salt_settings:salt_username",
                                         "anaconda")
        return user
    return user


def managed(name, prefix=False, packages=None, channels=None, user=None, rootdir=None):
    """
    Manage a conda environment and optionally manage it with conda.

    If environment doesn't exists its created

    Parameters
    ----------
    name : str
        Environment name or path.
    prefix : bool
        Indicates if the env arg should be used as prefix install (path)
        for example: ``conda create -p /foo/bar python=3``
    packages : list (default=[python])
        List of packages to be installed
    user : str
        User to run the commands as
    channels : list of str
        Additional channels to search for packages
    rootdir : str (default=/opt/continuum/anaconda)
        If multiple anaconda installations which one to use

    Examples
    --------
    .. code-block:: bash

        conda.managed:
            - name: /tmp/env
            - prefix: true
            - packages:
              - numpy
              - scipy
            - channels:
              - conda-forge
              - anaconda.org/my/channel
    """
    user = _get_salt_username(user)
    ret = {"name": name, "changes": {}, "comment": "", "result": True}

    if __opts__["test"]:
        ret["result"] = None

        env_exists = _check_if_environment_exists(env=name, prefix=prefix, user=user, rootdir=rootdir)
        if not env_exists:
            ret["comment"] = "Conda env {} is set to be created".format(name)
            return ret
        else:
            comments = []
            for pkg in packages:
                pkg_installed = _check_if_installed(pkg=pkg, env=name, prefix=prefix, user=user, rootdir=rootdir)
                if not pkg_installed:
                    comments.append("Package {} is set to be installed".format(pkg))
            if comments:
                ret["comment"] = "\n".join(comments)
            else:
                ret["comment"] = "Environment and packages are in the correct state"
            return ret

    pkgs = ",".join(packages)
    channels = channels or []
    channels_str = ",".join(channels)

    env_exists = ''
    module_ret= ''

    try:
        env_exists = _check_if_environment_exists(env=name, prefix=prefix, user=user, rootdir=rootdir)
        if env_exists:
            module_ret = __salt__["conda.install"](
                env=name, pkgs=pkgs, prefix=prefix,
                channels=channels_str, user=user,
                rootdir=rootdir)
            if module_ret.get("success", False):
                ret["comment"] = "Packages successfully installed in environment {}".format(name)
                ret["changes"][name] = ret["comment"]
                ret["result"] = True
            else:
                ret["model_ret"] = module_ret
                ret["comment"] = module_ret.get("error")
                ret["result"] = False
        else:
            module_ret = __salt__["conda.create"](
                name, pkgs=pkgs, prefix=prefix, channels=channels_str,
                user=user, rootdir=rootdir)
            if module_ret.get("success"):
                ret["comment"] = "Conda environment '{0}' created with packages: {1}".format(name, pkgs)
                ret["changes"][name] = ret["comment"]
                ret["result"] = True
            else:
                ret["model_ret"] = module_ret
                ret["comment"] = module_ret.get("error")
                ret["result"] = False
    except Exception as e:
        ret["comment"] = e
        ret["env_exists"] = env_exists
        ret["module_ret"] = module_ret
        ret["result"] = False
    return ret
