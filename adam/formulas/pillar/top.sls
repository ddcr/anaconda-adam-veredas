#!py
import os
import logging

from salt.syspaths import BASE_PILLAR_ROOTS_DIR as _BASE_PILLAR_ROOTS_DIR

log = logging.getLogger(__name__)


BASE_PILLAR_ROOTS_DIR = os.path.join(_BASE_PILLAR_ROOTS_DIR, "base")


def run():
  """
  Returns all the pillars in the pillars directory for all the nodes

  base:
    '*':
      - pillar1
      - pillar2
  """
  matches = []
  for filename in os.listdir(BASE_PILLAR_ROOTS_DIR):
    pillar_name, ext = os.path.splitext(filename)
    if ext != "" and ext != ".sls":
      log.debug("Pillar: Skipping bogus file: %s" % (filename,))
      continue
    if is_available(pillar_name):
      matches.append(pillar_name)
  if matches == []:
    return {}
  else:
    return {'base': {'*': matches}}


def is_available(pillar_name):
  log.debug("Checking pillar: {}".format(pillar_name))

  if pillar_name == "top":
    return False
  if pillar_name.startswith("."):
    return False

  if os.path.isfile(os.path.join(BASE_PILLAR_ROOTS_DIR, pillar_name + ".sls" )):
    return True
  if os.path.exists(os.path.join(BASE_PILLAR_ROOTS_DIR, pillar_name)):
    return True
  return False
