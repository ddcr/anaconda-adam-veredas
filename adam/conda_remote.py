"""
Methods for remote conda functionality
"""
from __future__ import absolute_import, division, print_function

import logging

from .responses import CondaResponse
from .exceptions import AdamException

logger = logging.getLogger(__name__)


def conda_install(cluster, target="*", env="root", pkgs=None, prefix=False, user=None, channels=None):
    """
    Remote conda install ...

    Parameters
    ----------
    cluster: Cluster Object
    target: str
        salt target: (minion_id, *, ...)
    env: str
        Name of conda environment (default: root)
    pkgs: str
        comma separated list of packages
    prefix: bool
        Use env name as name or full path to conda environment (default: name)
    channel: str
        conda channel
    user: str
        user to run command as (default: salt_setting:salt_username)


    Returns
    -------
    CondaResponse Object

    Examples
    --------
    conda_install("*", "pkgs=numpy=1.10,pandas=0.18")
    conda_install("*", "pkgs=numpy=1.10,pandas=0.18", env=py3k)
    """
    user = user or cluster.salt_settings.salt_username

    channels = channels or []
    channels_str = ",".join(channels)
    kwarg = dict(env=env, pkgs=pkgs, prefix=prefix, user=user, channels=channels_str)
    response = cluster.salt(target, "conda.install", kwarg=kwarg)
    response = response.as_cls(CondaResponse)
    return response


def conda_remove(cluster, target="*", env="root", pkgs=None, prefix=False, user=None):
    """
    Remote conda remove ...

    Parameters
    ----------
    cluster: Cluster Object
    target: str
        salt target: (minion_id, *, ...)
    env: str
        Name of conda environment (default: root)
    pkgs: str
        comma separated list of packages
    prefix: bool
        Use env name as name or full path to conda environment (default: name)
    channel: str
        conda channel
    user: str
        user to run command as (default: salt_setting:salt_username)

    Returns
    -------
    CondaResponse Object

    Examples
    --------
    conda_remove("*", "pkgs=numpy=1.10,pandas=0.18")
    conda_remove("*", "pkgs=numpy=1.10,pandas=0.18", env=py3k)
    """
    user = user or cluster.salt_settings.salt_username

    kwarg = dict(env=env, pkgs=pkgs, prefix=prefix, user=user)
    response = cluster.salt(target, "conda.remove", kwarg=kwarg)
    response = response.as_cls(CondaResponse)
    return response


def conda_list(cluster, target="*", env="root", prefix=False, user=None):
    """
    Remote conda install ...

    Parameters
    ----------
    cluster: Cluster Object
    target: str
        salt target: (minion_id, *, ...)
    env: str
        Name of conda environment (default: root)
    prefix: bool
        Use env name as name or full path to conda environment (default: name)
    user: str
        user to run command as (default: salt_setting:salt_username)

    Returns
    -------
    CondaResponse Object

    Examples
    --------
    conda_list("*", "")
    conda_list("*", env="py3k")
    """
    user = user or cluster.salt_settings.salt_username

    kwarg = dict(env=env, prefix=prefix, user=user)
    response = cluster.salt(target, "conda.list", kwarg=kwarg)
    response = response.as_cls(CondaResponse)
    return response


def conda_info(cluster, target="*", options=""):
    """
    Method to remotely call conda info

    Parameters
    ----------
    cluster: Cluster Object
    target: str
        salt target: (minion_id, *, ...)
    options: str
        options for conda info (a, e, l, s)

    Returns
    -------
    CondaResponse Object

    Examples
    --------

    conda_info("*")
    conda_info("*", "e")
    """
    response = cluster.salt(target, "conda.info", options)
    response = response.as_cls(CondaResponse)
    return response


def conda_create(cluster, target="*", env=None, pkgs=None, prefix=False, user=None, channels=None):
    """
    Remote conda install ...

    Parameters
    ----------
    cluster: Cluster Object
    target: str
        salt target: (minion_id, *, ...)
    env: str
        Name of conda environment (REQUIRED)
    pkgs: str
        comma separated list of packages
    prefix: bool
        Use env name as name or full path to conda environment (default: name)
    user: str
        user to run command as (default: salt_setting:salt_username)
    channel: list of str
        conda channels

    Returns
    -------
    CondaResponse Object

    Examples
    --------
    conda_create("*", env="py3k", "pkgs=numpy=1.10,pandas=0.18,python=3")
    """
    if not env:
        raise AdamException("ENV name is empty.  Please name the environment you wish to create")

    if not pkgs:
        raise AdamException("Packages string is empty.  Please list packages you wish to include in env")

    user = user or cluster.salt_settings.salt_username

    channels = channels or []
    channels_str = ",".join(channels)
    kwarg = dict(env=env, pkgs=pkgs, prefix=prefix, user=user, channels=channels_str)
    response = cluster.salt(target, "conda.create", kwarg=kwarg)
    response = response.as_cls(CondaResponse)
    return response


def conda_update(cluster, target="*", env="root", pkgs=None, prefix=False, user=None, channels=None):
    """
    Remote conda update ...

    Parameters
    ----------
    cluster: Cluster Object
    target: str
        salt target: (minion_id, *, ...)
    env: str
        Name of conda environment (default: root)
    pkgs: str
        comma separated list of packages
    prefix: bool
        Use env name as name or full path to conda environment (default: name)
    user: str
        user to run command as (default: salt_setting:salt_username)
    channel: str
        conda channel

    Returns
    -------
    CondaResponse Object

    Examples
    --------
    conda_update("*", "pkgs=numpy=1.10,pandas=0.18")
    conda_update("*", "pkgs=numpy=1.10,pandas=0.18", env=py3k)
    """
    user = user or cluster.salt_settings.salt_username

    channels = channels or []
    channels_str = ",".join(channels)
    kwarg = dict(env=env, pkgs=pkgs, prefix=prefix, user=user, channels=channels_str)
    response = cluster.salt(target, "conda.update", kwarg=kwarg)
    response = response.as_cls(CondaResponse)
    return response
