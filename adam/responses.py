from __future__ import absolute_import, division, print_function

import os
import sys
import copy
import logging
import collections
from itertools import groupby

from .utils import hash_dict
from .exceptions import AdamException, SaltException

logger = logging.getLogger(__name__)

try:
    # Make CondaRemoteResponse available for this module
    cur_dir = os.path.abspath(os.path.dirname(__file__))
    models_dir = os.path.join(cur_dir, "formulas", "salt", "_modules")
    assert os.path.exists(models_dir)
    sys.path.insert(0, models_dir)
    from conda_models import CondaRemoteResponse
    # sys.path.pop()
except:
    raise AdamException("Could not import CondaRemoteResponse, package installation might be corrupted")


class Response(collections.OrderedDict):
    """Wraps and parses a dict/json response coming from libpepper.
    Extends the response dict where keys of the dict represent the minions
    """

    @classmethod
    def from_libpepper(cls, data):
        """
        Parses a response coming from libpepper
        """
        new_response = cls()
        data = data["return"]
        for item in data:
            for minion_id, states in item.items():
                new_response[minion_id] = states
        return new_response

    def as_cls(self, cls_):
        new_response = cls_()
        new_response.update(self)
        if cls_ == StateResponse:
            new_response.parse_supervisor()
        return new_response

    def validate(self):
        """
        Validate the minion output for various errors such as no matching SLS or template errors.
        """
        for minion_id, states in self.items():
            if not isinstance(states, dict):
                raise SaltException("State response for minion {} is not a dictionary. {}".format(minion_id, states))

        if self.has_no_matching_sls():
            raise SaltException("No matching State file found. Try syncing the formulas.")
        if self.has_template_error():
            raise SaltException("Error rendering salt formulas")

    def groups(self, ignore_fields=None):
        """
        method groups similar dictionary response together in an effort to understand
        which nodes have the same output

        Parameters
        ----------
        ignore_fields: list
            list of fields to ignore when grouping

        Returns
        -------
        list of list of tuples
            tuples (node_name, dictionary response)
            list of tuples with same response
        """
        # remove field we are uninterested in comparing
        if ignore_fields is not None:
            me = copy.deepcopy(self)
            for id_ in me:
                for field in ignore_fields:
                    try:
                        me[id_].pop(field)
                    except KeyError:
                        pass
                    except AttributeError:
                        # probably an error with Salt and/or Anaconda Setup
                        raise AdamException(str(me[id_]))
        else:
            me = self

        hashes = [(hash_dict(me[m]), me[m], m) for m in me.keys()]

        # group responses with the same hash
        hashes.sort(key=lambda x: x[0])
        all_groups = []
        for h, group in groupby(hashes, lambda x: x[0]):
            nodes = []
            for _h, d, k in group:
                nodes.append(k)

            all_groups.append((nodes, d))

        return all_groups

    def has_template_error(self):
        for node_id, output in self.items():
            if isinstance(output, list) and len(output) == 1 and "Rendering SLS" in output[0]:
                return True
        return False

    def has_no_matching_sls(self):
        for node_id, output in self.items():
            if isinstance(output, list) and len(output) == 1 and "No matching sls found for" in output[0]:
                return True
        return False

    def has_no_space(self):
        """
        Check if disk is full

        Returns
        -------
        bool
        """

        for node_id, output in self.items():
            if isinstance(output, dict):
                for v in output.values():
                    error_check = v.get('changes', {}).get('stderr', '')
                    if "No space left on device" in error_check:
                        return True
        return False

    def has_existing_directory(self):
        """
        Check if directory exists

        Returns
        -------
        bool
        """

        for node_id, output in self.items():
            if isinstance(output, dict):
                for v in output.values():
                    error_check = v.get('changes', {}).get('stderr', '')
                    if "File or directory already exists" in error_check:
                        return True
        return False


class StateResponse(Response):

    def parse_supervisor(self):
        """
        Parse any state, check if the output comes from supervisor and parses the response
        """
        import re
        for minion_id, states in self.items():
            for state_name, state_content in states.items():
                comment = state_content["comment"]
                match = re.search("Service ([a-z_-]*) will be started", comment)
                if match:
                    service_name = match.groups()[0]
                    self[minion_id][state_name]["comment"] = "Service {} is stoped".format(service_name)

    def group_successful(self):
        """
        Groups the states of each minion by successful or failed

        Return
        ------
        Dictionary like:
            {
                "minion_id1": {"successful": [{...}], "failed": [{...}],
                "minion_id2": {"successful": [{...}], "failed": [{...}]
            }

        """
        field = "result"
        valid_output = True
        ret = {}
        for minion_id, states in self.items():
            inner_values = []
            for state_name, state_output in states.items():
                state_output["name"] = state_name
                inner_values.append(state_output)

            successful = [state for state in inner_values if state[field] == valid_output]
            failed = [state for state in inner_values if state[field] != valid_output]
            summary = {"successful": successful, "failed": failed}
            ret[minion_id] = summary
        return ret

    @property
    def failed_nodes(self):
        """
        Convenience method to check if any nodes of failed

        Returns
        -------
        [(minion_id, {error})]
        """
        failed = []
        groups = self.group_successful()
        for minion_id, resp in groups.items():
            errors = {}
            error = resp.get('failed', None)
            if error:
                failed.append((minion_id, error))

        return failed

    @property
    def has_failed(self):
        """
        Return boolean value of whether a remote conda command has failed

        Returns
        -------
        bool
        """
        return bool(self.failed_nodes)

class CondaResponse(Response):

    def update(self, values):
        values_ = {}
        for key, value in values.items():
            values_[key] = CondaRemoteResponse(value)
        super(CondaResponse, self).update(values_)

    def groups(self, ignore_fields=None):
        """
        method groups similar CondaRemoteResponse objects together in an effort to understand
        which nodes have the same output
        Parameters
        ----------
        ignore_fields: list
            list of fields to ignore when grouping
        Returns
        -------
        list of list of tuples
            tuples (node_name, dictionary response)
            list of tuples with same response
        """

        # remove field we are uninterested in comparing
        if ignore_fields is not None:
            me = copy.deepcopy(self)
            for id_ in me:
                for field in ignore_fields:
                    setattr(me[id_], field, None)
        else:
            me = self

        hashes = [(hash_dict(me[m].to_primitive(role="ignore_host")), me[m], m) for m in me.keys()]

        # group responses with the same hash
        hashes.sort(key=lambda x: x[0])
        all_groups = []
        for h, group in groupby(hashes, lambda x: x[0]):
            nodes = []
            for _h, d, k in group:
                nodes.append(k)

            all_groups.append((nodes, d))

        # reorder groups to put failed nodes at the end
        # helpful for UX when print failed nodes
        all_groups.sort(key=lambda g: g[1].success, reverse=True)

        return all_groups

    @property
    def failed_nodes(self):
        """
        Check responses for failures in three places:
         - error
         - error_type
         - schema_error

        Returns
        -------
        [(minion_id, {error})]
        """
        failed = []
        error_keys = CondaRemoteResponse.error_keys

        for minion_id, cresp in self.items():
            errors = {}
            for err in error_keys:
                error = getattr(cresp, err, None)
                if error:
                    errors[err] = error
            if errors:
                failed.append((minion_id, errors))

        return failed

    @property
    def has_failed(self):
        """
        Return boolean value of whether a remote conda command has failed

        Returns
        -------
        bool
        """
        return bool(self.failed_nodes)
