from __future__ import print_function, division, absolute_import


class AdamException(Exception):
    pass


class SaltException(AdamException):
    pass


class SSHException(AdamException):
    pass


class MKDIRException(AdamException):
    pass




class LicenseException(AdamException):
    pass


class RetriesExceededException(AdamException):

    def __init__(self, function, last_exception=None, message="Retries limit exceeded"):
        super(RetriesExceededException, self).__init__(message)
        self.function = function
        self.last_exception = last_exception
