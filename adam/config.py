"""Module to manage the interactions to the filesystem

Manages the save/load of cluster and profiles from yaml files to Adam Objects
"""
import os
import yaml
import glob
import logging
import itertools

from schematics.exceptions import ModelConversionError

from .exceptions import AdamException

logger = logging.getLogger(__name__)

ROOT_DIR = os.path.expanduser("~/.continuum/adam")
PROFILE_DIR = os.path.join(ROOT_DIR, "profile.d")
CLUSTER_DIR = os.path.join(ROOT_DIR, "cluster.d")


def makedirs():
    """Create the default config directories
    """
    if not os.path.exists(ROOT_DIR):
        os.makedirs(ROOT_DIR)
    if not os.path.exists(PROFILE_DIR):
        os.makedirs(PROFILE_DIR)
    if not os.path.exists(CLUSTER_DIR):
        os.makedirs(CLUSTER_DIR)


def get_profiles():
    """Load all the profiles in the config directory

    Returns
    -------
        list of adam.models.Profile
    """
    from .models import Profile
    glob_path = os.path.join(PROFILE_DIR, "*.yml")
    glob_path2 = os.path.join(PROFILE_DIR, "*.yaml")
    profile_filenames = multiple_file_types(glob_path, glob_path2)

    profiles = {}
    for profile_filepath in profile_filenames:
        logger.debug("Loading profile from {0}".format(profile_filepath))

        data = load_yml(profile_filepath)
        try:
            logger.debug("Validating Profile: {}".format(profile_filepath))
            profile = Profile(data)
            profile.validate()
        except ModelConversionError as e:
            logger.error("Invalid Profile: {}".format(profile_filepath))
            logger.error(e)
            raise e

        if profile.name in profiles:
            logger.warning("Previously defined profile >> {} << " "redefined in {}".format(profile.name, profile_filepath))

        profiles[profile.name] = profile

    return profiles


def get_profile(name):
    """Get a profile

    Returns
    -------
        adam.models.Profile
    """
    profiles = get_profiles()
    if name in profiles:
        return profiles[name]
    else:
        raise AdamException("Profile {name} not found in {dir}".format(name=name, dir=ROOT_DIR))


def profile_exists(name):
    """Checks if a profile exists based on the name

    Returns
    -------
        boolean
    """
    return name in get_profiles()


def get_clusters():
    """Load all the clusters in the config directory

    Returns
    -------
        list of adam.models.Cluster
    """
    from adam import Cluster
    glob_path = os.path.join(CLUSTER_DIR, "*.yml")
    glob_path2 = os.path.join(CLUSTER_DIR, "*.yaml")
    cluster_filenames = multiple_file_types(glob_path, glob_path2)

    clusters = {}
    for cluster_filepath in cluster_filenames:
        logger.debug("Loading cluster from {0}".format(cluster_filepath))

        data = load_yml(cluster_filepath)
        try:
            logger.debug("Validating cluster: {}".format(cluster_filepath))
            cluster = Cluster(data)
            cluster.validate()
        except ModelConversionError as e:
            logger.error("Invalid cluster: {}".format(cluster_filepath))
            logger.error(e)
            raise e

        if cluster.name in clusters:
            logger.warning("Previously defined cluster >> {} << " "redefined in {}".format(cluster.name, cluster_filepath))

        clusters[cluster.name] = cluster

    return clusters


def get_cluster(name):
    """Get a cluster based on the name

    Returns
    -------
        adam.models.Cluster
    """
    clusters = get_clusters()
    if name in clusters:
        return clusters[name]
    else:
        raise AdamException("Cluster {name} not found in {dir}".format(name=name, dir=ROOT_DIR))


def cluster_exists(name):
    """Checks if a profile exists based on the name

    Returns
    -------
        boolean
    """
    return name in get_clusters()


def save_cluster(cluster):
    """Save a cluster object to a file in the config directories
    """
    to_yml(cluster.to_primitive(), os.path.join(CLUSTER_DIR, cluster.name + ".yml"))


def remove_cluster(name):
    """Delete a cluster file from the config directories
    """
    from adam import Cluster
    glob_path = os.path.join(CLUSTER_DIR, '*.yml')
    glob_path2 = os.path.join(CLUSTER_DIR, '*.yaml')
    cluster_filenames = multiple_file_types(glob_path, glob_path2)

    for cluster_filepath in cluster_filenames:
        logger.debug("Loading cluster from {0}".format(cluster_filepath))
        data = load_yml(cluster_filepath)
        if data["name"] == name:
            os.remove(cluster_filepath)


def to_yml(data, filepath):
    """Save a dictionary to a file as a yaml file

    Parameters
    ----------
    data : dict
        data to save as yaml
    filepath : str
        location to save the yaml filen
    """
    with open(filepath, "w") as f:
        yaml.safe_dump(data, f, default_flow_style=False)


def multiple_file_types(*patterns):
    """Utility to checkout for a filename with multiple extentions

    Returns
    -------
        An iterable of files that can match multiple patterns
    """
    return itertools.chain.from_iterable(glob.glob(pattern) for pattern in patterns)


def load_yml(filepath):
    """Load a yaml file into a dictionary

    Parameters
    ----------
    filepath : str
        file path to yaml file

    Returns
    -------
        dict
    """
    with open(filepath) as f:
        data = yaml.load(f.read())
    return data
