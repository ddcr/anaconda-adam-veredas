from __future__ import absolute_import, division, print_function

import six

if six.PY2:
    FileNotFoundError = IOError
    PermissionError = IOError
    from StringIO import StringIO
    from urllib2 import URLError
    from urlparse import urlparse
else:
    PermissionError = PermissionError
    FileNotFoundError = FileNotFoundError
    from io import StringIO
    from urllib.error import URLError
    from io import StringIO
    from urllib.parse import urlparse
