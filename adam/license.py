import logging
import datetime

import adam
import _license

from .exceptions import LicenseException

logger = logging.getLogger(__name__)


def check_license_decorator(products=None):
    products = products or ("Anaconda Enterprise Notebooks",)

    def real_check(function):

        def wrapper(*args, **kwargs):
            check_valid_product_license(products)
            return function(*args, **kwargs)

        return wrapper

    return real_check


def check_valid_product_license(product_names):
    """
    Main entry point for license checking.
    Checks the license for one of many named product(s)

    Raises
    ------
        LicenseException if not a valid license for that product
    """
    if isinstance(product_names, str):
        product_names = [product_names]
    license = get_license(product_names)
    check_expired_license(license)
    check_valid_license(license)


def get_license_paths():
    """
    Get full paths to all license files
    """
    return _license.get_license_paths()


def get_all_licenses():
    """
    Return all the licenses

    Return
    ------
        list of License object
    """
    return _license.find_licenses()


def get_license(product_names):
    """
    From all of the available licenses, check for the named product
    If multiple licenses are valid for a product, it returns the license with the most days remaining

    Parameters
    ----------
        product_name (str)

    Return
    ------
        License object

    Raises
    ------
        LicenseException if not a valid license for that product
    """
    try:
        licenses = [license for license in _license.find_licenses() if license["product"] in product_names]
    except KeyError:
        raise LicenseException("Invalid license. \n" "Contact support via " "https://www.continuum.io/contact-us")
    if not licenses:
        raise LicenseException("No valid license found for product(s): \n"
                               "{} \n"
                               "Contact support via "
                               "https://www.continuum.io/contact-us".format(product_names))
    return max(licenses, key=lambda l: l.get("end_date", "99999-99-99"))


def check_valid_license(license):
    """
    Checks that the license is valid, raises LicenseException if not
    """
    license = (_license.verify_license(license)
               and license["vendor"] in ("Continuum Analytics, Inc.", "Continuum Analytics", "continuum"))
    if not license:
        raise LicenseException("Invalid license. \n" "Contact support via " "https://www.continuum.io/contact-us")


def check_expired_license(license):
    """
    Check if the license is expired, warns/nags if not
    """
    if get_days_left(license) < 0:
        logger.warning("License expired {} days ago. \n"
                       "You are no longer authorized to use this software. \n"
                       "Contact support via "
                       "https://www.continuum.io/contact-us \n".format(-get_days_left(license)))


def get_days_left(license):
    """
    Get the number of days left for a license
    """
    if "end_date" not in license:
        return float("inf")
    try:
        return (_license.date_from_string(license["end_date"]) - datetime.date.today()).days
    except (ValueError, TypeError):
        raise LicenseException("Licensing error needs to be addressed. \n"
                               "Contact support via "
                               "https://www.continuum.io/contact-us")
        return 0
    return -1


def get_available_nodes():
    """
    Check all the licenses for product "Anaconda Cluster" and returns the maximum number of nodes available
    """
    try:
        licenses = [license for license in _license.find_licenses() if license["product"] in "Anaconda Cluster"]
    except KeyError:
        raise LicenseException("Invalid license. \n" "Contact support via " "https://www.continuum.io/contact-us")
    max_nodes = 4
    for license in licenses:
        if "nodes" in license and license["nodes"] > max_nodes:
            max_nodes = license["nodes"]
    return max_nodes


def check_license_nodes(requested_nodes):
    """
    Check if the license is valid for the number of requested nodes.
    """
    clusters = adam.get_clusters()
    available_nodes = get_available_nodes()
    already_created_nodes = sum(len(cluster.nodes) for _, cluster in clusters.items())
    if already_created_nodes + requested_nodes > available_nodes:
        raise LicenseException("Requested {} nodes. \n"
                               "{} nodes are in use. \n"
                               "License is only valid for {} nodes. \n"
                               "Contact support via "
                               "https://www.continuum.io/contact-us".format(requested_nodes, already_created_nodes,
                                                                            available_nodes))
