"""
SSH utilities
"""
from __future__ import absolute_import, division, print_function

import os
import time
import errno
import logging
import posixpath
import threading
from socket import gaierror as sock_gaierror, error as sock_error

from .compatibility import FileNotFoundError, StringIO
from .utils import retry
from .exceptions import AdamException, SSHException, RetriesExceededException, MKDIRException

import paramiko

logger = logging.getLogger(__name__)


class SSHClient(object):

    def __init__(self, host, username=None, password=None, pkey=None, port=22, timeout=15, sudo_su_login=False,
                 invoke_shell=False, sudo_cmd="sudo -S", sudo_cmd_flags="", connect=True):
        """
        Wrapper around Paramiko.SSHClient with extra utilities.

        Parameters
        ----------
            host: Hostname of the node to SSH
            username: Username to SSH
            password: Password to SSH if the username has that option
            pkey: (paramiko.RSAKey) Private Key to SSH if the username has that option.
                See utilities `SSHClient.pkey_from_key_file` and `SSHClient.pkey_from_string`
            port (default=22): SSH port of the node
            sudo_su_login (default=False): When executing a command that uses sudo and not asuser then use invoke_shell with `sudo su -`
            connect (default=True): If True auto connect to the Node. If False do SSHClient.connect() to connect.
        """
        self.host = host
        self.username = username
        self.password = password
        self.pkey = pkey
        self.port = port
        self.timeout = timeout
        self.sudo_su_login = sudo_su_login
        self.invoke_shell = invoke_shell
        self.sudo_cmd = sudo_cmd
        self.sudo_cmd_flags = sudo_cmd_flags

        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.MissingHostKeyPolicy())
        self._sftp = None

        if connect:
            self.connect()

    @staticmethod
    def pkey_from_key_file(filepath):
        filepath = os.path.expanduser(filepath)
        pkey = None
        try:
            pkey = paramiko.RSAKey.from_private_key_file(filepath)
            logger.debug("%s Type matched as RSA", filepath)
        except paramiko.PasswordRequiredException:
            logger.debug("Unable to decrypt private key.  Please use an "
                         "ssh-agent and the `agent_pubkey` key in the YAML file")
        except paramiko.SSHException:
            pass

        try:
            pkey = paramiko.DSSKey.from_private_key_file(filepath)
            logger.debug("%s Type matched as DSA", filepath)
        except paramiko.PasswordRequiredException:
            logger.debug("Unable to decrypt private key.  Please use an "
                         "ssh-agent and the `agent_pubkey` key in the YAML file")
        except paramiko.SSHException:
            pass

        try:
            pkey = paramiko.ECDSAKey.from_private_key_file(filepath)
            logger.debug("%s Type matched as ECDSA", filepath)
        except paramiko.PasswordRequiredException:
            logger.debug("Unable to decrypt private key.  Please use an "
                         "ssh-agent and the `agent_pubkey` key in the YAML file")
        except paramiko.SSHException:
            pass

        if pkey:
            return pkey
        else:
            raise SSHException("Unable to load key file as RSA/DSA/ECDSA")

    @staticmethod
    def pkey_from_string(key):
        pkey = None

        try:
            logger.debug("Trying to load key as an RSA key")
            pkey = paramiko.RSAKey.from_private_key(StringIO(key))
        except paramiko.PasswordRequiredException:
            logger.debug("Unable to decrypt private key.  Please use an "
                         "ssh-agent and the `agent_pubkey` key in the YAML file")
        except paramiko.SSHException:
            pass

        try:
            logger.debug("Trying to load key as an DSA key")
            pkey = paramiko.DSSKey.from_private_key(StringIO(key))
        except paramiko.PasswordRequiredException:
            logger.debug("Unable to decrypt private key.  Please use an "
                         "ssh-agent and the `agent_pubkey` key in the YAML file")
        except paramiko.SSHException:
            pass

        try:
            logger.debug("Trying to load key as an ECDSA key")
            pkey = paramiko.ECDSAKey.from_private_key(StringIO(key))
        except paramiko.PasswordRequiredException:
            logger.debug("Unable to decrypt private key.  Please use an "
                         "ssh-agent and the `agent_pubkey` key in the YAML file")
        except paramiko.SSHException:
            pass

        if pkey:
            return pkey
        else:
            raise SSHException("Unable to load key as RSA/DSA/ECDSA")

    @staticmethod
    def pkey_from_key_file_w_agent(pubfilepath):
        """
        Parameters
        ----------
        pubfilepath: path to public ssh key

        Returns
        -------
        paramiko agent pkey
        """

        filepath = os.path.expanduser(pubfilepath)
        agent = paramiko.Agent()
        agent_keys = agent.get_keys()
        if len(agent_keys) == 0:
            raise SSHException("No keys in SSH Agent")

        with open(filepath) as f:
            data_pub = f.read()
            data_pub = data_pub.split(" ")

        # assume pub keys have preamble and email signature
        if len(data_pub) > 0:
            local_pub = data_pub[1]
        else:
            local_pub = data_pub[0]

        pub_keys = [(k, k.get_base64()) for k in agent_keys]

        for k, k_pub in pub_keys:
            if k_pub == local_pub:
                return k

        raise SSHException("Public key does not match any keys in the ssh-agent")

    def connect(self):
        """Connect to host
        """
        try:
            self.client.connect(self.host,
                                username=self.username,
                                password=self.password,
                                port=self.port,
                                pkey=self.pkey,
                                timeout=self.timeout)
        except paramiko.AuthenticationException as e:
            raise SSHException("Authentication Error to host '%s'" % self.host)
        except sock_gaierror as e:
            raise SSHException("Unknown host '%s'" % self.host)
        except sock_error as e:
            raise SSHException("Error connecting to host '%s:%s'\n%s" % (self.host, self.port, e))
        except paramiko.SSHException as e:
            raise SSHException("General SSH error - %s" % e)

    def close(self):
        self.client.close()

    def get_transport(self):
        return self.client.get_transport()

    def exec_command(self, command, onlyif=None, unless=None, asuser=None, sudo=False, **kwargs):
        """Wrapper to paramiko.SSHClient.exec_command with some utility extra functions

        Parameters
        ----------
            onlyif (str): Only execute the command if this check returns exit_code = 0
            unless (str): Only execute the command if this check returns exit_code = 1
                only checks if `onlyif is None`
        """
        if onlyif is not None:
            onlyif_ret = self.exec_command_(onlyif, asuser=asuser, sudo=sudo, **kwargs)
            if onlyif_ret["exit_code"] == 1:
                logger.debug("Onlyif execution failed, command not executed: %s" % onlyif)
                # return None
        elif unless is not None:
            unless_ret = self.exec_command_(unless, asuser=asuser, sudo=sudo, **kwargs)
            if unless_ret["exit_code"] == 0:
                logger.debug("Unless execution succeeded, command not executed: %s" % unless)
                # return None
        return self.exec_command_(command, asuser=asuser, sudo=sudo, **kwargs)

    def exec_command_(self, command, asuser=None, sudo=False, **kwargs):
        """Wrapper to paramiko.SSHClient.exec_command
        """
        if sudo:
            command = prep_sudo(self, command, asuser=asuser)
        elif asuser is not None:
            command = "su - {user} -c '{command}'".format(command=command, user=asuser)
        else:
            command = "bash -c '{command}'".format(command=command)

        logger.debug("Running command | %s | on '%s' as user %s", command, self.host, asuser or self.username)

        if self.invoke_shell:
            logger.debug("Opening SSH channel with invoked shell")
            channel = self.client.invoke_shell()
        else:
            logger.debug("Opening SSH channel")
            channel = self.get_transport().open_session()

        # stdin = channel.makefile("wb")
        stdout = channel.makefile("rb")
        stderr = channel.makefile_stderr("rb")

        if self.invoke_shell:
            stdout_ = ""
            stderr_ = ""
            exit_code = 0
            buff_size = 10024
            last_line_check = ""
            commands = []
            if self.sudo_su_login:
                logger.debug("Using sub-shell with 'sudo su -'")
                commands.append("sudo su -")
                commands.append(command)
            else:
                commands.append("echo 'start-shell-fake-cmd'")
                commands.append(command)

            for i, cmd in enumerate(commands):
                logger.debug("Sending command | %s | to shell", cmd)
                channel.send(cmd + "\n")
                if i == 0:
                    time.sleep(1)
                    output = channel.recv(buff_size).decode("utf-8")
                    last_line_check = output.split("\r\n")[-1]
                    logger.debug(last_line_check)
                else:
                    output = ""
                    while last_line_check not in output:
                        time.sleep(0.5)
                        output += channel.recv(buff_size).decode("utf-8")
                if i == len(commands) - 1:
                    # Return the output of the last command: Ignore first and last lines of shell
                    stdout_ = "\n".join(output.split("\r\n")[1:-1])

                    # Echo the exit code of the last command
                    channel.send("echo $?" + "\n")
                    exit_code_output = channel.recv(buff_size).decode("utf-8")

                    try:
                        exit_code = int(exit_code_output.split("\r\n")[1])
                    except ValueError:
                        channel.send("echo $?" + "\n")
                        exit_code_output = channel.recv(buff_size).decode("utf-8")
                        if "?" in exit_code_output:
                            logger.debug("get exit code one more time")
                            channel.send("echo $?" + "\n")
                            exit_code_output = channel.recv(buff_size).decode("utf-8")
                        logger.debug("exit_code_output: {}".format(exit_code_output))
                        # exit_code = int(exit_code_output.split("\r\n")[1])
                        exit_code = 8  # for now default to non-zero/one value

            channel.close()
            ret = {"stdout": stdout_, "stderr": stderr_, "exit_code": exit_code}
        else:
            channel.get_pty()
            channel.exec_command(command, **kwargs)
            while not (channel.recv_ready() or channel.closed or channel.exit_status_ready()):
                time.sleep(0.2)
            ret = {"stdout": stdout.read().strip().decode("utf-8"),
                   "stderr": stderr.read().strip().decode("utf-8"),
                   "exit_code": channel.recv_exit_status()}
        channel.close()
        return ret

    @property
    def sftp(self):
        if self._sftp is None:
            self._sftp = self.make_sftp()
        return self._sftp

    def make_sftp(self):
        """Make SFTP client from an open transport"""
        transport = self.get_transport()
        transport.open_session()
        return paramiko.SFTPClient.from_transport(transport)

    def file_exists(self, path):
        try:
            self.sftp.stat(path)
        except IOError as error:
            if error.errno == errno.ENOENT:
                return False
        else:
            return True

    def mkdir(self, path, mode=511, asuser=None, sudo=False, ignore_existing=False):
        """Create a directory including all needed parents
        """
        if self.dir_exists(path):
            if ignore_existing:
                pass
            else:
                raise SSHException("Directory '{}' already exists".format(path))
        else:
            output = self.exec_command("mkdir -p {path}".format(path=path), asuser=asuser, sudo=sudo)
            if output["exit_code"] != 0:
                error = output["stdout"] + output["stderr"]
                raise MKDIRException("Could not create directory: '{}' {}".format(path, error))

    def dir_exists(self, path):
        try:
            self.sftp.chdir(path)
            return True
        except IOError:
            return False

    def put(self, local, remote, sudo=False):
        """Copy local file to host via SFTP/SCP

        Copy is done natively using SFTP/SCP version 2 protocol, no scp command
        is used or required.
        """
        if not os.path.exists(local):
            raise SSHException("Local file '{}' doesn't exist".format(local))
        if os.path.isdir(local):
            self.mkdir(remote, sudo=sudo, ignore_existing=True)
            self.put_dir(local, remote, sudo=sudo)
        else:
            self.put_single(local, remote, sudo=sudo)

    def put_single(self, local, remote, sudo=False):
        if sudo:
            real_remote = remote
            remote = "/tmp/adam/tmp_copy_file"
            if not self.dir_exists("/tmp/adam"):
                self.mkdir("/tmp/adam", asuser=self.username, sudo=sudo)

        logger.debug("Uploading file %s to %s", local, remote)
        try:
            self.sftp.put(local, remote)
        except FileNotFoundError:
            raise SSHException("Could not upload file '{}' to '{}' parent directory doesn't exists".format(local,
                                                                                                           remote))

        if sudo:
            cmd = "yes | cp -rf {} {}".format(remote, real_remote)
            output = self.exec_command(cmd, sudo=True)
            if output["exit_code"] != 0:
                error = output["stdout"] + output["stderr"]
                raise SSHException("Could not upload file '{}' to '{}': {}".format(local, remote, error))
            cmd = "rm -rf {}".format(remote)
            self.exec_command(cmd, sudo=True)

    def put_dir(self, local, remote, sudo=False, recursing=False):
        logger.debug("Uploading directory %s to %s", local, remote)

        if sudo and not recursing:
            # Only do this when sudo=True and its the first time `put_dir` is called (not a reursive call)
            real_remote = remote
            remote = "/tmp/adam/tmp_copy_dir"

        if not self.dir_exists(remote):
            self.mkdir(remote, asuser=self.username, sudo=sudo)
        for item in os.listdir(local):
            if os.path.isfile(os.path.join(local, item)):
                self.put(os.path.join(local, item), "%s/%s" % (remote, item))
            else:
                path = posixpath.join(remote, item)
                if not self.dir_exists(path):
                    self.mkdir(path, asuser=self.username, sudo=sudo)
                recursive_local = os.path.join(local, item)
                recursive_remote = "%s/%s" % (remote, item)
                self.put_dir(recursive_local, recursive_remote, sudo=sudo, recursing=True)

        if sudo and not recursing:
            # Only do this when sudo=True and its the first time `put_dir` is called (not a reursive call)
            cmd = "yes | cp -rf {}/* {}".format(remote, real_remote)
            output = self.exec_command(cmd, sudo=True)
            if output["exit_code"] != 0:
                error = output["stdout"] + output["stderr"]
                raise SSHException("Could not upload directory '{}' to '{}': {}".format(local, remote, error))
            cmd = "rm -rf {}".format(remote)
            self.exec_command(cmd, sudo=True)

    def stat(self, path):
        return self.sftp.stat(path)

    def chown(self, path, uid, gid):
        return self.sftp.chown(path, uid, gid)

    def chmod(self, path, mode, sudo=False):
        command = "chmod {mode} {path}".format(mode=mode, path=path)
        return self.exec_command(command, sudo=sudo)

    def open(self, path, mode="r", *args, **kwargs):
        return self.sftp.open(path, mode=mode, *args, **kwargs)

    def remove(self, path):
        return self.sftp.remove(path)


def exec_command(client, command, *args, **kwargs):
    """
    Execute a command in a SSHClient.
    It retries on failed execution.
    Raises AdamException if command failed
    """

    @retry(retries=3, wait=0)
    def __remote_cmd():
        ret = client.exec_command(command, *args, **kwargs)
        if ret["exit_code"] != 0:
            raise Exception(ret["stdout"] + ret["stderr"])
        return ret

    try:
        return __remote_cmd()
    except RetriesExceededException as e:
        raise AdamException("Remote command failed. Last Exception:\n{}".format(e.last_exception))


def prep_sudo(client, command, asuser=None):
    """
    Utility to handle sudo options from crazy setups. ENJOY!
    """
    if client.invoke_shell and client.sudo_su_login:
        if asuser is None:
            command = command
        else:
            command = "su - {user} -c '{command}'".format(command=command, user=asuser)
    elif client.sudo_su_login:
        command = "echo '{command}' | sudo su -".format(command=command)
    else:
        if asuser is not None:
            sudo_cmd = "{sudo_cmd} -u {user} {sudo_cmd_flags}".format(sudo_cmd=client.sudo_cmd, sudo_cmd_flags=client.sudo_cmd_flags, user=asuser)
            command = "{sudo_cmd} bash -c '{command}'".format(sudo_cmd=sudo_cmd, command=command)
        else:
            sudo_cmd = "{sudo_cmd} {sudo_cmd_flags}".format(sudo_cmd=client.sudo_cmd, sudo_cmd_flags=client.sudo_cmd_flags)
            command = "{sudo_cmd} bash -c '{command}'".format(sudo_cmd=sudo_cmd, command=command)
    return command


def parallel_exec_command(clients, command, *args, **kwargs):
    """
    Execute a command in multiple nodes in parallel.
    It retries on failed execution.

    Params
    ------
        clients: list of `SSHClient`
        command: string of the command to execute.
            Available replacements:
                {node_number}: Number of the node in the clients list
        *args, **kwargs: extra arguments to pass to SSHClient.exec_command
    Returns
    -------
        Dictionary with the results for each node. If the command fails the result of that
        Node is `False`.
    """

    def async_cmd(client, command, results):

        @retry(retries=3, wait=0)
        def __remote_cmd():

            ret = client.exec_command(command, *args, **kwargs)
            if ret["exit_code"] != 0:
                raise Exception(ret["stdout"] + ret["stderr"])
            return ret

        try:
            results[client.host] = __remote_cmd()
        except RetriesExceededException as e:
            results[client.host] = e

    results, threads = {}, []
    for i, client in enumerate(clients):
        command_ = format_command(command, node_number=i)
        t = threading.Thread(target=async_cmd, args=(client, command_, results))
        t.start()
        threads.append(t)
    for t in threads:
        t.join()
    return results


def parallel_upload(clients, local, remote, *args, **kwargs):
    """
    Upload a file to multiple nodes in parallel.

    Parameters
    ----------
        clients: list of `SSHClient`
        local: local filepath to the file
        remote : remove filepath to upload the file to
    """

    def async_upload(client, local, remote, results):

        @retry(retries=3, wait=0)
        def __remote_upload():
            client.put(local, remote, *args, **kwargs)
            return True

        try:
            results[client.host] = __remote_upload()
        except RetriesExceededException as e:
            results[client.host] = e

    results, threads = {}, []
    for i, client in enumerate(clients):
        t = threading.Thread(target=async_upload, args=(client, local, remote, results))
        t.start()
        threads.append(t)
    for t in threads:
        t.join()
    return results


def format_command(command, node_number=None):
    """
    Utility to format commands using python formatting templates
    """
    if node_number is not None and "{node_number}" in command:
        command = command.format(node_number=node_number)
    return command


def check_results(results, error_msg):
    """
    Utility to check the results multiple SSH commands results and file uploads
    that are usually created by `parallel_exec_command` and `parallel_upload`.

    Raises and `AdamException` if one of the nodes failed.
    """
    all_ok = all(r is False for r in results)
    if not all_ok:
        failed_nodes = {}
        for minion_host, minion_data in results.items():
            if isinstance(minion_data, RetriesExceededException):
                failed_nodes[minion_host] = minion_data
        if failed_nodes:
            for minion_host, minion_data in failed_nodes.items():
                msg = "{msg}. Node: {node}. Error: {error}".format(msg=error_msg,
                                                                   node=minion_host,
                                                                   error=minion_data.last_exception)
                logger.error(msg)
            raise AdamException(msg)
