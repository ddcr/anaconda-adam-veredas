"""
Utility to make the salt conda state importable so sphinx can use their docstring
"""

import os
import sys

cur_dir = os.path.abspath(os.path.dirname(__file__))
states_dir = os.path.join(cur_dir, "..", "formulas", "salt", "_states")
sys.path.insert(0, states_dir)

import conda

managed = conda.managed

del conda
sys.path.pop()
