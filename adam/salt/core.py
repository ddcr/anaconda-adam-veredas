"""
Utility functions to manage salt bootstrap
"""
from __future__ import absolute_import, division, print_function

import os
import yaml
import stat
import time
import logging
import posixpath

from adam.ssh import exec_command, parallel_exec_command, check_results
from adam.utils import retry, tmpfile
from adam.exceptions import AdamException, RetriesExceededException


logger = logging.getLogger(__name__)


def set_roles(cluster, extra_node_roles=None):
    """
    Set salt roles in the nodes of the cluster.

    1. Uses the cluster object to set the roles that are defined in the `Node` objects
    2. Optionally set extra roles using the `extra_node_roles` kwargs

    Arguments
    ---------
        cluster: `adam.Cluster` object
        roles (default=None): Dictionary like `{"node host": [list of roles]}` or `{"node host": "role"}`
    """
    salt_call = posixpath.join(cluster.salt_prefix, "bin", "salt-call")
    command = "{} grains.append roles {{}}".format(salt_call)

    for node in cluster.nodes:
        for role in node.roles:
            logger.debug("Setting role {role} in node {host}".format(role=role, host=node.host))
            client = node.ssh_client
            client.exec_command(command.format(role), sudo=True)
            client.close()

    if extra_node_roles:
        if len(extra_node_roles) > len(cluster.nodes):
            raise AdamException("Number nodes to set roles is higher than the nodes of nodes in the cluster")

        for node_host, roles_list in extra_node_roles.items():
            if isinstance(roles_list, str):
                roles_list = [roles_list]

            for role in roles_list:
                client = cluster.get_node(host=node_host).ssh_client
                client.exec_command(command.format(role), sudo=True)
                client.close()

    cluster.salt("*", "saltutil.sync_all")


def upload_licenses(cluster):
    """
    Upload license files to the correct location on the salt master.
    Requires sudo.
    """

    from adam import license

    head_client = cluster.head.ssh_client
    salt_prefix = cluster.salt_prefix
    licenses_dst_salt = posixpath.join(salt_prefix, "srv", "salt", "base", "licenses")

    logger.debug("Uploading license files")
    if not head_client.dir_exists(licenses_dst_salt):
        head_client.mkdir(licenses_dst_salt, sudo=True)
    for license_path in license.get_license_paths():
        license_src_fn = os.path.basename(license_path)
        license_dst_fn = posixpath.join(licenses_dst_salt, license_src_fn)
        head_client.put(license_path, license_dst_fn, sudo=True)


def upload_formulas(cluster):
    """
    Upload the salt pillars to the correct location in the salt environment
    Salt syncs the formulas
    Requires sudo.
    """
    import adam
    from adam import license
    from adam.plugin_models import get_plugins_modules

    head_client = cluster.head.ssh_client
    salt_prefix = cluster.salt_prefix
    adam_src = os.path.realpath(os.path.dirname(adam.__file__))
    src_salt_root = os.path.join(adam_src, "formulas", "salt")
    src_pillar_root = os.path.join(adam_src, "formulas", "pillar")
    base_dst_salt_root = posixpath.join(salt_prefix, "srv", "salt", "base")
    base_dst_pillar_root = posixpath.join(salt_prefix, "srv", "pillar", "base")
    plugins_dst_salt_root = posixpath.join(salt_prefix, "srv", "salt", "plugins")
    plugins_dst_pillar_root = posixpath.join(salt_prefix, "srv", "pillar", "plugins")

    logger.debug("Uploading core formulas")
    head_client.put(src_salt_root, base_dst_salt_root, sudo=True)
    head_client.put(src_pillar_root, base_dst_pillar_root, sudo=True)

    plugins_modules = get_plugins_modules()
    if len(plugins_modules) > 0:
        logger.debug("Uploading plugin formulas")
        for plugin_name, module in plugins_modules:
            plugin_src = os.path.realpath(os.path.dirname(module.__file__))
            plugin_formulas = os.path.join(plugin_src, "formulas")
            if os.path.exists(plugin_formulas):
                src_salt_root = os.path.join(plugin_formulas, "salt")
                if os.path.exists(src_salt_root):
                    head_client.put(src_salt_root, plugins_dst_salt_root, sudo=True)
                src_pillar_root = os.path.join(plugin_formulas, "pillar")
                if os.path.exists(src_pillar_root):
                    head_client.put(src_pillar_root, plugins_dst_pillar_root, sudo=True)

    upload_settings(cluster)

    logger.debug("Salt: Syncing modules")

    @retry(retries=10, wait=3)
    def salt_sync():
        results = cluster.salt("*", "saltutil.sync_all")
        if len(results) != len(cluster.nodes):
            raise Exception("Couldn't sync modules to all nodes {}".format(results))

        # necessary when using pillar data in _modules
        results = cluster.salt("*", "saltutil.refresh_pillar")
        if len(results) != len(cluster.nodes):
            raise Exception("Couldn't refresh pillar data to all nodes {}".format(results))

        results = cluster.salt("*", "conda.prefix")
        for minionid, value in results.items():
            if cluster.anaconda_dir != value:
                raise Exception("Conda prefix is incorrect. Expected {exp} != Actual {act}:\n{results}".format(
                    exp=cluster.anaconda_dir, act=value, results=results))

    try:
        salt_sync()
    except RetriesExceededException as e:
        raise AdamException("Couldn't sync salt modules to all nodes. Last Exception:\n{}".format(e.last_exception))

    head_client.close()


def upload_settings(cluster):
    """
    Upload the cluster definition that includes the plugin settings to the Pillar directory as `settings.sls`
    """
    logger.debug("Salt: Upload cluster settings to pillars")
    salt_prefix = cluster.salt_prefix
    profile_dst_pillar_root = posixpath.join(salt_prefix, "srv", "pillar", "base")
    initsls = posixpath.join(profile_dst_pillar_root, "settings.sls")

    head_client = cluster.head.ssh_client

    with tmpfile() as tmp_initsls:
        with open(tmp_initsls, "w") as f:
            f.write(yaml.safe_dump(cluster.to_primitive(), default_flow_style=False))
        head_client.put(tmp_initsls, initsls, sudo=True)

    head_client.close()


def flush_iptables(cluster):
    """
    Flush iptables on all cluster nodes
    """
    logger.debug("Flushing iptables rules")
    clients = [node.ssh_client for node in cluster.nodes]
    command = "iptables -F"
    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't flush iptables")

    [client.close() for client in clients]


def set_selinux_permissive(cluster):
    """
    Set SELinux to permissive (setenforce 0) on all nodes
    """
    logger.debug("Setting SELinux to permissive (setenforce 0)")
    clients = [node.ssh_client for node in cluster.nodes]
    command = "setenforce 0 || /bin/true"
    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't set SELinux to permissive")

    [client.close() for client in clients]


def set_selinux_context(cluster):
    """
    Set SELinux contexts
    """
    logger.debug("Setting SELinux context for salt-master")
    command_template = "chcon system_u:object_r:rpm_exec_t:s0 {bin}"
    head_client = cluster.head.ssh_client
    salt_master_bin = "{env}/bin/salt-master".format(env=cluster.salt_prefix)
    salt_master_command = command_template.format(bin=salt_master_bin)
    exec_command(head_client, salt_master_command, sudo=True)

    logger.debug("Setting SELinux context for salt-api")
    command_template = "chcon system_u:object_r:rpm_exec_t:s0 {bin}"
    head_client = cluster.head.ssh_client
    salt_api_bin = "{env}/bin/salt-api".format(env=cluster.salt_prefix)
    salt_api_command = command_template.format(bin=salt_api_bin)
    exec_command(head_client, salt_api_command, sudo=True)

    logger.debug("Setting SELinux context for salt-minion")
    salt_minion_bin = "{env}/bin/salt-minion".format(env=cluster.salt_prefix)
    salt_minion_command = command_template.format(bin=salt_minion_bin)
    clients = [node.ssh_client for node in cluster.nodes]
    results = parallel_exec_command(clients, salt_minion_command, sudo=True)
    check_results(results, "Couldn't set SELinux to permissive for salt-minion")

    logger.debug("Setting SELinux context for salt-call")
    salt_call_bin = "{env}/bin/salt-call".format(env=cluster.salt_prefix)
    salt_call_command = command_template.format(bin=salt_call_bin)
    clients = [node.ssh_client for node in cluster.nodes]
    results = parallel_exec_command(clients, salt_call_command, sudo=True)
    check_results(results, "Couldn't set SELinux to permissive for salt-call")

    head_client.close()
    [client.close() for client in clients]


def configure_condarc(cluster):
    """
    configure various condarc settings
     - channels
     - channel_alias
     - ...
    """
    clients = [node.ssh_client for node in cluster.nodes]

    # can't use `conda config --add channels` because this will
    # add an undesired `- defaults` to the condarc file
    condarc_dict = {}

    # ssl_verify can be true/false/str
    # http://conda.pydata.org/docs/install/central.html#ssl-verification
    ssl_verify = cluster.conda.ssl_verify

    if str(ssl_verify).lower() == 'false':
        ssl_verify = False
    elif str(ssl_verify).lower() == 'true':
        ssl_verify = True

    condarc_dict["ssl_verify"] = ssl_verify
    condarc_dict["auto_update_conda"] = False
    condarc_dict["channels"] = cluster.conda.channels
    condarc_dict["channel_alias"] = cluster.conda.channel_alias
    if cluster.network.http_proxy or cluster.network.https_proxy:
        condarc_dict["proxy_servers"] = {}
        if cluster.network.http_proxy:
            condarc_dict["proxy_servers"]["http"] = cluster.network.http_proxy
        if cluster.network.https_proxy:
            condarc_dict["proxy_servers"]["https"] = cluster.network.https_proxy
    condarc_file = posixpath.join(cluster.salt_prefix, ".condarc")

    @retry(retries=5, wait=3)
    def upload():
        results = upload_config(clients, condarc_file, condarc_dict, sudo=True)
        check_results(results, "Couldn't create condarc for salt environment")
    try:
        upload()
    except RetriesExceededException as e:
        raise AdamException("Couldn't upload condarc files")

    logger.debug("Confirm correct conda settings")
    command = "CONDARC={CONDARC} {conda} config --get channels --system".format(conda=cluster.conda_bin,
                                                                        CONDARC=cluster.conda_rc)
    results = parallel_exec_command(clients, command, sudo=True)

    for node, msg_back in results.items():
        config = msg_back["stdout"]
        if "anaconda-adam" not in config:
            raise AdamException("anaconda-adam is not listed in the condarc channels")

    [client.close() for client in clients]


def install_salt(cluster, force_flush_iptables=False, force_selinux_permissive=False, force_selinux_context=False):
    """
    Install salt package and start salt-master, salt-api and salt-minions daemons
    Requires sudo.
    """
    clients = [node.ssh_client for node in cluster.nodes]
    head_client = cluster.head.ssh_client

    if cluster.conda.conda_canary:
        command = "CONDARC={CONDARC} {conda} config --add channels conda-canary --system; " \
                  "CONDARC={CONDARC} {conda} update conda -y".format(conda=cluster.conda_bin,
                                                   CONDARC=cluster.conda_rc)
        results = parallel_exec_command(clients, command, sudo=True)
        check_results(results, "Couldn't add conda-canary to condarc file")

    logger.debug("All Nodes: Creating environment for salt in: {}".format(cluster.salt_prefix))
    command = "CONDARC={CONDARC} {conda} install -y python=2 pyopenssl schematics==1.1.1 " \
              "tornado salt==2016.3.1 cherrypy==5.5.0".format(conda=cluster.conda_bin,
                                                              CONDARC=cluster.conda_rc)
    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't create conda environment for salt")
    logger.debug("All Nodes: Setting up salt-minion")

    minion_config = {
        "id": "{node_host}",
        "master": cluster.head.host,
        "master_port": cluster.salt_settings.minion_ret_port,
        "tcp_pub_port": cluster.salt_settings.minion_pub_port,
        "tcp_pull_port": cluster.salt_settings.minion_pull_port,
        "mine_functions": {
            "network.get_hostname": [],
            "network.interfaces": [],
            "network.ip_addrs": [],
            "grains.get": ["id"],
        },
        "mine_interval": 2,
        "log_level": "info",
        "log_file": posixpath.join(cluster.salt_prefix, "var", "log", "salt", "minion"),
        "hash_type": "sha256",
        "backend": "requests",
        "requests_lib": True
    }
    remote = "{env}/etc/salt/minion".format(env=cluster.salt_prefix)

    @retry(retries=5, wait=3)
    def upload():
        results = upload_config(clients, remote, minion_config, sudo=True)
        check_results(results, "Couldn't upload salt-minion config".format(cluster.rootdir))
    try:
        upload()
    except RetriesExceededException as e:
        raise AdamException("Couldn't upload minion-config files")

    logger.debug("Head node: Setting up salt-master")
    master_config = {
        "auto_accept": True,
        "gather_job_timeout": 15,
        "timeout": 10,
        "log_level": "info",
        "hash_type": "sha256",
        "log_file": "{env}/var/log/salt/master".format(env=cluster.salt_prefix),
        "publish_port": cluster.salt_settings.job_pub_port,
        "ret_port": cluster.salt_settings.minion_ret_port,
        "file_roots": {
            "base": [
                "{env}/srv/salt/plugins".format(env=cluster.salt_prefix),
                "{env}/srv/salt/base".format(env=cluster.salt_prefix),
            ]
        },
        "pillar_roots": {
            "base": [
                "{env}/srv/pillar/plugins".format(env=cluster.salt_prefix),
                "{env}/srv/pillar/base".format(env=cluster.salt_prefix),
            ]
        },
        "external_auth": {
            "pam": {
                cluster.salt_settings.salt_username: [".*", "@jobs"]
            }
        },
        "rest_cherrypy": {
            "port": cluster.salt_settings.rest_port,
            "ssl_crt": "{env}/etc/pki/tls/certs/localhost.crt".format(env=cluster.salt_prefix),
            "ssl_key": "{env}/etc/pki/tls/certs/localhost.key".format(env=cluster.salt_prefix),
            "expire_responses": False
        }
    }
    remote = "{env}/etc/salt/master".format(env=cluster.salt_prefix)
    results = upload_config([head_client], remote, master_config, sudo=True)
    check_results(results, "Couldn't upload salt-master config".format(cluster.rootdir))

    # Security stuff be done after installing binaries and before executing any commands
    if cluster.security.flush_iptables or force_flush_iptables:
        flush_iptables(cluster)
    if cluster.security.selinux_permissive or force_selinux_permissive:
        set_selinux_permissive(cluster)
    if cluster.security.selinux_context or force_selinux_context:
        set_selinux_context(cluster)

    logger.debug("Head node: Creating Self Signed SSL certificate")
    command = "{env}/bin/salt-call --local tls.create_self_signed_cert cacert_path={env}/etc/pki".format(env=cluster.salt_prefix)
    exec_command(head_client, command, sudo=True)

    logger.debug("Head node: Starting salt-master")
    command = "kill `cat {env}/var/run/salt-master.pid` || /bin/true; {env}/bin/salt-master -d".format(
        env=cluster.salt_prefix)
    exec_command(head_client, command, sudo=True)

    logger.debug("Head node: Starting salt-api")
    command = "kill `cat {env}/var/run/salt-api.pid` || /bin/true; {env}/bin/salt-api -d".format(
        env=cluster.salt_prefix)
    exec_command(head_client, command, sudo=True)

    logger.debug("All nodes: Starting salt-minion")
    command = "kill `cat {env}/var/run/salt-minion.pid` || /bin/true; {env}/bin/salt-minion -d".format(
        env=cluster.salt_prefix)
    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't start salt-minions".format(cluster.rootdir))

    head_client.close()
    [client.close() for client in clients]


def check_salt_running(cluster):
    """
    Checks:
        1. Head node: salt-master is running
        2. Head node: salt-api is running
        3. All nodes: salt-minion is running

    Requires sudo.

    Returns
    -------
        Dictionary like: {node_host: salt_minion status (boolean)}

    Raises
    -----
        AdamException if salt-master or salt-api services are not running
    """
    head_client = cluster.head.ssh_client

    logger.debug("Head node: Check salt-master running")
    command = "test -e {env}/var/run/salt-master.pid".format(env=cluster.salt_prefix)
    exec_command(head_client, command, sudo=True)
    command = "kill -0 `cat {env}/var/run/salt-master.pid`".format(env=cluster.salt_prefix)
    exec_command(head_client, command, sudo=True)

    logger.debug("Head node: Check salt-api running")
    command = "test -e {env}/var/run/salt-api.pid".format(env=cluster.salt_prefix)
    exec_command(head_client, command, sudo=True)
    command = "kill -0 `cat {env}/var/run/salt-api.pid`".format(env=cluster.salt_prefix)
    exec_command(head_client, command, sudo=True)

    head_client.close()

    logger.debug("All nodes: Check salt-minion running")
    clients = [node.ssh_client for node in cluster.nodes]
    minion_pid = posixpath.join(cluster.salt_prefix, "var", "run", "salt-minion.pid")
    results = parallel_exec_command(clients, "kill -0 $(cat {}); echo $?".format(minion_pid), sudo=True)
    ret = {}
    for node_host, status in results.items():
        ret[node_host] = status["stdout"] == "0"
    [client.close() for client in clients]
    return ret


def check_salt_api_connection(cluster):
    results = {}

    @retry(retries=5, wait=3)
    def check(results):
        results.update(cluster.salt("*", "test.ping"))

    try:
        check(results)
    except RetriesExceededException as e:
        pass  # Maybe raise here?

    return results


def install_service_scripts(cluster):
    """
    Optional function to install service scripts in /etc/init.d/ and
    add scripts to runlevel restart
    """

    logger.debug("Installing service scripts in /etc/init.d and setting chkconfig")

    results = cluster.salt(cluster.head.host, "state.sls", "service_scripts.master")
    if results.has_failed:
        logger.debug(results.failed_nodes)

    results = cluster.salt("*", "state.sls", "service_scripts.minion")
    if results.has_failed:
        logger.debug(results.failed_nodes)

    clients = [node.ssh_client for node in cluster.nodes]
    [client.close() for client in clients]
    return results



def install_miniconda(cluster):
    """
    For all the nodes: Download and install miniconda for salt
    Usually needs to be ran after `create_anaconda_user`
    Requires sudo
    """
    clients = [node.ssh_client for node in cluster.nodes]
    miniconda_url = cluster.conda.miniconda_url
    logger.debug("All Nodes: Downloading miniconda.sh")
    miniconda_path = "{homedir}/miniconda.sh".format(homedir=cluster.rootdir)
    command = "curl -q -L -o {miniconda} {url}".format(miniconda=miniconda_path, url=miniconda_url)
    http_proxy = "http_proxy={http_proxy}".format(http_proxy=cluster.network.http_proxy) if cluster.network.http_proxy else ""
    https_proxy = "https_proxy={https_proxy}".format(https_proxy=cluster.network.https_proxy) if cluster.network.https_proxy else ""
    command = "{http_proxy} {https_proxy} {command}".format(http_proxy=http_proxy, https_proxy=https_proxy, command=command)
    hash_type, miniconda_hash = cluster.conda.miniconda_hash.split("=")
    if hash_type == "md5":
        unless_cmd = "[ $(md5sum {miniconda_path} | awk '{{ print $1 }}') == '{miniconda_hash}' ]".format(miniconda_path=miniconda_path, miniconda_hash=miniconda_hash)
    else:
        unless_cmd = "test -e {miniconda}".format(miniconda=miniconda_path)
    results = parallel_exec_command(clients, command, unless=unless_cmd, sudo=True)
    check_results(results, "Couldn't download miniconda")

    logger.debug("All Nodes: Set owner of miniconda.sh")
    miniconda_path = "{homedir}/miniconda.sh".format(homedir=cluster.rootdir)
    command = "chown {user}:{group} {miniconda}".format(miniconda=miniconda_path,
                                                        group=cluster.salt_settings.salt_groupname,
                                                        user=cluster.salt_settings.salt_username)
    results = parallel_exec_command(clients, command, unless=unless_cmd, sudo=True)
    check_results(results, "Couldn't download miniconda")

    logger.debug("All Nodes: Installing miniconda")
    command = "bash {homedir}/miniconda.sh -f -b -p {prefix}".format(prefix=cluster.salt_prefix,
                                                                     homedir=cluster.rootdir)
    unless_cmd = "test -e {prefix}/bin/conda".format(prefix=cluster.salt_prefix)
    results = parallel_exec_command(clients, command, unless=unless_cmd, sudo=True)
    check_results(results, "Couldn't install miniconda")

    [client.close() for client in clients]


def create_anaconda_group(cluster):
    """
    For all the nodes create the anaconda group.
    Requires sudo.
    """
    clients = [node.ssh_client for node in cluster.nodes]

    logger.debug("All Nodes: Creating {group} group".format(group=cluster.salt_settings.salt_groupname))
    command = "getent group {group} &> /dev/null || groupadd {group}".format(group=cluster.salt_settings.salt_groupname)
    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't create group in nodes: {failed_nodes}")
    [client.close() for client in clients]


def check_anaconda_group(cluster):
    """
    Checks if the anaconda group is present on the cluster nodes

    Returns
    -------
        Dictionary like: {node_host: status (boolean)}
    """
    clients = [node.ssh_client for node in cluster.nodes]
    results = parallel_exec_command(clients, "getent group {group} &> /dev/null; echo $?".format(group=cluster.salt_settings.salt_groupname))
    ret = {}
    for node_host, status in results.items():
        ret[node_host] = status["stdout"] == "0"
    [client.close() for client in clients]
    return ret


def create_anaconda_user(cluster):
    """
    For all the nodes create the anaconda linux user.
    This user home directory will be `/opt/continuum/anaconda`.
    Requires sudo
    """
    clients = [node.ssh_client for node in cluster.nodes]

    logger.debug("Create home directory")
    command = "mkdir -p {homedir}".format(homedir=cluster.rootdir)
    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't create home directory: {homedir}".format(homedir=cluster.rootdir))

    logger.debug("All Nodes: Creating {user} user".format(user=cluster.salt_settings.salt_username))
    command = "getent passwd {username} &> /dev/null || useradd -M -g {group} -d {homedir} -p $(openssl passwd -1 {password}) {username}".format(
        username=cluster.salt_settings.salt_username, group=cluster.salt_settings.salt_groupname,
        homedir=cluster.rootdir, password=cluster.salt_settings.salt_password)
    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't create user in nodes: {failed_nodes}")

    logger.debug("Change ownership on home directory")
    command = "chown -R {username}:{groupname} {homedir}".format(username=cluster.salt_settings.salt_username,
                                                                 groupname=cluster.salt_settings.salt_groupname,
                                                                 homedir=cluster.rootdir)

    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't change ownership of homedir to user: {username}".format(username=cluster.salt_settings.salt_username))

    logger.debug("All Nodes: Setting permissions to 755 on home dir %s", cluster.rootdir)
    command = "chmod {mode} {path}".format(mode=755, path=cluster.rootdir)
    results = parallel_exec_command(clients, command, sudo=True)
    check_results(results, "Couldn't configure global readability: {failed_nodes}")

    for client in clients:
        l_stat = client.stat(cluster.rootdir)
        if not l_stat.st_mode == 16877:  # oct(16877) == 755
            raise AdamException("Failed to set {rootdir} to 755".format(rootdir=cluster.rootdir))

    [client.close() for client in clients]


def check_anaconda_user(cluster):
    """
    Checks if the anaconda user is present on the cluster nodes

    Returns
    -------
        Dictionary like: {node_host: status (boolean)}
    """
    clients = [node.ssh_client for node in cluster.nodes]
    results = parallel_exec_command(clients, "getent passwd {username} &> /dev/null; echo $?".format(username=cluster.salt_settings.salt_username))
    ret = {}
    for node_host, status in results.items():
        ret[node_host] = status["stdout"] == "0"
    [client.close() for client in clients]
    return ret


def upload_config(clients, filepath, data, *args, **kwargs):
    """
    Uploads a dictionary configuration to the nodes in yaml format
    """
    import yaml
    import tempfile
    from adam.compatibility import StringIO

    results = {}
    for client in clients:
        stream = StringIO()
        yaml.safe_dump(data, stream, default_flow_style=False)
        output = stream.getvalue().format(node_host=client.host)
        with tempfile.NamedTemporaryFile("w") as tempf:
            tempf.write(output)
            tempf.flush()
            tempf.seek(0)
            local = tempf.name
            time.sleep(.4)
            results[client.host] = client.put(local, filepath, *args, **kwargs)

    return results
