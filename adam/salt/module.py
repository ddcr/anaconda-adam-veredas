"""
Utility to make the salt conda module importable so sphinx can use their docstring
"""

import os
import sys

cur_dir = os.path.abspath(os.path.dirname(__file__))
modules_dir = os.path.join(cur_dir, "..", "formulas", "salt", "_modules")
sys.path.insert(0, modules_dir)

import conda

create = conda.create._original
install = conda.install._original
update = conda.update._original
remove = conda.remove._original
list = conda.list_._original
info = conda.info_._original

del conda
sys.path.pop()
